//
//  AfterRegistrationVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 12/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class AfterRegistrationVC: UIViewController {

    
    @IBOutlet weak var viewImg1: UIView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var viewRegistration: UIView!
    @IBOutlet weak var lblRegistration: UILabel!
    
    
    @IBOutlet weak var viewImg2: UIView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var viewUpload: UIView!
    @IBOutlet weak var lblUplad: UILabel!
    
    @IBOutlet weak var viewImg3: UIView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var viewAddVehicle: UIView!
    @IBOutlet weak var lblAddVehicle: UILabel!
    
    
    @IBOutlet weak var heightUploadDocuments: NSLayoutConstraint!
    @IBOutlet weak var heightAddVehicle: NSLayoutConstraint!
    
    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var btnAddVehicle: UIButton!
    @IBOutlet weak var viewMidLogo: UIView!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

    @IBAction func btnUpload(_ sender: Any) {
    }
    @IBAction func btnAddVehicle(_ sender: Any) {
    }
    

    func corner_radius(){
        
        
        self.viewRegistration.layer.cornerRadius = 3
        self.viewRegistration.layer.masksToBounds = true
        
        self.viewUpload.layer.cornerRadius = 3
        self.viewUpload.layer.masksToBounds = true
        
        self.viewAddVehicle.layer.cornerRadius = 3
        self.viewAddVehicle.layer.masksToBounds = true
        self.viewMidLogo.layer.cornerRadius = viewMidLogo.frame.size.height/2

    }
    
    
    
}
