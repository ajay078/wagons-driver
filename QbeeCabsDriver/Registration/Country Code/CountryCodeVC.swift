//
//  CountryCodeVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 15/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class CountryCodeVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    

    var arrAlphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    var delegateCountry:SendDataBackDelegate!
    var arrFilteredCountryCode = [countrySection]()
    
    var selectedIndex:Int = 10000
    
    //MARK: - Outlets
    
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    //MARK: - View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.getCountryCode()

    }
    

    //MARK: - Button Actions
    
    @IBAction func btnBack(_ sender: Any) {
//        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    

    //MARK: - Table View Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.arrFilteredCountryCode.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.darkGray

        let obj = self.arrFilteredCountryCode[section]
        
        header.textLabel?.text = obj.countryInitial

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let obj = self.arrFilteredCountryCode[section]
        
        return obj.arrCountryCode.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CountryCell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as! CountryCell
        
        let obj = self.arrFilteredCountryCode[indexPath.section].arrCountryCode[indexPath.row]

        cell.lblCountry.text = obj.country_name
        cell.lblCountryCode.text = "+\(obj.phone_code)"

        cell.selectionStyle = .blue
        
        if selectedIndex == indexPath.row{
            cell.viewCell.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        }else{
            cell.viewCell.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
       // cell.backgroundColor = .red
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.selectedIndex = indexPath.row
        self.tableViewOutlet.reloadData()
//        let vc:CountryCell = self.storyboard?.instantiateViewController(withIdentifier: "CountryCell") as! CountryCell
//        let dict = arrayCountryCode[indexPath.row] as! [String:AnyObject]
//        vc.strCountryCode = "+\(dict["phone_code"] as! String)"
//
////        self.navigationController?.present(vc, animated: true, completion: nil)
//        navigationController?.popViewController(animated: true)
//        dismiss(animated: true, completion: nil)
        
        let obj = self.arrFilteredCountryCode[indexPath.section].arrCountryCode[indexPath.row]

        let strCountryCode:String = "+\(obj.phone_code)"
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2){
            self.dismiss(animated: true, completion: nil)
            self.delegateCountry.sendData!(addCountryCode: strCountryCode)

        }
        

        
    }
    
    
    
    
    func getCountryCode() {
        // File location
        let fileURLProject = Bundle.main.path(forResource: "countrycode", ofType: "txt")
        // Read from the file
        var readStringProject = ""
        do {
            readStringProject = try String(contentsOfFile: fileURLProject!, encoding: String.Encoding.utf8)
            let countryData: NSData = readStringProject.data(using: String.Encoding.utf8)! as NSData
            
            let dictData: NSDictionary = try! JSONSerialization.jsonObject(with: countryData as Data, options: []) as! NSDictionary
            
            let arrCountryCodeNew = dictData["country"] as! [[String:String]]
            for countryIntial in self.arrAlphabet{
                
                let arrTemp = arrCountryCodeNew.filter{ ($0["country_name"]?.hasPrefix(countryIntial) ?? false) }
                let obj = countrySection.init(initial: countryIntial, array: arrTemp)
                self.arrFilteredCountryCode.append(obj)

            }
        
            
        } catch _ as NSError {
        }
    }
    
    
    // MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
//        lblHeader.text! = objLang?["LBL_SELECT_CONTRY"] as? String ?? ""
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
//            self.viewHeader.semanticContentAttribute = .forceRightToLeft //SMP: LTR to RTL
            
        }
        
    }
}
