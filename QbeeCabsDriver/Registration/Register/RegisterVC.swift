//
//  RegisterVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 10/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView



class RegisterVC: UIViewController, UITextFieldDelegate, SendDataBackDelegate {

    var strCountryCode:String = ""
    var strMaleFemale:String = ""

    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCoutryCode: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    
    @IBOutlet weak var imgMale: UIImageView!
    @IBOutlet weak var imgFemale: UIImageView!
    
    
    @IBOutlet weak var lblIAgree: UILabel!
    
    @IBOutlet weak var viewCheckBox: UIView!
    
    @IBOutlet weak var viewOtp: UIView!
    @IBOutlet weak var viewOtpIn: UIView!
    @IBOutlet weak var lblOtp: UITextField!
    
    
    @IBOutlet weak var viewInFirst: UIView!
    @IBOutlet weak var viewInLast: UIView!
    @IBOutlet weak var viewInEmail: UIView!
    @IBOutlet weak var viewInPassword: UIView!
    @IBOutlet weak var viewInCountryCode: UIView!
    @IBOutlet weak var viewInMobile: UIView!
    
    @IBOutlet weak var lblRequiredFirst: UILabel!
    @IBOutlet weak var lblRequiredLast: UILabel!
    @IBOutlet weak var lblRequiredEmail: UILabel!
    @IBOutlet weak var lblRequiredPassword: UILabel!
    @IBOutlet weak var lblRequiredCountryCode: UILabel!
    @IBOutlet weak var lblRequiredMobile: UILabel!

    
    @IBOutlet weak var heightVRFirst: NSLayoutConstraint!
    @IBOutlet weak var heightVRLast: NSLayoutConstraint!
    @IBOutlet weak var heightVREmail: NSLayoutConstraint!
    @IBOutlet weak var heightVRPassword: NSLayoutConstraint!
    @IBOutlet weak var heightVRCountryCode: NSLayoutConstraint!
    @IBOutlet weak var heightVRMobile: NSLayoutConstraint!


    
    
    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewOtp.isHidden = true
        self.padding()


    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//    return .lightContent
//    }
    
    

    
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnMale(_ sender: Any) {
        imgMale.image = UIImage(named: "radio")
        imgFemale.image = UIImage(named: "dot")
        
        self.strMaleFemale = "male"
    }
    
    @IBAction func btnFemale(_ sender: Any) {
        imgMale.image = UIImage(named: "dot")
        imgFemale.image = UIImage(named: "radio")
        
        self.strMaleFemale = "female"
    }
    

    
    
    @IBAction func btnTerms(_ sender: Any) {
        let sb:UIStoryboard = UIStoryboard(name: "HomeSB", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "SupportVC") as! SupportVC
        vc.isFromRegister = true
        self.navigationController?.pushViewController(vc, animated: true)
        

    }
    
    
    @IBAction func btnRegister(_ sender: Any) {
//        self.empty_textField()
        
        let vc:AfterRegistrationVC = self.storyboard?.instantiateViewController(withIdentifier: "AfterRegistrationVC") as! AfterRegistrationVC
        self.present(vc, animated: true, completion: nil)

    }
    @IBAction func btnCountryCode(_ sender: Any) {
        let vc:CountryCodeVC = self.storyboard?.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        vc.delegateCountry = self
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    @IBAction func btnAlreadyAccount(_ sender: Any) {
        let vc:LogInVC = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnOtpResend(_ sender: Any) {
    }
    @IBAction func btnOtpSubmit(_ sender: Any) {
    }
    
    
    
    
    func sendData(addCountryCode: String) {
        txtCoutryCode.text = addCountryCode

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtFirstName{
            txtLastName.becomeFirstResponder()
        }else if textField == txtLastName{
            txtEmail.becomeFirstResponder()
        }else if textField == txtEmail{
            txtPassword.becomeFirstResponder()
        }else if textField == txtPassword{
            txtCoutryCode.becomeFirstResponder()
        }else if textField == txtCoutryCode{
            txtMobile.becomeFirstResponder()
        }else if textField == txtMobile{
            textField.resignFirstResponder()
        }
        
        
        return true
    }
    
    func padding(){
//        txtFirstName.setLeftPaddingPoints(10)
//        txtLastName.setLeftPaddingPoints(10)
//        txtEmail.setLeftPaddingPoints(10)
//        txtPassword.setLeftPaddingPoints(10)
//        txtCoutryCode.setLeftPaddingPoints(10)
//        txtMobile.setLeftPaddingPoints(10)
        
        
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtCoutryCode.delegate = self
        txtMobile.delegate = self
        
        viewCheckBox.layer.borderWidth = 1

    }
    
    func empty_textField(){
        
        if txtFirstName.text!.isEmpty{
            self.viewInFirst.backgroundColor = UIColor.red
            self.heightVRFirst.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            self.lblRequiredFirst.text = "REQUIRED"
        }else if txtLastName.text!.isEmpty{
            self.viewInLast.backgroundColor = UIColor.red
            self.heightVRLast.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            self.lblRequiredLast.text = "REQUIRED"
        }else if txtEmail.text!.isEmpty{
            self.viewInEmail.backgroundColor = UIColor.red
            self.heightVREmail.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            self.lblRequiredEmail.text = "REQUIRED"
        }else if txtPassword.text!.isEmpty{
            self.viewInPassword.backgroundColor = UIColor.red
            self.heightVRPassword.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            self.lblRequiredPassword.text = "REQUIRED"
            
        }else if txtCoutryCode.text!.isEmpty{
            self.viewInCountryCode.backgroundColor = UIColor.red
            self.heightVRCountryCode.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            self.lblRequiredCountryCode.text = "REQUIRED"
            
        }else if txtMobile.text!.isEmpty{
            self.viewInMobile.backgroundColor = UIColor.red
            self.heightVRMobile.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            self.lblRequiredMobile.text = "REQUIRED"
        }
        
        
        
        
    }

    
}
