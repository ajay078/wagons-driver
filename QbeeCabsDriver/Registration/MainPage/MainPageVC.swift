//
//  MainPageVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 09/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class MainPageVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    var arrListLang = [ModalListLang]()
    var selectedIndex:Int = 10000
    var selectedLanguage = ""

    var window: UIWindow?
    var navigationcontroller:UINavigationController?
    
    @IBOutlet weak var btnLogIn: UIButton!
    @IBOutlet weak var viewSelectLang: UIView!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblWelcomeTo: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    

    @IBOutlet weak var viewLang: UIView!
    @IBOutlet weak var viewLangIn: UIView!

    @IBOutlet weak var tableViewLang: UITableView!

    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedLanguage = UserDefaults.standard.string(forKey: "default_language") ?? ""

        self.border_radius()
        self.post_listLang()
    }
    

// MARK: - Button Actions

    
    @IBAction func btnLogIn(_ sender: Any) {

        let vc:LogInVC = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func btnRegister(_ sender: Any) {
        let vc:RegisterVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func border_radius(){

        btnLogIn.layer.cornerRadius = 5
//        btnRegister.layer.cornerRadius = 5
        
        btnLogIn.layer.borderWidth = 2
        btnLogIn.layer.borderColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)

        viewLang.isHidden = true
        viewLangIn.layer.cornerRadius  = 5
        viewSelectLang.layer.borderWidth = 0.5

    }
    
    @IBAction func btnLanguage(_ sender: Any) {
        
        
        
        ValidationManager.sharedManager.showMainViewNN(viewMain: viewLang, viewInside: viewLangIn)
        
        
//        self.viewLang.isHidden = false
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            self.lblTitle.textAlignment = .right
        }
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblTitle.text! = objLang?["LBL_PROFILE_SELECT_LANGUAGE"] as? String ?? ""
        
        self.tableViewLang.reloadData()
    }
    
    @IBAction func btnHideViewLang(_ sender: Any) {
//        self.viewLang.isHidden = true
        ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: self.viewLang, viewPopUP: self.viewLangIn)

    }
    
    func alert(){
        let alert = UIAlertController(title: "Select Language", message: "Message", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
// MARK: - TableView Methods
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrListLang.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:EditProfileCell = tableView.dequeueReusableCell(withIdentifier: "EditProfileCell", for: indexPath) as! EditProfileCell
        cell.lblTitle.text! = arrListLang[indexPath.row].vTitle
                
//        let tempLang = self.arrListLang.filter({$0.vCode == self.selectedLanguage})
//        print(tempLang)

        
        if selectedIndex == indexPath.row{
            cell.viewCell.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        }else{
            cell.viewCell.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        
        let indexOfDefaultLang = self.arrListLang.firstIndex(where: {$0.vCode == self.selectedLanguage})

        if indexOfDefaultLang == indexPath.row{
            cell.viewCell.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        }else{
            cell.viewCell.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }


        
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedLanguage = arrListLang[indexPath.row].vCode
        self.selectedIndex = indexPath.row
        self.tableViewLang.reloadData()
        

        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2){
//            self.viewLang.isHidden = true
            
            ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: self.viewLang, viewPopUP: self.viewLangIn)
            
        }
        self.post_listLang()
        
    }
    
    
    // MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        btnLogIn.setTitle(objLang?["LBL_SIGN_IN_TXT"] as? String ?? "", for: .normal)
        lblWelcomeTo.text! = "\(objLang?["LBL_WELCOME_TO"] as? String ?? "") Wagons Taxi"
        
    }
    
}

// MARK: - Webservice calling

extension MainPageVC{
    
    func post_listLang(){
        
        ValidationManager.sharedManager.showLoader()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?AppVersion=1.0.4&GeneralMemberId=&GeneralAppVersion=1.0.4&vUserDeviceCountry=US&vLang=\(self.selectedLanguage)&vTimeZone=Asia %2FKolkata&type=generalConfigData&UserType=Driver&GeneralUserType=Driver&tSessionId=&GeneralDeviceType=ios"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue

                if action == "1"{
                    self.arrListLang.removeAll()
                    if let listLang = json["LIST_LANGUAGES"].arrayObject{
                        for index in 0..<listLang.count{
                            let obj = listLang[index]
                            let modalObj = ModalListLang.init(dict: obj as? [String : Any] ?? [:])
                            self.arrListLang.append(modalObj)
                        }
                    }
                    
                    
                    self.tableViewLang.reloadData()

                    if let LanguageLabels = json["LanguageLabels"].dictionaryObject{
                        UserDefaults.standard.setValue(LanguageLabels, forKey: "AllLanguageLabels")
                        
                    }
                    // let dictAllLanguageLabels = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    
                    if let defaultLanguage = json["DefaultLanguageValues"].dictionaryObject{
                        self.lblLanguage.text! = defaultLanguage["vTitle"] as? String ?? ""
                        self.selectedLanguage = defaultLanguage["vCode"] as? String ?? ""
                        UserDefaults.standard.setValue(self.selectedLanguage, forKey: "default_language")
                        
                        
                        let langCode = UserDefaults.standard.string(forKey: "default_language")
                        if langCode == "AR"{
                            self.viewSelectLang.semanticContentAttribute = .forceRightToLeft //SMP: LTR to RTL
                            self.lblLanguage.textAlignment = .right

                        }else{
                            self.viewSelectLang.semanticContentAttribute = .forceLeftToRight //SMP: LTR to RTL
                            self.lblLanguage.textAlignment = .left

                        }
                        
                    }
                    
                    self.setLanguageLabel()

                    
                }
                


                
                ValidationManager.sharedManager.hideLoader()
                
            case .failure(let error):
                print(error)
                 ValidationManager.sharedManager.hideLoader()


            }
        }
        
    }
    
        

    
}
