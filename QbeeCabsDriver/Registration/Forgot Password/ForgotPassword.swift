//
//  ForgotPassword.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 10/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ForgotPassword: UIViewController, UITextFieldDelegate {

    
    
    // MARK: - Web service variable

    let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    //MARK: - Outlets
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var viewHeader: UIView!

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblWeJustNeedYour: UILabel!
    @IBOutlet weak var btnContinue: UIButton!

    @IBOutlet weak var viewInEmail: UIView!
    @IBOutlet weak var heightVREmail: NSLayoutConstraint!
    @IBOutlet weak var lblRequiredEmail: UILabel!
    
    
    // MARK:- View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.height_required()
        self.corner_radius()
        txtEmail.setLeftPaddingPoints(5)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - Button Actions
    
    @IBAction func btnBack(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnContinue(_ sender: Any) {
        self.view.endEditing(true)
        if txtEmail.text!.isEmpty{
            
           self.viewInEmail.backgroundColor = UIColor.red
           self.heightVREmail.constant = 20
           UIView.animate(withDuration: 0.3) {
               self.view.layoutIfNeeded()
           }
           let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
           lblRequiredEmail.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
            
        }else if !isValidEmail(strEmail: txtEmail.text!){
            
            self.viewInEmail.backgroundColor = UIColor.red
            self.heightVREmail.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblRequiredEmail.text! = objLang?["LBL_FEILD_EMAIL_ERROR_SIGN_UP"] as? String ?? ""
        }else{
            self.post_resetPassword()
        }
    }

    
    

    

    
    func isValidEmail(strEmail:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: strEmail)
        return result
    }
    
    func height_required(){
        heightVREmail.constant = 0
    }
// MARK: - Text Field Methods

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtEmail{
            self.viewInEmail.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            self.heightVREmail.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            
        }else{
            self.viewInEmail.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        }
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtEmail{
            self.viewInEmail.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else{
            self.viewInEmail.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        }
        
        return true
        
    }
// MARK: - set Language Labels

    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")

        lblHeader.text! = objLang?["LBL_FORGOR_PASSWORD"] as? String ?? ""
        btnContinue.setTitle(objLang?["LBL_CONTINUE_BTN"] as? String ?? "", for: .normal)
        lblWeJustNeedYour.text! = objLang?["LBL_FORGET_PASS_NOTE"] as? String ?? ""
        txtEmail.placeholder = objLang?["LBL_EMAIL_LBL_TXT"] as? String ?? ""
     
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            self.txtEmail.textAlignment = .right
            self.lblRequiredEmail.textAlignment = .right
            self.viewHeader.semanticContentAttribute = .forceRightToLeft //SMP: LTR to RTL

        }
        
    }
    
    
    
    
    func corner_radius(){
        txtEmail.delegate = self


    }
    
}

//MARK: - Web service calling


extension ForgotPassword{
    
    func post_resetPassword(){
        
        
        ValidationManager.sharedManager.showLoader()

//        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=requestResetPassword&vEmail=\(txtEmail.text!)&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kRequestResetPassword),
                          URLQueryItem(name: "GeneralMemberId", value: ""),
                          URLQueryItem(name: "GeneralAppVersion", value: ""),
                          URLQueryItem(name: "AppVersion", value: ""),
                          URLQueryItem(name: "vUserDeviceCountry", value: ""),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: ""),
                          URLQueryItem(name: "vTimeZone", value: ""),
                          URLQueryItem(name: "vEmail", value: txtEmail.text!)]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                
                if action == "1"{

                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    
                    let text = objLang?["LBL_PASSWORD_SENT_TXT"] as? String ?? ""
                    let btnText = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
                    
                    ValidationManager.sharedManager.showAlertWithAction(title: "", message: text, btnText: btnText , controller: self, completion: {_ in
                        self.view.endEditing(true)
                        self.navigationController?.popViewController(animated: true)
                        
                    })
                    
                }else{

                    
                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    let text = objLang?["LBL_WRONG_EMAIL_PASSWORD_TXT"] as? String ?? ""
                    let btnText = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
                    self.view.endEditing(true)
                    ValidationManager.sharedManager.showAlert(message: text, btnTitle: btnText, controller: self)


                }
                
                
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
}
