//
//  ViewController.swift
//  QbeeCabs Passenger
//
//  Created by mac mini  on 09/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class LogInVC: UIViewController, UITextFieldDelegate {

    var window: UIWindow?
    var navigationcontroller:UINavigationController?
    
    // MARK: - Web service variable

    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    //MARK:- Outlets
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var viewUpEmail: UIView!
    @IBOutlet weak var viewUpPassword: UIView!
    @IBOutlet weak var lblViewUpEmail: UILabel!
    @IBOutlet weak var lblViewUpPassword: UILabel!
    @IBOutlet weak var heightViewUpEmail: NSLayoutConstraint!
    @IBOutlet weak var heightViewUpPassword: NSLayoutConstraint!
    @IBOutlet weak var viewInEmail: UIView!
    @IBOutlet weak var viewInPassword: UIView!
    @IBOutlet weak var viewRequiredEmail: UIView!
    @IBOutlet weak var viewRequiredPassword: UIView!
    @IBOutlet weak var lblRequiredEmail: UILabel!
    @IBOutlet weak var lblRequiredPassword: UILabel!
    @IBOutlet weak var heightVREmail: NSLayoutConstraint!
    @IBOutlet weak var heightVRPassword: NSLayoutConstraint!
    @IBOutlet weak var viewHeader: UIView!

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnLogIn: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!

    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.corner_radius()
        self.setLanguageLabel()
        txtEmail.delegate = self
        txtPassword.delegate = self
        
        
//        txtEmail.text! = "wagonsD@gmail.com"
//        txtPassword.text! = "123456"
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
// MARK: - Button Actions

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLogIn(_ sender: Any) {
        self.view.endEditing(true)
//        self.post_logIn()
        self.empty_textField()
        
        
        //
        
        //        let vc:HomePageVC = self.storyboard?.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
        //        self.navigationController?.pushViewController(vc, animated: true)
        
        //        let sb:UIStoryboard = UIStoryboard(name: "HomeSB", bundle: nil)
        //        let vc = sb.instantiateViewController(withIdentifier: "navHome") as! UINavigationController
        //        self.navigationController?.pushViewController(vc, animated: true)
        //
        
        
    }
    @IBAction func btnForgotPassword(_ sender: Any) {
        self.view.endEditing(true)
        let vc:ForgotPassword = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPassword") as! ForgotPassword
        self.navigationController?.pushViewController(vc, animated: true)
        
        //        let vc:AfterRegistrationVC = self.storyboard?.instantiateViewController(withIdentifier: "AfterRegistrationVC") as! AfterRegistrationVC
        //        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func btnDontHave(_ sender: Any) {
        let vc:RegisterVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Text Field Methods

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail{
            txtPassword.becomeFirstResponder()
        }else if textField == txtPassword{
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtEmail{
            
            self.viewInEmail.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            self.viewInPassword.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            
            self.viewInEmail.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            self.heightVREmail.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtPassword{
            
            self.viewInEmail.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.viewInPassword.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            
            self.heightVRPassword.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
        return true
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtEmail{
            
            //            if string.isEmpty {
            //                return true
            //            }
            
            let currentText = txtEmail.text ?? ""
            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
            
            if replacementText.count > 0{
                heightViewUpEmail.constant = 20
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                
            }else{
                
                heightViewUpEmail.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                txtEmail.placeholder = objLang?["LBL_PHONE_EMAIL_SIGN_IN"] as? String ?? ""
                
            }
            
            
        }else if textField == txtPassword{
            
            let currentText = txtPassword.text ?? ""
            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
            
            if replacementText.count > 0{
                heightViewUpPassword.constant = 20
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                
            }else{
                
                heightViewUpPassword.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                txtPassword.placeholder = objLang?["LBL_PASSWORD_LBL_TXT"] as? String ?? ""
                
            }
            
        }
        
        
        return true
        
        
    }
    
    
    
    
    
    func corner_radius(){
        
        txtEmail.delegate = self
        txtPassword.delegate = self
        
        txtEmail.setLeftPaddingPoints(5)
        txtPassword.setLeftPaddingPoints(5)
        self.height_required()
        
        // txtEmail.text! = "aje078sharma@gmail.com"
        //  txtPassword.text! = "123456"
        
    }
    
    func height_required(){
        heightVREmail.constant = 0
        heightVRPassword.constant = 0
        
        heightViewUpEmail.constant = 0
        heightViewUpPassword.constant = 0

    }
    func empty_textField(){
        if txtEmail.text!.isEmpty{
            self.viewInEmail.backgroundColor = UIColor.red
            self.heightVREmail.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblRequiredEmail.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
            
        }else if !isValidEmail(strEmail: txtEmail.text!){
            self.viewInEmail.backgroundColor = UIColor.red
            self.heightVREmail.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblRequiredEmail.text! = objLang?["LBL_FEILD_EMAIL_ERROR_SIGN_UP"] as? String ?? ""
        }
        if txtPassword.text!.isEmpty{
            self.viewInPassword.backgroundColor = UIColor.red
            self.heightVRPassword.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblRequiredPassword.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
            
        }else if txtPassword.text!.count < 6{
            self.viewInPassword.backgroundColor = UIColor.red
            self.heightVRPassword.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            
            let text1 = objLang?["LBL_RIDER_ERROR_PASS_LENGTH_PREFIX_CHANGE_PASSWORD"] as? String ?? ""
            let text2 = objLang?["LBL_RIDER_ERROR_PASS_LENGTH_SUFFIX_CHANGE_PASSWORD"] as? String ?? ""

            lblRequiredPassword.text! = text1 + " 6 " + text2
            
        }else{
            
            if CheckInternet.connection(){
                self.post_logIn()
            }else{
                let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                let textAlert = objLang?["LBL_RIDER_NO_INTERNET_MSG_GENERAL"] as? String ?? ""
                let textOk = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
                ValidationManager.sharedManager.showAlert(message: textAlert, title: "", btnTitle: textOk, controller: self)
            }
            
            
        }
    }
    
    func navigate_home(){
//        let sb: UIStoryboard = UIStoryboard(name: "HomeSB", bundle:Bundle.main)
//        navigationcontroller = sb.instantiateViewController(withIdentifier: "navHome") as? UINavigationController
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window?.rootViewController = navigationcontroller
//        window?.makeKeyAndVisible()
        
        
        let sb: UIStoryboard = UIStoryboard(name: "HomeSB", bundle:Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navSession") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationcontroller
        window?.makeKeyAndVisible()
        
    }
    
    

    
    
    func isValidEmail(strEmail:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: strEmail)
        return result
    
    }
    

    
// MARK: - set Language Labels

    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_SIGN_IN_TXT"] as? String ?? ""
        txtEmail.placeholder = objLang?["LBL_PHONE_EMAIL_SIGN_IN"] as? String ?? ""
        txtPassword.placeholder = objLang?["LBL_PASSWORD_LBL_TXT"] as? String ?? ""
        lblViewUpEmail.text! = objLang?["LBL_PHONE_EMAIL_SIGN_IN"] as? String ?? ""
        lblViewUpPassword.text! = objLang?["LBL_PASSWORD_LBL_TXT"] as? String ?? ""
        btnLogIn.setTitle(objLang?["LBL_SIGN_IN_TXT"] as? String ?? "", for: .normal)
        btnForgotPassword.setTitle(objLang?["LBL_FORGET_PASS_TXT"] as? String ?? "", for: .normal)

        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            self.txtEmail.textAlignment = .right
            self.txtPassword.textAlignment = .right
            self.lblViewUpEmail.textAlignment = .right
            self.lblViewUpPassword.textAlignment = .right
            self.lblRequiredEmail.textAlignment = .right
            self.lblRequiredPassword.textAlignment = .right
            self.viewHeader.semanticContentAttribute = .forceRightToLeft //SMP: LTR to RTL

        }
        
        
    }
    
    
}





// MARK: - Webservice calling

extension LogInVC{
    
    func post_logIn(){
        
        UserDefaults.standard.removeObject(forKey: "tSessionId")
        UserDefaults.standard.removeObject(forKey: "iDriverId")
        UserDefaults.standard.removeObject(forKey: "vName")
        UserDefaults.standard.removeObject(forKey: "vLastName")
        UserDefaults.standard.removeObject(forKey: "vCountry")
        UserDefaults.standard.removeObject(forKey: "vImage")

        ValidationManager.sharedManager.showLoader()
        
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&vLang=EN&vDeviceType=ios&type=signIn&GeneralUserT ype=Driver&tSessionId=&GeneralDeviceType=ios&GeneralMemberId=&vUserDeviceCountry=US&vDeviceToken=\(kDeviceToken)&vCurrency=&vTimeZone=Asia/Kolkata&vEmail=\(txtEmail.text!)&UserType=Driver&vPassword=\(txtPassword.text!)"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kSignIn),
                          URLQueryItem(name: "GeneralMemberId", value: ""),
                          URLQueryItem(name: "vFirebaseDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "vDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "vDeviceType", value: kDeviceType),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: ""),
                          URLQueryItem(name: "vCurrency", value: ""),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone),
                          URLQueryItem(name: "vEmail", value: self.txtEmail.text),
                          URLQueryItem(name: "vPassword", value: self.txtPassword.text)]

        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue

                if action == "1"{
                    if let message = json["message"].dictionaryObject{
                        _ = MUserDetaiils.init(dict: message)

                        self.navigate_home()

                    }
                }else{
                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    let textAlert = objLang?["LBL_WRONG_EMAIL_PASSWORD_TXT"] as? String ?? ""
                    let textOk = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
                    ValidationManager.sharedManager.showAlert(message: textAlert, title: "", btnTitle: textOk, controller: self)

                    
                    ValidationManager.sharedManager.hideLoader()

                }
                


                ValidationManager.sharedManager.hideLoader()


                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()

            }
        }
        
    }
    
}
