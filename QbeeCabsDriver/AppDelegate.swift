//
//  AppDelegate.swift
//  QbeeCabsDriver
//
//  Created by mac mini  on 21/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import UserNotifications
import GoogleMaps
import GooglePlaces
import Firebase
import FirebaseCore
import FirebaseMessaging
import FirebaseInstanceID
import FirebaseAuth
import FirebaseAnalytics


let kGoogleApiKey = "AIzaSyCf0LjzuQ1WhqygSTQhVAkXvuGQ-F9S190"
var kDeviceToken : String = ""


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var navigationcontroller: UINavigationController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        GMSServices.provideAPIKey(kGoogleApiKey)
        GMSPlacesClient.provideAPIKey(kGoogleApiKey)
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.tokenRefreshNotification),name: NSNotification.Name.InstanceIDTokenRefresh,object: nil)

        FirebaseApp.configure()

        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                
                let refreshedToken = result.token
                print("InstanceID token: \(refreshedToken)")
                self.strNotificationToken = refreshedToken
                kDeviceToken = self.strNotificationToken
                print("strNotificationToken: \(self.strNotificationToken)")
                self.navigate_session()

            }
        }
        
        
        
        
        
        
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    var strNotificationToken = ""

    @objc func tokenRefreshNotification(notification: NSNotification) {
        /*
         if InstanceID.instanceID().token() != nil {
         let refreshedToken = InstanceID.instanceID().token()!
         print("InstanceID token: \(refreshedToken)")
         strNotificationToken = refreshedToken
         print("apnsToken: \(strNotificationToken)")
         } else {
         connectToFcm()
         }
         */
        //New Methods
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
                self.connectToFcm()
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                
                let refreshedToken = result.token
                //print("InstanceID token: \(refreshedToken)")
                self.strNotificationToken = refreshedToken
                print("strNotificationToken: \(self.strNotificationToken)")
            }
        }
    }
    
    func connectToFcm() {
        /*
         Messaging.messaging().connect { (error) in
         if (error != nil) {
         print("Unable to connect with FCM. \(error)")
         } else {
         print("Connected to FCM.")
         }
         }
         */
        if Messaging.messaging().isDirectChannelEstablished {
            print("Connected to FCM.")
        } else {
            print("Disconnected from FCM.")
            //print("Unable to connect with FCM. \(error)")
        }
    }
    
    func navigate_session(){
        let sb:UIStoryboard = UIStoryboard(name: "HomeSB", bundle: Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navSession") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationcontroller
        window?.makeKeyAndVisible()
        
    }
    
    func navigate_home(){
        let sb:UIStoryboard = UIStoryboard(name: "HomeSB", bundle: Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navHome") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationcontroller
        window?.makeKeyAndVisible()
    }

}

extension AppDelegate {
    
    //MARK: - Remote Notification Get Device token methods.
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken as Data
        print("didRegisterForRemoteNotificationsWithDeviceToken = \(deviceToken)")
        //For SINCH MANAGED PUSH
        /*
         if let push = self.push{
         push.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
         }
         */
        
//        InstanceID.instanceID().instanceID { (result, error) in
//        if let error = error {
//        print("Error fetching remote instange ID: \(error)")
//        } else if let result = result {
//        print("Remote instance ID token: \(result.token)")
//         }
//        }
//
//
//
//        Messaging.messaging().shouldEstablishDirectChannel = true
//
//
//        let token = InstanceID.instanceID()
        
        
        
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                
                let refreshedToken = result.token
                print("InstanceID token: \(refreshedToken)")
                self.strNotificationToken = refreshedToken
                print("strNotificationToken: \(self.strNotificationToken)")
            }
        }
        
        
        
//        if let refreshedToken = InstanceID.instanceID().token() {
//            print("InstanceID token: \(refreshedToken)")
//        }
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        kDeviceToken = fcmToken
        print("didReceiveRegistrationToken = \(fcmToken)")
//        self.updateFirebaseToken(strToken: fcmToken)
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        kDeviceToken = fcmToken
//        ConnectToFCM()
    }
    
//    func tokenRefreshNotification(_ notification: Notification) {
//        if let refreshedToken = InstanceID.instanceID().token(withAuthorizedEntity: "", scope: "") {
//            kDeviceToken = refreshedToken
//        }
//        ConnectToFCM()
//    }
    
    // Receive data message on iOS 10 devices while app is in the foreground.
    @objc(applicationReceivedRemoteMessage:) func application(received remoteMessage: MessagingRemoteMessage) {
    }
    
//    func ConnectToFCM() {
//        if let refreshedToken = InstanceID.instanceID().token() {
//            kDeviceToken = refreshedToken
//            print(refreshedToken)
//
//        }
//    }
    

}

//MARK: - Register for Remote notification
private extension AppDelegate{
    
    func registerForRemoteNotification() {
        // iOS 10 support
        if #available(iOS 10, *) {
            let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options:authOptions){ (granted, error) in
                UNUserNotificationCenter.current().delegate = self
                Messaging.messaging().delegate = self
            }
            UIApplication.shared.registerForRemoteNotifications()
        }else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    
    
}
