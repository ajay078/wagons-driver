//
//  SelectDocumentUploadVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 11/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SelectDocumentUploadVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var imagePicker = UIImagePickerController()
    var imgUploadProfile:UIImage? = nil

    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var imgCreditCard: UIImageView!
    
    @IBOutlet weak var txtSelectedDate: UITextField!
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var viewDatePicker: UIView!
    
    
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewDateIn: UIView!
    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewMessageIn: UIView!
    @IBOutlet var btnMessageOk: UIButton!
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet var viewButtonL: UIView!
    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.corner_radius()
//        showDatePicker()
        imagePicker.delegate = self

        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }
    
// MARK: - Button Actions

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpload(_ sender: Any) {
        self.camera_photo()
    }
    @IBAction func btnCancel(_ sender: Any) {
        self.view.endEditing(true)
        self.viewDate.isHidden = true
    }
    
    @IBAction func btnOk(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        txtSelectedDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
        self.viewDate.isHidden = true
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        self.viewMessage.isHidden = false
        
        if self.imgUploadProfile != nil{
            let iDriverVehicleId = UserDefaults.standard.string(forKey: "iDriverVehicleId") ?? ""
            
            
            // https://apps.wagonstaxi.com/webimages/upload/documents/driver/2/TempFile_20200828062253.jpg
            
            let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
            let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    
            
            let urlImage = "https://apps.wagonstaxi.com/webservice.php?type=uploaddrivedocument&iMemberId=\(userId)&MemberType=Driver&doc_usertype=vehicle&doc_masterid=1&doc_name=License&iDriverVehicleId=\(iDriverVehicleId)&iDriverId=\(userId)"
            
//            let urlImage = "https://apps.wagonstaxi.com/webservice.php?iDriverId=\(userId)&GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&type=uploadVehicleImage&UserType=Driver&GeneralUserType=Driver&GeneralDeviceType=ios&tSessionId=\(sessionId)&iDriverVehicleId=\(iDriverVehicleId)&doc_usertype=vehicle"
            
//            let urlImage = "http://apps.wagonstaxi.com/webservice.php?type=uploaddrivedocument&iMemberId=\(userId)&MemberType=Driver&doc_usertype=Driver&doc_masterid=1&doc_name=driving license&doc_id=7&displayDocList&tSessionId=\(sessionId)&GeneralUserType=Driver&GeneralMemberId=\(userId)&ex_date=&doc_file&EXPIRE_DOCUMENT=no&ex_status=no&&iDriverVehicleId=\(iDriverVehicleId)"
            
//        let urlImage = "https://apps.wagonstaxi.com/webservice.php?iDriverId=\(userId)&GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&type=uploaddrivedocument&UserType=Driver&GeneralUserType=Driver&GeneralDeviceType=Android&tSessionId=\(sessionId)&iDriverVehicleId=\(iDriverVehicleId)"
            
//    let urlImage = "https://apps.wagonstaxi.com/webservice.php?type=uploadVehicleImage&iMemberId=\(userId)&MemberType=Driver&doc_usertype=vehicle&doc_masterid=1&doc_name=License&doc_id=&tSessionId=\(sessionId)&GeneralUserType=Driver&GeneralMemberId=\(userId)&ex_date=2021-08-28&iDriverVehicleId=\(iDriverVehicleId)&doc_file"
            
            
            let newURL = urlImage.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            let parameter:[String:Any] = [:]
            let headers = ["Authorization" : "Bearer",
                           "Content-Type": "application/json"]
            self.uploadPhoto(newURL!, image: self.imgUploadProfile!, params: parameter, header: headers)
        }
        

        
    }
    
    @IBAction func btnMessageOk(_ sender: Any) {
        self.viewMessage.isHidden = true
    }
    
    
    func corner_radius(){
        self.viewMessage.isHidden = true
        self.viewMessageIn.layer.cornerRadius = 5
        self.viewMessageIn.layer.masksToBounds = true
        
        self.viewDateIn.layer.cornerRadius = 5
        self.viewDateIn.layer.masksToBounds = true
        
        
    }
    
    func uploadPhoto(_ url: String, image: UIImage, params: [String : Any], header: [String:String]) {
        let httpHeaders = HTTPHeaders(header)
        AF.upload(multipartFormData: { multiPart in
            for p in params {
                multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
            }
            multiPart.append(image.jpegData(compressionQuality: 0.4)!, withName: "vImage", fileName: "file.jpg", mimeType: "image/jpg")
        }, to: url, method: .post, headers: httpHeaders) .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
        }).responseJSON(completionHandler: { data in
            print("upload finished: \(data)")
        }).response { (response) in
            switch response.result {
            case .success(let resut):
                print("upload success result: \(String(describing: resut))")
            case .failure(let err):
                print("upload err: \(err)")
            }
        }
    }
    
    func camera_photo(){
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
//        if imgCreditCard.image == UIImage(named: "profile placeHolder"){
            imgCreditCard.image = image
            self.imgUploadProfile = image

            //            self.imgProfile = UIImagePNGRepresentation(imgProfile.image!) as UIImage
            
//        }
        dismiss(animated: true, completion: nil)
        
    }
    
    
//     let datePicker = UIDatePicker()



//     func showDatePicker(){
//       //Formate Date
//       datePicker.datePickerMode = .date
//
//      //ToolBar
//      let toolbar = UIToolbar();
//      toolbar.sizeToFit()
//      let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//     let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
//
//    toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
//
//     txtSelectedDate.inputAccessoryView = toolbar
//     txtSelectedDate.inputView = datePicker
//
//    }
//
//     @objc func donedatePicker(){
//
//      let formatter = DateFormatter()
//      formatter.dateFormat = "dd/MM/yyyy"
//      txtSelectedDate.text = formatter.string(from: datePicker.date)
//      self.view.endEditing(true)
//    }
//
//    @objc func cancelDatePicker(){
//       self.view.endEditing(true)
//     }
    
    
    
    
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_SELECT_DOC_LIST_OF_DOCUMENT"] as? String ?? ""
        lblMessage.text! = objLang?["LBL_UPLOAD_DOC_SUCCESS"] as? String ?? ""
        btnMessageOk.setTitle(objLang?["LBL_BTN_OK_TXT"] as? String ?? "", for: .normal)


        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft
            self.viewButtonL.semanticContentAttribute = .forceRightToLeft
            self.lblMessage.textAlignment = .right
            
            
        }
        
    }
    
    
    
    
}
