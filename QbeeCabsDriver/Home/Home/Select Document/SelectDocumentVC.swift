//
//  SelectDocumentVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 22/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SelectDocumentVC: UIViewController {

    var iDriverVehicleId = ""
    var arrDocList = [[String:Any]]()
    var imageUrl = ""
    
    @IBOutlet weak var heightViewDocument: NSLayoutConstraint!
    @IBOutlet weak var btnManage: UIButton!
    
    
    @IBOutlet weak var lblExpired: UILabel!
    @IBOutlet weak var heightExpired: NSLayoutConstraint!
    
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var viewDown: UIView!
    
    @IBOutlet weak var viewManageDocument: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewImgDocument: UIView!
    @IBOutlet weak var viewImgDocumentIn: UIView!
    @IBOutlet weak var imgDocumentPic: UIImageView!
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var lblDrivingLicenceL: UILabel!
   
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.corner_radius()
        self.post_documentList()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnDown(_ sender: Any) {
        
        if heightViewDocument.constant == 0 {
            btnManage.isHidden = false
            heightViewDocument.constant = 80
            imgArrow.image = UIImage(named: "up-arrow (1)")
        }else{
            heightViewDocument.constant = 0
            btnManage.isHidden = true
            imgArrow.image = UIImage(named: "down-arrow (4)")

        }


        
    }
    @IBAction func btnManage(_ sender: Any) {

        let vc:SelectDocumentUploadVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectDocumentUploadVC") as! SelectDocumentUploadVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnOpenDoc(_ sender: Any) {
        self.viewImgDocument.isHidden = false
    }
    @IBAction func btnHideDoc(_ sender: Any) {
        self.viewImgDocument.isHidden = true
    }
    

    
    
    func details(){
        
        DispatchQueue.global(qos: .background).async {
            do
            {
                let url = URL(string: self.imageUrl)
                if let data = try? Data(contentsOf: url!){
                    DispatchQueue.main.async {
                        self.imgDocumentPic.image = UIImage(data: data)

                    }
                }

            }

        }
    }
    
    func corner_radius(){
        self.viewImgDocument.isHidden = true
        heightViewDocument.constant = 0
        btnManage.isHidden = true
        viewDown.layer.cornerRadius = 5
        viewDown.layer.masksToBounds = true
        viewManageDocument.layer.cornerRadius = 5
        viewManageDocument.layer.masksToBounds = true
        imgArrow.image = UIImage(named: "down-arrow (4)")
        heightExpired.constant = 0
        
        
        
    }
    
    
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_SELECT_DOC_LIST_OF_DOCUMENT"] as? String ?? ""
        lblExpired.text! = objLang?["LBL_EXPIRED_TXT"] as? String ?? ""
        btnManage.setTitle(objLang?["LBL_MANAGE"] as? String ?? "", for: .normal)

        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft
            self.viewMain.semanticContentAttribute = .forceRightToLeft

            
        }
        
    }
    
}

// MARK: - Webservice calling

extension SelectDocumentVC{
    
    func post_documentList(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        self.iDriverVehicleId = UserDefaults.standard.string(forKey: "iDriverVehicleId") ?? ""
        
        ValidationManager.sharedManager.showLoader()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&iMemberId=\(userId)&vTimeZone=Asia/Kolkata&doc_usertype=driver&iDriverVehicleId=\(iDriverVehicleId)&type=displayDocList&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios"
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&iMemberId=\(userId)&vTimeZone=Asia/Kolkata&doc_usertype=vehicle&iDriverVehicleId=\(iDriverVehicleId)&type=displayDocList&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
        
        //
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    
                    if let message = json["message"].arrayObject{
                        for index in 0..<message.count{
                            let obj = message[index] as? [String:Any]
                            self.arrDocList.append(obj ?? [:])
                        }
                        let obj = self.arrDocList[0]
                        self.imageUrl = obj["vimage"] as? String ?? ""
                    }
                    
                    self.details()
                }
                
                
                ValidationManager.sharedManager.hideLoader()
                
                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
}
