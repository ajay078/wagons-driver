//
//  HomePageVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 21/04/20.
//  Copyright © 2020 Blips. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON


class HomePageVC: UIViewController, SWRevealViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {

    var arrEmergencyContactList = [ModalEmergencyContactList]()
    var arrSelectVehicle = [ModalHomeSelectVehicle]()
    var arrFilteredSelectVehicle = [ModalHomeSelectVehicle]()
    var strCurrentLat:String = ""
    var strCurrentLong:String = ""
    var zoomLevel:Float = 14
    
    var window: UIWindow?
    var navigationcontroller:UINavigationController?
    
    var timerAvailable = Timer()
    var timerGetRequest = Timer()
    
    
    var strVehicleNo = ""
    var swipeGesture = UISwipeGestureRecognizer()
    var bottomConstantEmergencyCall : CGFloat = 0
    var bottomConstantContactList : CGFloat = 0

    var passengerId = ""
    var pName = ""
    var pRating = ""
    var pSourceLatitude = ""
    var pSourceLongitude = ""
    
    var pDestLatitude = ""
    var pDestLongitude = ""
    var pITripId = ""
    var pICabRequestId:Int = 0
    var pMsgCode = ""
    
    var iCabBookingId = ""
    
    
    // MARK: - Web service variable

    let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    // MARK: - Outlets


    @IBOutlet weak var viewFullpage: UIView!
    @IBOutlet weak var viewSelectCar: UIView!
    @IBOutlet weak var viewSelectCarIn: UIView!
    
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblVehicleNo: UILabel!
    @IBOutlet weak var lblSelectedVehicle: UILabel!
    @IBOutlet weak var lblOffline: UILabel!
    @IBOutlet weak var switchOffline: UISwitch!
    @IBOutlet weak var btnChange: UIButton!
    
    @IBOutlet weak var tableViewSelectVehicle: UITableView!
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    @IBOutlet weak var tableViewContactList: UITableView!
    
    @IBOutlet weak var viewMidLogo: UIView!
    
    @IBOutlet weak var viewEmergencyCall: UIView!
    @IBOutlet weak var bottomViewEmergencyCall: NSLayoutConstraint!

    @IBOutlet weak var viewEmergencyContactList: UIView!
    @IBOutlet weak var bottomViewEmergencyContactList: NSLayoutConstraint!
    
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var viewLogoutConfirm: UIView!
    @IBOutlet weak var lblLogOutL: UILabel!
    @IBOutlet weak var lblAreYourSure: UILabel!
    @IBOutlet weak var btnNoL: UIButton!
    @IBOutlet weak var btnYesL: UIButton!
    @IBOutlet var viewButtonL: UIView!
    
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var viewVehicleDetails: UIView!
    @IBOutlet weak var lblSelectYourCar: UILabel!
    @IBOutlet weak var btnManageVehicles: UIButton!
    @IBOutlet weak var btnAddNew: UIButton!
    @IBOutlet weak var viewTripAssigned: UIView!
    @IBOutlet weak var imgTripAssigned: UIImageView!
    @IBOutlet weak var lblTripAssigned: UILabel!
    
    @IBOutlet weak var leadingViewBtnMenu: NSLayoutConstraint!
    @IBOutlet weak var trailingViewBtnMenu: NSLayoutConstraint!
    @IBOutlet weak var leadingViewButtonCall: NSLayoutConstraint!
    @IBOutlet weak var trailingViewButtonCall: NSLayoutConstraint!
    
    @IBOutlet weak var mapView: GMSMapView!
    
// MARK: - View Did Load

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()

        let tripStatus = UserDefaults.standard.string(forKey: "TripStatus")
        let ratingFromDriver = UserDefaults.standard.string(forKey: "Ratings_From_Driver")
        let iActive = UserDefaults.standard.string(forKey: "iActive")
        let paymentCollect = UserDefaults.standard.string(forKey: "ePaymentCollect")
        
        if tripStatus == "Active"{
            self.navigate_enRoute()
        }else if tripStatus == "Arrived"{
            self.navigate_enRoute()
        }else if tripStatus == "On Going Trip"{
            self.navigate_enRoute()
        }else if iActive == "Finished" && paymentCollect == "No"{
            self.navigate_finalFare()
        }else if iActive == "Finished" && ratingFromDriver == "Not Done"{
            self.navigate_rating()
        }else{
            self.post_selectVehicle()
            self.post_emergencyContact()
            self.corner_radius()
            self.viewSelectCar.isHidden = true
            
            self.swipeUptoEmergencyCall()
            self.swipeUptoEmergencyContactList()
            
            bottomViewEmergencyCall.constant = -500
            bottomViewEmergencyContactList.constant = -500
            
            let langCode = UserDefaults.standard.string(forKey: "default_language")
            if langCode == "AR"{
                btnMenu.addTarget(revealViewController, action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
            }else{
                btnMenu.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                
            }
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.showViewLogout(_:)), name: NSNotification.Name(rawValue: "isViewLogoutPopShow"), object: nil)
            

            
            
 
        }

        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 5)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PlacePicker.shared.locationAuthorization(controller: self, success: { (placeDict) in
            print("place info = \(placeDict)")
            self.strCurrentLat = placeDict["lat"] as? String ?? "0"
            self.strCurrentLong = placeDict["long"] as? String ?? "0"
            let StrAdd = placeDict["add"] as? String ?? ""
            print(StrAdd)
            self.timerAvailable = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
            self.timerGetRequest = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.updateGetRequest), userInfo: nil, repeats: true)
            self.my_current_location()
        }) { (error) in
            print("error = \(error.localizedDescription)")
        }
        
        self.profile_image()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timerAvailable.invalidate()
        self.timerGetRequest.invalidate()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }
    
    
// MARK: - Run Loop Method

    @objc func update() {
        self.post_showAvailable()
    }
    
    @objc func updateGetRequest() {
//        self.post_getRequest()
        self.post_getRequestFromDispatcher()
        
        
        
    }
    
    @objc func showViewLogout(_ notification: NSNotification) {
           print(notification.userInfo ?? "")
           if let dict = notification.userInfo as NSDictionary? {
               if let isFromLogout = dict["isLogoutPopUp"] as? Bool{
                   if isFromLogout == true{
                    ValidationManager.sharedManager.showMainViewNN(viewMain: viewLogout, viewInside: viewLogoutConfirm)
                        

                   }
               }
           }
    }
    @IBAction func btnTest(_ sender: Any) {
        
        let vc:AcceptRequestVC = self.storyboard?.instantiateViewController(withIdentifier: "AcceptRequestVC") as! AcceptRequestVC
        vc.passengerId = self.passengerId
        vc.pName = self.pName
        vc.pRating = self.pRating
        vc.pSourceLatitude = self.pSourceLatitude
        vc.pSourceLongitude = self.pSourceLongitude
        vc.pDestLatitude = self.pDestLatitude
        vc.pDestLongitude = self.pDestLongitude
        vc.pICabRequestId = self.pICabRequestId
        vc.pMsgCode = self.pMsgCode
        self.navigationController?.pushViewController(vc, animated: true)
        
//        let vc:EnRouteVC = self.storyboard?.instantiateViewController(withIdentifier: "EnRouteVC") as! EnRouteVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func switchOffline(_ sender: Any) {
        if switchOffline.isOn{
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblOffline.text! = objLang?["LBL_GO_OFFLINE_TXT"] as? String ?? ""
            
            isOffline = false
            self.timerAvailable = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
            self.timerGetRequest = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.post_getRequest), userInfo: nil, repeats: true)
            

        }else{
            isOffline = true
            
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblOffline.text! = objLang?["LBL_GO_ONLINE_TXT"] as? String ?? ""
            
            self.timerAvailable.invalidate()
            self.timerGetRequest.invalidate()

        }
        
    }
    
    // MARK: - Button Actions

    
    @IBAction func btnMenu(_ sender: Any) {
//        viewFullpage.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

    }
    @IBAction func btnChangeVehicle(_ sender: Any) {
        self.updateTableViewHeightConstraint(arrayCount: arrFilteredSelectVehicle.count)
        ValidationManager.sharedManager.showMainViewNN(viewMain: viewSelectCar, viewInside: viewSelectCarIn)
//        self.viewSelectCar.isHidden = false

    }
    @IBAction func btnHideViewChangeVehicle(_ sender: Any) {
//        self.viewSelectCar.isHidden = true
        ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: viewSelectCar, viewPopUP: viewSelectCarIn)
    }
    
    
    @IBAction func btnAddNew(_ sender: Any) {
        let vc:AddVehicleVC = self.storyboard?.instantiateViewController(withIdentifier: "AddVehicleVC") as! AddVehicleVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnManageVehicle(_ sender: Any) {
        let vc:ManageVehicleVC = self.storyboard?.instantiateViewController(withIdentifier: "ManageVehicleVC") as! ManageVehicleVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnEmergencyCall(_ sender: Any) {
        bottomViewEmergencyCall.constant = 0
        UIView.animate(withDuration: 0.3){
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func btnEmergencyContact(_ sender: Any) {
        bottomViewEmergencyContactList.constant = 0
        UIView.animate(withDuration: 0.3){
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnLogoutYes(_ sender: Any) {
        
        UserDefaults.standard.removeObject(forKey: "tSessionId")
        UserDefaults.standard.removeObject(forKey: "iDriverId")
        UserDefaults.standard.removeObject(forKey: "vName")
        UserDefaults.standard.removeObject(forKey: "vLastName")
        UserDefaults.standard.removeObject(forKey: "vCountry")
        UserDefaults.standard.removeObject(forKey: "vImage")
        UserDefaults.standard.removeObject(forKey: "vCurrencyDriver")

        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle:Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navMain") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationcontroller
        window?.makeKeyAndVisible()
        
    }
    @IBAction func btnLogoutNo(_ sender: Any) {
        ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: viewLogout, viewPopUP: viewLogoutConfirm)

    }
    
// MARK: - TableView Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableViewSelectVehicle{
            return arrFilteredSelectVehicle.count
        }else{
            return self.arrEmergencyContactList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewSelectVehicle{
            let cell:HSelectVehicleCell = tableView.dequeueReusableCell(withIdentifier: "HSelectVehicleCell", for: indexPath) as! HSelectVehicleCell
            let obj = arrFilteredSelectVehicle[indexPath.row]
            cell.lblVehicleName.text! = "\(obj.vMake) \(obj.vTitle)"
            
            return cell
        }else{
            let cell:EmergencyContactCell = tableView.dequeueReusableCell(withIdentifier: "EmergencyContactCell", for: indexPath) as! EmergencyContactCell
            
            let obj = arrEmergencyContactList[indexPath.row]
            
            cell.lblName.text! = obj.strName
            cell.lblNumber.text! = obj.strPhone
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewSelectVehicle{
            let obj = arrFilteredSelectVehicle[indexPath.row]
            lblSelectedVehicle.text! = "\(obj.vMake) \(obj.vTitle)"
            lblVehicleNo.text! = "\(obj.vLicencePlate)"
            
            UserDefaults.standard.set("\(obj.vMake) \(obj.vTitle)", forKey: "selectedVehicle")
            UserDefaults.standard.set("\(obj.vLicencePlate)", forKey: "vehicleNo")
//            self.viewSelectCar.isHidden = true
            ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: viewSelectCar, viewPopUP: viewSelectCarIn)
        }else{
            
            let obj = arrEmergencyContactList[indexPath.row]
            let number = obj.strPhone
            
            let strNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
            
            //  UIApplication.shared.open(NSURL(string: "tel://1234567891")! as URL)
            UIApplication.shared.open(NSURL(string: "tel://\(strNumber)")! as URL)
            
        }
        
    }
    

    
    

   
    
    

    

    
    
    
    
//    @objc func post_getRequest(){
//
//        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
//        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
//        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
//
////        let urlString = "http://app.wagonstaxi.com/webservice.php?GeneralAppVersion=1.0.4&vLongitude=\(self.strCurrentLong)&iMemberId=\(userId)&type=configDriverTripStatus&GeneralUserType=Driver&isSubsToCabReq=false&tSessionId=\(sessionId)&GeneralDeviceType=Android&GeneralMemberId=\(userId)&vLatitude=\(self.strCurrentLat)&iTripId=&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&UserType=Driver"
//
//        let urlString = "http://app.wagonstaxi.com/webservice.php?GeneralAppVersion=1.0.4&vLongitude=\(self.strCurrentLong)&iMemberId=\(userId)&type=configDriverTripStatus&GeneralUserType=Driver&isSubsToCabReq=false&tSessionId=\(sessionId)&GeneralDeviceType=Android&GeneralMemberId=\(userId)&vLatitude=\(self.strCurrentLat)&iTripId=&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&UserType=Driver"
//
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
//
//        let headers:HTTPHeaders = ["Authorization" : "Bearer",
//                                   "Content-Type": "application/json"]
//
//        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
//
//
//            switch responseObject.result {
//            case .success(let value):
//                let json = JSON(value)
//                print(json)
//
//
//
//                let action = json["Action"].stringValue
//                print(action)
//                if action == "1"{
//                    self.activityIndicatorView.startAnimating()
//                    self.timerAvailable.invalidate()
//                    self.timerGetRequest.invalidate()
//
//                    let message = json["message"].arrayValue
//                    let obj = message[0].string
//
//
//                    let data = obj!.data(using: .utf8)!
//                    do {
//
//                        if let jsonDict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
//
//                        {
//                            self.pName = jsonDict["PName"] as? String ?? ""
//                            self.passengerId = jsonDict["PassengerId"] as? String ?? ""
//                            self.pRating = jsonDict["PRating"] as? String ?? ""
//                            self.pSourceLatitude = jsonDict["sourceLatitude"] as? String ?? ""
//                            self.pSourceLongitude = jsonDict["sourceLongitude"] as? String ?? ""
//                            self.pDestLatitude = jsonDict["destLatitude"] as? String ?? ""
//                            self.pDestLongitude = jsonDict["destLongitude"] as? String ?? ""
//                            self.pICabRequestId = jsonDict["iCabRequestId"] as? Int ?? 0
//                            self.pMsgCode = jsonDict["MsgCode"] as? String ?? ""
//
//                           print(jsonDict) // use the json here
//                        } else {
//                            print("bad json")
//                        }
//                    } catch let error as NSError {
//                        print(error)
//                    }
//
//
//                    let vc:AcceptRequestVC = self.storyboard?.instantiateViewController(withIdentifier: "AcceptRequestVC") as! AcceptRequestVC
//                    vc.passengerId = self.passengerId
//                    vc.pName = self.pName
//                    vc.pRating = self.pRating
//                    vc.pSourceLatitude = self.pSourceLatitude
//                    vc.pSourceLongitude = self.pSourceLongitude
//                    vc.pDestLatitude = self.pDestLatitude
//                    vc.pDestLongitude = self.pDestLongitude
//                    vc.pICabRequestId = self.pICabRequestId
//                    vc.pMsgCode = self.pMsgCode
//                    self.navigationController?.pushViewController(vc, animated: true)
//
//                    self.activityIndicatorView.stopAnimating()
//
//                }
//
//
//                self.activityIndicatorView.stopAnimating()
//
//
//
//            case .failure(let error):
//                print(error)
//                self.activityIndicatorView.stopAnimating()
//
//            }
//        }
//
//    }
    
    

    
    func profile_details(){
        
//        self.lblSelectedVehicle.text! = UserDefaults.standard.string(forKey: "selectedVehicle") ?? ""
//        self.lblVehicleNo.text! = UserDefaults.standard.string(forKey: "vehicleNo") ?? ""
                
    }
    
    func profile_image(){
        let imageUrlId = "https://apps.wagonstaxi.com/webimages/upload/Driver/"
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let imgUserId:String = UserDefaults.standard.string(forKey: "vImage") ?? ""
        
        let imageUrl = imageUrlId+"\(userId)/"+imgUserId
        print(imageUrl)
        DispatchQueue.global(qos: .background).async {
            //            do
            //            {
            let url = URL(string: imageUrl)
            if let data = try? Data(contentsOf: url!){
                DispatchQueue.main.async {
                    self.imgProfile.image = UIImage(data: data)
                    
                }
            }
            
            //            }
            
        }
    }
    
    func navigate_enRoute(){
        let vc:EnRouteVC = self.storyboard?.instantiateViewController(withIdentifier: "EnRouteVC") as! EnRouteVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func navigate_rating(){
        let vc:RatingVC = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func navigate_finalFare(){
        let vc:FinalFareVC = self.storyboard?.instantiateViewController(withIdentifier: "FinalFareVC") as! FinalFareVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        
        lblLogOutL.text! = objLang?["LBL_SIGNOUT_TXT"] as? String ?? ""
        lblAreYourSure.text! = objLang?["LBL_WANT_LOGOUT_APP_TXT"] as? String ?? ""
        btnNoL.setTitle(objLang?["LBL_NO"] as? String ?? "", for: .normal)
        btnYesL.setTitle(objLang?["LBL_YES"] as? String ?? "", for: .normal)
        lblOffline.text! = objLang?["LBL_GO_OFFLINE_TXT"] as? String ?? ""
        btnChange.setTitle(objLang?["LBL_CHANGE"] as? String ?? "", for: .normal)
        lblSelectYourCar.text! = objLang?["LBL_SELECT_CAR_TXT"] as? String ?? ""
        btnManageVehicles.setTitle(objLang?["LBL_MANAGE_VEHICLES"] as? String ?? "", for: .normal)
        btnAddNew.setTitle(objLang?["LBL_ADD_VEHICLES"] as? String ?? "", for: .normal)
        
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            leadingViewBtnMenu.isActive = false
            trailingViewBtnMenu.isActive = true
            trailingViewBtnMenu.constant = 0
            self.viewVehicleDetails.semanticContentAttribute = .forceRightToLeft
            self.viewButtonL.semanticContentAttribute = .forceRightToLeft

            lblLogOutL.textAlignment = .right
            lblAreYourSure.textAlignment = .right
            lblVehicleNo.textAlignment = .right
            lblSelectedVehicle.textAlignment = .right
            btnChange.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.right

            
        }else{
            leadingViewBtnMenu.isActive = true
            trailingViewBtnMenu.isActive = false
        }
        
        



        
    }
    
    func corner_radius(){
        
        let vMake = UserDefaults.standard.string(forKey: "vMake") ?? ""
        let vModal = UserDefaults.standard.string(forKey: "vModel") ?? ""
        
        self.lblSelectedVehicle.text! = "\(vMake) \(vModal)"
        self.lblVehicleNo.text! = UserDefaults.standard.string(forKey: "vLicencePlateNo") ?? ""
        
        
        imgProfile.layer.cornerRadius = imgProfile.frame.size.height/2
        viewMidLogo.layer.cornerRadius = viewMidLogo.frame.size.height/2
        imgTripAssigned.layer.cornerRadius = imgProfile.frame.size.height/2

        viewSelectCarIn.layer.cornerRadius = 5
        viewSelectCarIn.layer.masksToBounds = true
        
        viewLogout.isHidden = true
        viewTripAssigned.isHidden = true

        viewLogoutConfirm.layer.cornerRadius = 5
        viewLogoutConfirm.layer.masksToBounds = true
    }
    
    
    
    
    func my_current_location(){
        
        self.mapView.clear()
        
        let source = strCurrentLat+","+strCurrentLong
        
        // let destination = strDestinationLat+","+strDestinationLong
        
        let meetinglat = (strCurrentLat as NSString).doubleValue
        let meetinglong = (strCurrentLong as NSString).doubleValue
        
        var camera = GMSCameraPosition.camera(withLatitude: meetinglat, longitude: meetinglong, zoom: self.zoomLevel)
        
        
        self.mapView.camera = camera
        let position = CLLocationCoordinate2D(latitude: meetinglat, longitude: meetinglong)
        let marker = GMSMarker(position: position)
        marker.icon = UIImage(named: "ico_map_pin_m")
        
        UserDefaults.standard.set(self.strCurrentLat, forKey: "currentLat")
        UserDefaults.standard.set(self.strCurrentLong, forKey: "currentLong")
        
        
        marker.map = self.mapView
    }
    
    
    func updateTableViewHeightConstraint(arrayCount:Int){
        
        var height = CGFloat(arrayCount ) * 60

        
        let screenHeight = UIScreen.main.bounds.height
        if height > (screenHeight - 220) {
            height = screenHeight - 220
        }
        
        heightTableView.constant = height
    }
    
    func swipeUptoEmergencyCall(){
        swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeToEmergencyCall))
        swipeGesture.direction = UISwipeGestureRecognizer.Direction.up
        self.viewEmergencyCall.addGestureRecognizer(swipeGesture)
        
        swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeToEmergencyCall))
        swipeGesture.direction = UISwipeGestureRecognizer.Direction.down
        self.viewEmergencyCall.addGestureRecognizer(swipeGesture)
    }
    
    func swipeUptoEmergencyContactList(){
        swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeToEmergencyContactList))
        swipeGesture.direction = UISwipeGestureRecognizer.Direction.up
        self.viewEmergencyContactList.addGestureRecognizer(swipeGesture)
        
        swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeToEmergencyContactList))
        swipeGesture.direction = UISwipeGestureRecognizer.Direction.down
        self.viewEmergencyContactList.addGestureRecognizer(swipeGesture)
    }
}


// MARK: - Webservice calling

extension HomePageVC{
    
    func post_selectVehicle(){

        ValidationManager.sharedManager.showLoader()
        
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?vUserDeviceCountry=&MemberType=Driver&iMemberId=\(userId)&vTimeZone=Asia/Kolkata&type=displaydrivervehicles&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "MemberType", value: kUserType),
                          URLQueryItem(name: "type", value: kDisplaydrivervehicles),
                          URLQueryItem(name: "iMemberId", value: userId),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "tSessionId", value: sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType)]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                }
                
                if let message = json["message"].arrayObject{
                    print(message)
                    for index in 0..<message.count{
                        let obj = message[index]
                        let modalObj = ModalHomeSelectVehicle.init(dict: obj as? [String : Any] ?? [:])
                        self.arrSelectVehicle.append(modalObj)
                    }
                    
                    self.arrFilteredSelectVehicle = self.arrSelectVehicle.filter({$0.eStatus == "Active"})
                    
                    self.profile_details()
                    
                }
                
                self.tableViewSelectVehicle.reloadData()
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    func post_showAvailable(){
        
        
//        let urlString = "http://qbeecabs.com/webservice.php?GeneralAppVersion=1.0.4&vLongitude=\(self.strCurrentLong)&iMemberId=\(userId)&type=configDriverTripStatus&GeneralUserType=Driver&isSubsToCabReq=false&tSessionId=\(sessionId)&GeneralDeviceType=Android&GeneralMemberId=\(userId)&vLatitude=\(self.strCurrentLat)&iTripId=&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&UserType=Driver"
        
        
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?Status=Available&vFirebaseDeviceToken=\(kDeviceToken)&isUpdateOnlineDate=true&GeneralAppVersion=1.0.4&type=updateDriverStatus&GeneralUserType=Driver&tSessionId=\(sessionId)&iDriverId=\(userId)&GeneralDeviceType=Android&GeneralMemberId=\(userId)&vUserDeviceCountry=\(country)&vDeviceToken=\(kDeviceToken)&vTimeZone=Asia/Kolkata"
        
        
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "type", value: kUpdateDriverStatus),
                          URLQueryItem(name: "GeneralMemberId", value: self.userId),
                          URLQueryItem(name: "iDriverId", value: self.userId),
                          URLQueryItem(name: "vFirebaseDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "vDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone),
                          URLQueryItem(name: "Status", value: "Available"),
                          URLQueryItem(name: "isUpdateOnlineDate", value: "true")]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                print(action)
                if action == "1"{
                    
                }
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    func post_getRequestFromDispatcher(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&vLang=EN&vDeviceType=Android&type=getDetail&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android&AppVersion=1.0.4&GeneralMemberId=\(userId)&iUserId=\(userId)&vUserDeviceCountry=\(country)&vDeviceToken=\(kDeviceToken)&vTimeZone=Asia/Kolkata&UserType=Driver"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    
                    if let message = json["message"].dictionaryObject{
                        let tripStatus = message["vTripStatus"] as? String ?? ""
                        
                        if tripStatus == "Active"{
                            self.navigate_enRoute()
                        }
                        

                        
                    }
                    
                }else{
                }
                
                
                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
    @objc func post_getRequest(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralAppVersion=1.0.4&vLongitude=\(self.strCurrentLong)&iMemberId=\(userId)&type=configDriverTripStatus&GeneralUserType=Driver&isSubsToCabReq=false&tSessionId=\(sessionId)&GeneralDeviceType=Android&GeneralMemberId=\(userId)&vLatitude=\(self.strCurrentLat)&iTripId=&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&UserType=Driver"
        
        //        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&iDriverId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&bookingType=checkBookings&vTimeZone=Asia/Kolkata&type=checkBookings&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                
                let action = json["Action"].stringValue
                
                if action == "1"{
                    
                    let message = json["message"].arrayValue
                    
                    for index in 0..<message.count{
                        let obj = message[index]
                        
                        self.iCabBookingId = obj["iCabBookingId"].stringValue
                        self.passengerId = obj["iUserId"].stringValue
                        self.pSourceLatitude = obj["vSourceLatitude"].stringValue
                        self.pSourceLongitude = obj["vSourceLongitude"].stringValue
                        self.pDestLatitude = obj["vDestLatitude"].stringValue
                        self.pDestLongitude = obj["vDestLongitude"].stringValue
       
                        let vc:AcceptRequestVC = self.storyboard?.instantiateViewController(withIdentifier: "AcceptRequestVC") as! AcceptRequestVC
                        vc.iCabBookingId = self.iCabBookingId
                        vc.passengerId = self.passengerId
                        vc.pName = self.pName
                        vc.pRating = self.pRating
                        vc.pSourceLatitude = self.pSourceLatitude
                        vc.pSourceLongitude = self.pSourceLongitude
                        vc.pDestLatitude = self.pDestLatitude
                        vc.pDestLongitude = self.pDestLongitude
                        //                    vc.pICabRequestId = self.pICabRequestId
                        //                    vc.pMsgCode = self.pMsgCode
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    
                    
                    
                    
                    
                    
                }
                
                
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    func post_emergencyContact(){

        ValidationManager.sharedManager.showLoader()
//        let urlString = "http://app.wagonstaxi.com/webservice.php?iUserId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=loadEmergencyContacts&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
        
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?iUserId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=loadEmergencyContacts&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios"
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kLoadEmergencyContacts),
                          URLQueryItem(name: "iUserId", value: self.userId),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone)]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let actionContactList = json["Action"].stringValue
                print("action is \(actionContactList)")
                
                if let message = json["message"].arrayObject{
                    print(message)
                    
                    for index in 0..<message.count{
                        let obj = message[index]
                        let modalObj = ModalEmergencyContactList.init(dict: obj as? [String : Any] ?? [:])
                        self.arrEmergencyContactList.append(modalObj)
                    }
                    self.tableViewContactList.reloadData()
                    ValidationManager.sharedManager.hideLoader()

                }
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
}
