//
//  EmergencyContactCell.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 22/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class EmergencyContactCell: UITableViewCell {

    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
