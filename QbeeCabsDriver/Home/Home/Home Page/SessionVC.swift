//
//  SessionVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 27/05/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class SessionVC: UIViewController {

    var window: UIWindow?
    var navigationcontroller: UINavigationController?
    
    
    // MARK: - Web service variable

    let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    //MARK: - Outlets

    
    @IBOutlet weak var viewConfirm: UIView!
    @IBOutlet weak var viewConfirmIn: UIView!
    @IBOutlet weak var viewCant: UIView!
    @IBOutlet weak var lblConfirm: UILabel!
    @IBOutlet weak var lblSessionExpired: UILabel!
    @IBOutlet weak var lblCantCommunicate: UILabel!
    @IBOutlet weak var btnRetry: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOk: UIButton!

    @IBOutlet var viewCantButtonL: UIView!
    @IBOutlet var viewConfirmInButtonL: UIView!
    
    
    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
    
    
// MARK: - View Did Load

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.corner_radius()
//        NVActivityIndicatorView(frame: activityIndicatorView.frame, type: .ballPulse, color: .blue, padding: 100)
        activityIndicatorView.type = .ballPulse
        self.activityIndicatorView.startAnimating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        
        if CheckInternet.connection(){
            if userId.isEmpty{
                self.navigate_logIn()
            }else{
                self.post_deviceSession()
            }
        }else{
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            let textAlert = objLang?["LBL_RIDER_NO_INTERNET_MSG_GENERAL"] as? String ?? ""
            let textOk = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
            ValidationManager.sharedManager.showAlert(message: textAlert, title: "", btnTitle: textOk, controller: self)
            
        }
        
        
        
        
    }
    
    @IBAction func btnOk(_ sender: Any) {
        self.navigate_logIn()
    }
    
    @IBAction func btnInternetRetry(_ sender: Any) {
    }
    @IBAction func btnInternetCancel(_ sender: Any) {
//        if CheckInternet.connection()
    }
    
    @IBAction func btnRetry(_ sender: Any) {
        self.post_deviceSession()
        self.viewConfirm.isHidden = true
    }
    @IBAction func btnCancel(_ sender: Any) {
        self.viewConfirm.isHidden = true
    }
    
    func navigate_home(){
        self.activityIndicatorView.stopAnimating()
        let sb:UIStoryboard = UIStoryboard(name: "HomeSB", bundle: Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navHome") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationcontroller
        window?.makeKeyAndVisible()
    }
    
    func navigate_logIn(){
        let sb:UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navMain") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationcontroller
        window?.makeKeyAndVisible()
    }
    
    
    // MARK: - set Language Labels
    
    func setLanguageLabel(){
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        
        lblCantCommunicate.text! = objLang?["LBL_TRY_AGAIN_TXT"] as? String ?? ""
        btnCancel.setTitle(objLang?["LBL_CANCEL_TXT"] as? String ?? "", for: .normal)
        btnRetry.setTitle(objLang?["LBL_RETRY_BTN_TXT_NO_INTERNET"] as? String ?? "", for: .normal)
        lblConfirm.text! = objLang?["LBL_BTN_CONFIRM_TXT"] as? String ?? ""
        lblSessionExpired.text! = objLang?["LBL_SESSION_TIME_OUT"] as? String ?? ""
        btnOk.setTitle(objLang?["LBL_BTN_OK_TXT"] as? String ?? "", for: .normal)
        
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewCantButtonL.semanticContentAttribute = .forceRightToLeft
            self.viewConfirmInButtonL.semanticContentAttribute = .forceRightToLeft
            lblConfirm.textAlignment = .right
            lblCantCommunicate.textAlignment = .right
            lblSessionExpired.textAlignment = .right
            
            
        }
        

    }
    
    func corner_radius(){
        self.viewConfirm.isHidden = true
        self.viewConfirmIn.layer.cornerRadius = 5   
        self.viewConfirmIn.layer.masksToBounds = true
        
        self.viewCant.isHidden = true
        self.viewCant.layer.cornerRadius = 5
        self.viewCant.layer.masksToBounds = true
        
    }

    
}


// MARK: - Webservice calling

extension SessionVC{
    
    func post_deviceSession(){

        self.activityIndicatorView.startAnimating()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&vLang=EN&vDeviceType=ios&type=getDetail&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios&AppVersion=1.0.4&GeneralMemberId=\(userId)&iUserId=\(userId)&vUserDeviceCountry=\(country)&vDeviceToken=\(kDeviceToken)&vTimeZone=Asia/Kolkata&UserType=Driver"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kGetDetail),
                          URLQueryItem(name: "GeneralMemberId", value: self.userId),
                          URLQueryItem(name: "iUserId", value: self.userId),
                          URLQueryItem(name: "vFirebaseDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "vDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "vDeviceType", value: kDeviceType),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone),
                          URLQueryItem(name: "vLang", value: "EN")]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    
                    if let message = json["message"].dictionaryObject{
                        let vTripStatus = message["vTripStatus"] as? String ?? ""
                        UserDefaults.standard.setValue(vTripStatus, forKey: "TripStatus")
                        
                        let ratingFromDriver = message["Ratings_From_Driver"] as? String ?? ""
                        UserDefaults.standard.setValue(ratingFromDriver, forKey: "Ratings_From_Driver")
                        
                        let obj = message["TripDetails"] as? [String:Any] ?? [:]
                        
                        let iActive = obj["iActive"] as? String ?? ""
                        UserDefaults.standard.set(iActive, forKey: "iActive")
                        
                        let ePaymentCollect = obj["ePaymentCollect"] as? String ?? ""
                        UserDefaults.standard.setValue(ePaymentCollect, forKey: "ePaymentCollect")

                        self.navigate_home()

                    }
                    
                }else{
                    self.activityIndicatorView.stopAnimating()
                    self.viewConfirm.isHidden = false
                }
                
                self.activityIndicatorView.stopAnimating()
                
                
            case .failure(let error):
                print(error)
                self.activityIndicatorView.stopAnimating()
                self.viewCant.isHidden = false

            }
        }
        
    }
    
}
