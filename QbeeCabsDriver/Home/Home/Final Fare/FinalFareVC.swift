//
//  FinalFareVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 15/05/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class FinalFareVC: UIViewController {
    
    var window: UIWindow?
    var navigationcontroller:UINavigationController?
    
    var objFinalFare:ModalFinalFare?
    
    var dictFareDetails:[String:Any] = [:]
    var pName = ""
    var strDistance:String = ""
    var strTime:String = ""
    var SubTotal:Double = 0
    var normalTotal:Double = 0
    
    var totalDistance:Double = 0
    var totalTime:Double = 0
    var strGst:Double = 0
    
    var strTotalDistance = ""
    var strTotalTime = ""
    
    var strRating:String = ""
    var strTripId:String = ""
    
    var endTotalFare = ""
    var tStartDate = ""
    var vTripPaymentMode = ""
    var fDiscount = ""
    var currencySymbol = ""
    
    var strCurrentLat:String = ""
    var strCurrentLong:String = ""
    var strDestinationLat:String = ""
    var strDestinationLong:String = ""
    
    // MARK: - Outlets

    
    @IBOutlet weak var lblTotalFare: UILabel!
    
    
    @IBOutlet weak var lblTripDate: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    
    @IBOutlet weak var lblPaymentType: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
 
    
    
    @IBOutlet weak var lblBaseFare: UILabel!
    @IBOutlet weak var lblMinimum: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblPromocode: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet var lblWallet: UILabel!
    
    @IBOutlet weak var lblVehicleName: UILabel!
    
    @IBOutlet weak var lblAmountBaseFare: UILabel!
    @IBOutlet weak var lblAmountDistance: UILabel!
    @IBOutlet weak var lblAmountTime: UILabel!
    @IBOutlet weak var lblAmountPromocode: UILabel!
    @IBOutlet weak var lblAmountMinimum: UILabel!
    @IBOutlet var lblAmountWallet: UILabel!
    
    @IBOutlet weak var viewFare: UIView!
    @IBOutlet weak var viewFareIn: UIView!
    
    @IBOutlet weak var viewPaymentType: UIView!
    @IBOutlet weak var viewUpar: UIView!
    

    @IBOutlet var viewBaseFareL: UIView!
    @IBOutlet var viewDistanceL: UIView!
    @IBOutlet var viewTimeL: UIView!
    @IBOutlet var viewPromocodeL: UIView!
    @IBOutlet var viewMinimumL: UIView!
    @IBOutlet var viewWalletL: UIView!
    
    
    

    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
    

    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.setValue("", forKey: "TripStatus")
        self.corner_radius()

        self.post_displayFare()

    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }
    
// MARK: - Button Actions

    @IBAction func btnColletPayment(_ sender: Any) {
        self.post_collectPayment()

        
        
    }

    func get_fareDetails(){

        
        var indexOfDict:Int = -1
        
        for (key, value) in dictFareDetails {
            indexOfDict = indexOfDict + 1
            switch indexOfDict {
            case 0:
                lblBaseFare.text = key
                lblAmountBaseFare.text = value as? String ?? ""
            case 1:
                lblDistance.text = key
                lblAmountDistance.text = value as? String ?? ""
            case 2:
                lblTime.text = key
                lblAmountTime.text! = value as? String ?? ""
            case 3:
                lblMinimum.text = key
                lblAmountMinimum.text = value as? String ?? ""
            case 4:
                lblPromocode.text = key
                lblAmountPromocode.text = value as? String ?? ""
                
            default:
                break
            }
            
            //            if let index = Int(key) {
            //            let index = Index()
            print(key)
            print(value)
            print(index)
            
            
            
            
            
            //            }
        }
    }
    
    
    func get_fareNew(){
        
        for (key, value) in self.dictFareDetails {
            
            let obj = self.dictFareDetails
            print(obj)

            if key.contains("Base"){
                viewBaseFareL.isHidden = false
                lblBaseFare.text = key
                lblAmountBaseFare.text = obj[key] as? String ?? ""
            }
            if key.contains("Distance"){
                viewDistanceL.isHidden = false
                lblDistance.text = key
                lblAmountDistance.text = obj[key] as? String ?? ""

            }
            if key.contains("Time"){
                viewTimeL.isHidden = false
                lblTime.text = key
                lblAmountTime.text = obj[key] as? String ?? ""

            }
            if key.contains("Promo"){
                viewPromocodeL.isHidden = false
                lblPromocode.text = key
                lblAmountPromocode.text = obj[key] as? String ?? ""
            }
            
            if key.contains("Minimum"){
                viewMinimumL.isHidden = false
                lblMinimum.text = key
                lblAmountMinimum.text = obj[key] as? String ?? ""
            }
            if key.contains("Wallet"){
                viewWalletL.isHidden = false
                lblWallet.text = key
                lblAmountWallet.text = obj[key] as? String ?? ""
            }
            
            print(key)
            print(value)
            print(index)
            
            
        }
        
        
    }
    
    
    
    func total_fare(){
        

//        let obj = objFinalFare
//        self.strTripId = obj?.strTripId ?? ""
        
//        self.strDistance = "Distance (\(obj?.finalDistance ?? ""))"
//        self.lblDistance.text! = strDistance
//        self.strTime = "Distance (\(obj?.finalTime ?? ""))"
//        self.lblTime.text! = strTime
//        self.lblAmountBaseFare.text! = String(format: "%.2f", obj?.BaseFare ?? 0) // "3.14"
//        self.lblAmountBaseFare.text! = String(obj?.BaseFare ?? 0)
//        self.lblAmountDistance.text! = String(format: "%.2f", obj?.PricePerKM ?? 0) // "3.14"
//        self.lblAmountTime.text! = String(format: "%.2f", obj?.PricePerMin ?? 0) // "3.14"

//        self.lblTripDate.text! = obj?.tripDate ?? ""
//        self.lblCarName.text! = obj?.vehicleType ?? ""
//
//        let distance = self.strDistance.replacingOccurrences(of: " km", with: "")
//        if let pricePerKm = obj?.PricePerKM, let distance = Double(distance){
//            self.totalDistance = pricePerKm*distance
//            print(totalDistance)
//            self.strTotalDistance = String(totalDistance)
//        }
//        self.lblAmountDistance.text = strTotalDistance
//
//        let time = self.strTime.replacingOccurrences(of: " mins", with: "")
//        if let pricePerMin = obj?.PricePerMin, let time = Double(time){
//            self.totalTime = pricePerMin*time
//            print(totalTime)
//        }
//        let doubleStr = String(format: "%.2f", totalTime) // "3.14"
//        self.lblAmountTime.text = doubleStr
//
//        if let baseFare = obj?.BaseFare{
//            self.SubTotal = self.SubTotal + baseFare
//        }
//        //        if let pricePerKM = obj?.PricePerKM{
//        self.SubTotal = self.SubTotal + totalDistance
//        //        }
//        //        if let pricePerMin = obj?.PricePerMin{
//        self.SubTotal = self.SubTotal + totalTime
//        //        }
//        self.strGst = self.SubTotal*18/100
//        let doubleStrGst = String(format: "%.2f", strGst) // "3.14"
//
//        self.lblAmountMinimum.text! = String(doubleStrGst)
//
//        self.SubTotal = self.SubTotal + strGst
//        let doubleStrTotal = String(format: "%.2f", SubTotal) // "3.14"
//        lblAmountSubtotal.text! = String(doubleStrTotal)
//        lblTotalFare.text! = String(doubleStrTotal)

        
        
        
        
        
    }
    
    
    func corner_radius(){
        self.viewPaymentType.layer.cornerRadius = 8
        self.viewPaymentType.layer.masksToBounds = true
        
        self.viewFare.layer.cornerRadius = 8
        self.viewFare.layer.masksToBounds = true
        
        
        viewBaseFareL.isHidden = true
        viewDistanceL.isHidden = true
        viewTimeL.isHidden = true
        viewPromocodeL.isHidden = true
        viewMinimumL.isHidden = true
        viewWalletL.isHidden = true

    }
    
    
    
    
}

// MARK: - Webservice calling

extension FinalFareVC{
    
    func post_displayFare(){
        self.activityIndicatorView.startAnimating()
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        //        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&iMemberId=\(userId)&vTimeZone=Asia %2FCalcutta&type=displayFare&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios"
        
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&iMemberId=\(userId)&vTimeZone=Asia%2FKolkata&type=displayFare&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                
//                self.dictFareDetails = json["FareDetailsArr"].dictionaryObject ?? [:]
                self.dictFareDetails = json["FareDetailsArr"].dictionaryObject ?? [:]

                self.strTripId = json["iTripId"].stringValue
                self.lblVehicleName.text! = json["vVehicleType"].stringValue
                self.endTotalFare = json["FareSubTotal"].stringValue
                self.tStartDate = json["tStartDate"].stringValue
                self.vTripPaymentMode = json["vTripPaymentMode"].stringValue
                self.tStartDate = json["tStartDate"].stringValue
                self.fDiscount = json["fDiscount"].stringValue
                self.currencySymbol = json["CurrencySymbol"].stringValue
                
                let objPassengerDetails = json["PassengerDetails"].dictionaryObject
                let firstName = objPassengerDetails?["vName"] as? String ?? ""
                let lastName = objPassengerDetails?["vLastName"] as? String ?? ""
                self.pName = "\(firstName) \(lastName)"
//                self.get_fareDetails()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2){
                    self.get_fareNew()
                }
                
//                self.lblAmountSubtotal.text! = self.endTotalFare
                self.lblTotalFare.text! = "\(self.endTotalFare)"
                self.lblPaymentType.text! = "Payment type \(self.vTripPaymentMode)"
                self.lblTripDate.text! = self.tStartDate
                self.lblDiscount.text! = "\(self.currencySymbol)\(self.fDiscount)"
                
                self.activityIndicatorView.stopAnimating()
                
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    func post_collectPayment(){
        self.activityIndicatorView.startAnimating()
        

        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&iTripId=\(strTripId)&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=CollectPayment&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    UserDefaults.standard.setValue("Yes", forKey: "ePaymentCollect")
                    let vc:RatingVC = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
                self.activityIndicatorView.stopAnimating()
                
                
            case .failure(let error):
                print(error)
                self.activityIndicatorView.stopAnimating()
                
            }
        }
        
    }
    

    
    
    
}
