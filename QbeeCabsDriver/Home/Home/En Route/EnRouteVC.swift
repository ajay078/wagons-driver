//
//  EnRouteVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 18/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleMaps
import GooglePlaces


class EnRouteVC: UIViewController, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    var window: UIWindow?
    var navigationcontroller: UINavigationController?
    var objTripDetail = ModalCurrentTrip(dict: [:])

    var pName = ""
    var pRating = ""
    var pPassengerPickUp = ""
    var pPicName = ""
    var pPassengerId = ""
    var pDestLocAddress = ""
    var pTripId = ""
    var strTripRating:String = ""

    var pSourceLatitude = ""
    var pSourceLongitude = ""
    var destLatitude = ""
    var destLongitude = ""
    var destAddress = ""
    
    var endTotalFare = ""
    var currencySymbol = ""
    var isTripBegin:Bool = false
    
    var tripStatusIntial = ""
    var driverInitialLat = ""
    var driverInitialLong = ""
    
    var source = ""
    var destination = ""
    
    var intialLat:Double = 0
    var intialLong:Double = 0
    
    var passengerInitialLat = ""
    var passengerInitailLong = ""
    
    var strCurrentLat:String = ""
    var strCurrentLong:String = ""
    var strDestinationLat:String = ""
    var strDestinationLong:String = ""
    let locManager = CLLocationManager()
    var zoomLevel:Float = 15
    var cameraPosition = GMSCameraPosition()

    
    var simulateCurrentLat = ""
    var simulateCurrentLong = ""
    
    
    var arrEmergencyContactList = [ModalEmergencyContactList]()

    var swipeGesture = UISwipeGestureRecognizer()
    var swipeGestureBegin = UISwipeGestureRecognizer()
    var swipeGestureEnd = UISwipeGestureRecognizer()

    var bottomConstantEmergencyCall : CGFloat = 0
    var bottomConstantContactList : CGFloat = 0
    
    
    var timerBeforeArrived = Timer()
    var timerCurrentStatus = Timer()

// MARK: - Outlets

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtEnterOtp: UITextField!
    
    @IBOutlet weak var viewEnterOtp: UIView!
    @IBOutlet weak var viewEnterOtpIn: UIView!
    
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewMessageIn: UIView!
    
    
    @IBOutlet weak var viewOption: UIView!
    @IBOutlet weak var viewBtnPassengerDetail: UIView!
    @IBOutlet weak var viewBtnWayBill: UIView!
    @IBOutlet weak var viewBtnCancelTrip: UIView!
    
    @IBOutlet weak var viewBtnEnterOtp: UIView!
    
    
    @IBOutlet weak var viewBtnArrived: UIView!
    
    @IBOutlet weak var lblDistanceTime: UILabel!
    
    @IBOutlet weak var viewPassengerDetail: UIView!
    @IBOutlet weak var viewPassengerDetailIn: UIView!
    @IBOutlet weak var lblPassengerName: UILabel!
    @IBOutlet weak var imgPassengerImage: UIImageView!
    @IBOutlet weak var lblRating: UILabel!
    
    
    @IBOutlet weak var viewSlideBeginTrip: UIView!
    @IBOutlet weak var viewSlideBeginTripCircle: UIView!
    @IBOutlet weak var lblBeginEndTrip: UILabel!
    
    @IBOutlet weak var imgSlideBeginEnd: UIImageView!
    
    
    
    
    @IBOutlet weak var lblPassengerPickUp: UILabel!
    
    
    @IBOutlet weak var viewCancelTrip: UIView!
    @IBOutlet weak var viewCancelTripIn: UIView!
    @IBOutlet weak var txtCancelTripReason: UITextField!
    @IBOutlet weak var txtCancelTripComment: UITextField!
    
    
    
    
    @IBOutlet weak var imgRating1: UIImageView!
    @IBOutlet weak var imgRating2: UIImageView!
    @IBOutlet weak var imgRating3: UIImageView!
    @IBOutlet weak var imgRating4: UIImageView!
    @IBOutlet weak var imgRating5: UIImageView!
    
    
    
    @IBOutlet weak var viewEmergencyCall: UIView!
    @IBOutlet weak var bottomViewEmergencyCall: NSLayoutConstraint!

    @IBOutlet weak var viewEmergencyContactList: UIView!
    @IBOutlet weak var bottomViewEmergencyContactList: NSLayoutConstraint!
    
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet var viewPassengerPickL: UIView!
    @IBOutlet var btnShowPassengerDetail: UIButton!
    @IBOutlet var btnShowWayBill: UIButton!
    @IBOutlet var btnShowCancelTrip: UIButton!
    @IBOutlet var lblNavigateL: UILabel!
    @IBOutlet var btnMessageOk: UIButton!
    @IBOutlet var viewBtnMessageOk: UIView!
    @IBOutlet var lblCancelTripL: UILabel!
    @IBOutlet var viewBtnContinueCancel: UIStackView!
    @IBOutlet var btnContinueTrip: UIButton!
    @IBOutlet var btnCancelTripNow: UIButton!
    @IBOutlet var lblPassengerDetailL: UILabel!
    @IBOutlet var lblCallL: UILabel!
    @IBOutlet var lblMessageL: UILabel!
    
    @IBOutlet var leadingViewOption: NSLayoutConstraint!
    @IBOutlet var trailingViewOption: NSLayoutConstraint!
    
    @IBOutlet var btnArrived: UIButton!
    
    
    @IBOutlet var viewInReason: UIView!
    @IBOutlet var viewInComment: UIView!
    @IBOutlet weak var heightVRReason: NSLayoutConstraint!
    @IBOutlet weak var heightVRComment: NSLayoutConstraint!
    @IBOutlet weak var lblRequiredReason: UILabel!
    @IBOutlet weak var lblRequiredComment: UILabel!

    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var tableViewContactList: UITableView!
    
    
// MARK: - View Did Load

    
    override func viewDidLoad() {
        super.viewDidLoad()
        swipeGesture.delegate = self
        swipeGestureBegin.delegate = self
        swipeGestureEnd.delegate = self
        self.setLanguageLabel()
        self.post_getCurrentTripInfo()

        self.corner_radius()
        self.swipeToBeginEndTrip()
        
//        self.post_emergencyContact()

        self.swipeUptoEmergencyCall()
        self.swipeUptoEmergencyContactList()
        
        bottomViewEmergencyCall.constant = -500
        bottomViewEmergencyContactList.constant = -500
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 5)

        
        self.timerBeforeArrived = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.updateCurrentStatusBeforeArrived), userInfo: nil, repeats: true)

        
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timerCurrentStatus.invalidate()
        ValidationManager.sharedManager.hideLoader()
    }
    
    @objc func updateCurrentStatus() {
//        PlacePicker.shared.locationAuthorization(controller: self, success: { (placeDict) in
//            print("place info = \(placeDict)")
            
//            self.strCurrentLat = placeDict["lat"] as? String ?? "0"
//            self.strCurrentLong = placeDict["long"] as? String ?? "0"
            
//            self.simulateCurrentLat = placeDict["lat"] as? String ?? "0"
//            self.simulateCurrentLong = placeDict["long"] as? String ?? "0"
            
//            let StrAdd = placeDict["add"] as? String ?? ""
//            print(StrAdd)
//
            self.my_current_location()
//            self.post_currentStatus()

//        }) { (error) in
//            print("error = \(error.localizedDescription)")
//        }
    }

        @objc func updateCurrentStatusBeforeArrived() {
            self.post_getCurrentTripInfo()
        }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }
    
//MARK:- Button Actions


    @IBAction func btnSubmitOtp(_ sender: Any) {
        self.viewEnterOtp.isHidden = true
    }
    @IBAction func btnMessageOk(_ sender: Any) {
        
        let tripStatus = UserDefaults.standard.string(forKey: "TripStatus")
        
        if tripStatus == "Cancelled"{
            self.navigate_session()
        }else{
            self.post_driverArrived()
            self.viewMessage.isHidden = true
            self.viewEnterOtp.isHidden = true
            self.viewBtnEnterOtp.isHidden = true
            self.viewBtnArrived.isHidden = true
            self.viewSlideBeginTrip.isHidden = false
            // self.lblTitle.text! = "EN ROUTE"
            
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblTitle.text! = objLang?["LBL_RIDER_EN_ROUTE_MAIN_SCREEN"] as? String ?? ""
            
            self.lblPassengerPickUp.text! = self.pDestLocAddress
        }
        
        
    }
    @IBAction func btnOption(_ sender: Any) {
        
        if viewOption.isHidden == false{
            self.viewOption.isHidden = true
        }else{
            self.viewOption.isHidden = false
        }
    }
    
    @IBAction func btnShowPassenegerDetail(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnPassengerDetail.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        self.viewPassengerDetail.isHidden = false
    }
    
    @IBAction func btnHidePassengerDetail(_ sender: Any) {
        self.viewOption.isHidden = true
        self.viewPassengerDetail.isHidden = true
    }
    
    
    
    @IBAction func btnShowWayBill(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnWayBill.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:WayBillVC = self.storyboard?.instantiateViewController(withIdentifier: "WayBillVC") as! WayBillVC
        self.navigationController?.pushViewController(vc, animated: true)
        self.viewOption.isHidden = true
    }
    @IBAction func btnShowCancelTrip(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnCancelTrip.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        self.viewCancelTrip.isHidden = false
    }
    
    
    @IBAction func btnViewEnterOtp(_ sender: Any) {
        self.viewEnterOtp.isHidden = false
    }
    
    
    @IBAction func btnArrived(_ sender: Any) {
        self.viewMessage.isHidden = false
    }
    
    
    @IBAction func btnPassengerCall(_ sender: Any) {
    }
    @IBAction func btnPassengerMessage(_ sender: Any) {
    }
    
    @IBAction func btnEmergencyCall(_ sender: Any) {
        bottomViewEmergencyCall.constant = 0
        UIView.animate(withDuration: 0.5){
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func btnEmergencyContact(_ sender: Any) {
        bottomViewEmergencyContactList.constant = 0
        UIView.animate(withDuration: 0.5){
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnContinueTrip(_ sender: Any) {
        self.view.endEditing(true)
        self.viewOption.isHidden = true
        self.viewCancelTrip.isHidden = true
    }
    @IBAction func btnCancelTripNow(_ sender: Any) {
        self.view.endEditing(true)

        if txtCancelTripReason.text!.isEmpty{

            self.viewInReason.backgroundColor = UIColor.red
            self.heightVRReason.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblRequiredReason.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
            
            
            
        }else if txtCancelTripComment.text!.isEmpty{
            
            self.viewInComment.backgroundColor = UIColor.red
            self.heightVRComment.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblRequiredComment.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
            
        }else{
            self.viewCancelTrip.isHidden = true
            self.post_cancelTrip()
        }

        
    }
    
    //MARK:- Text Field Methods
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCancelTripReason{
            
            self.viewInReason.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            self.viewInComment.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            
            self.viewInReason.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            self.heightVRReason.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtCancelTripComment{
            
            self.viewInReason.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.viewInComment.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            
            self.heightVRComment.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
        return true
        
    }
    
    func height_required(){
        heightVRReason.constant = 0
        heightVRComment.constant = 0
        


    }
    
//MARK:- Table View Methods

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrEmergencyContactList.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:EmergencyContactCell = tableView.dequeueReusableCell(withIdentifier: "EmergencyContactCell", for: indexPath) as! EmergencyContactCell
        
        let obj = arrEmergencyContactList[indexPath.row]
        
        cell.lblName.text! = obj.strName
        cell.lblNumber.text! = obj.strPhone
        
        return cell
    }
    


    //MARK: - General Functions
    
    func corner_radius(){
        
        self.height_required()
        txtCancelTripReason.delegate = self
        txtCancelTripComment.delegate = self
        
        self.viewMessage.isHidden = true
        self.viewEnterOtp.isHidden = true
        self.viewOption.isHidden = true
        self.viewPassengerDetail.isHidden = true
        self.viewBtnEnterOtp.isHidden = true
        self.viewSlideBeginTrip.isHidden = true
        self.viewCancelTrip.isHidden = true
        
        self.viewMessageIn.layer.cornerRadius = 5
        self.viewMessageIn.layer.masksToBounds = true
        
        self.viewEnterOtpIn.layer.cornerRadius = 5
        self.viewEnterOtpIn.layer.masksToBounds = true
        
        self.viewPassengerDetailIn.layer.cornerRadius = 5
        self.viewPassengerDetailIn.layer.masksToBounds = true
        
        self.viewBtnEnterOtp.layer.cornerRadius = 20
        self.viewBtnEnterOtp.layer.masksToBounds = true
        
        self.viewCancelTripIn.layer.cornerRadius = 20
        self.viewCancelTripIn.layer.masksToBounds = true
        
        self.viewSlideBeginTripCircle.layer.cornerRadius = viewSlideBeginTripCircle.frame.size.height/2
        self.viewSlideBeginTripCircle.layer.masksToBounds = true
        
        self.imgPassengerImage.layer.cornerRadius = imgPassengerImage.frame.size.height/2

        
    }
    
        func location_details(){
            
            let obj = objTripDetail.tripDetails
            
            
            self.lblPassengerPickUp.text! = obj.tSaddress
            
            strCurrentLat = obj.tStartLat
            strCurrentLong = obj.tStartLong
            strDestinationLat = obj.tEndLat
            strDestinationLong = obj.tEndLong
            pDestLocAddress = obj.tDaddress
            self.draw_googleMapDirection()
            
        }
        
        
        func passenger_details(){
            
            let obj = objTripDetail.tripDetails.passengerDetails
            self.pName = "\(obj.firstName) \(obj.lastName)"
            self.lblPassengerName.text! = self.pName
            self.lblRating.text! = obj.rating
            self.passengerInitialLat = obj.passengerIntialLat
            self.passengerInitailLong = obj.passengerIntailLong
            
            
            
            let imageUrlId = "https://apps.wagonstaxi.com/webimages/upload/Passenger/"
            self.pPassengerId = obj.passengerId
            let imgUserId:String = obj.imgIdPassenger
            
            let imageUrl = imageUrlId+"\(pPassengerId)/"+imgUserId
            print(imageUrl)
            DispatchQueue.global(qos: .background).async {
                let url = URL(string: imageUrl)
                if let data = try? Data(contentsOf: url!){
                    DispatchQueue.main.async {
                        self.imgPassengerImage.image = UIImage(data: data)
                        
                    }
                }
                
                
            }
            
    //        self.strTripRating = self.pRating
            self.strTripRating = obj.rating
            let doubleRating = Double(strTripRating) ?? 0.0
            let IntRating:Int = Int(round(doubleRating))
            self.get_rating(rating: IntRating)
            
        }
    

    func driver_details(){
     
        let obj = objTripDetail.tripDetails.driverDetails
        self.driverInitialLat = obj.driverLat
        self.driverInitialLong = obj.driverLong
        
        self.draw_googleMapDirection()

    }
    
    
    func colorMenuButton(){
        
        self.viewBtnPassengerDetail.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnWayBill.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnCancelTrip.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)


        
    }
    
    
    func get_rating(rating:Int){
        
        switch rating {
        case 1:
            imgRating1.image = UIImage(named: "star yellow")
        case 2:
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            
        case 3:
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            imgRating3.image = UIImage(named: "star yellow")
            
            
        case 4:
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            imgRating3.image = UIImage(named: "star yellow")
            imgRating4.image = UIImage(named: "star yellow")
            
            
        case 5:
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            imgRating3.image = UIImage(named: "star yellow")
            imgRating4.image = UIImage(named: "star yellow")
            imgRating5.image = UIImage(named: "star yellow")
            
            
        default:
            break
        }
        
    }
    
    
    //MARK: - Swipe Functions

    func swipeToBeginEndTrip(){
        swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToBeginEndTrip))
        swipeGesture.direction = UISwipeGestureRecognizer.Direction.right
        self.viewSlideBeginTrip.addGestureRecognizer(swipeGesture)
        
        
    }
    
    
    func swipeUptoEmergencyCall(){
        swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeToEmergencyCall))
        swipeGesture.direction = UISwipeGestureRecognizer.Direction.up
        self.viewEmergencyCall.addGestureRecognizer(swipeGesture)
        
        swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeToEmergencyCall))
        swipeGesture.direction = UISwipeGestureRecognizer.Direction.down
        self.viewEmergencyCall.addGestureRecognizer(swipeGesture)
    }
    
    func swipeUptoEmergencyContactList(){
        swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeToEmergencyContactList))
        swipeGesture.direction = UISwipeGestureRecognizer.Direction.up
        self.viewEmergencyContactList.addGestureRecognizer(swipeGesture)
        
        swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeToEmergencyContactList))
        swipeGesture.direction = UISwipeGestureRecognizer.Direction.down
        self.viewEmergencyContactList.addGestureRecognizer(swipeGesture)
    }
    
    
    
    func navigate_home(){
        let sb: UIStoryboard = UIStoryboard(name: "HomeSB", bundle:Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navHome") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationcontroller
        window?.makeKeyAndVisible()
    }
    func navigate_session(){
        let sb: UIStoryboard = UIStoryboard(name: "HomeSB", bundle:Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navSession") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationcontroller
        window?.makeKeyAndVisible()
    }
    
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblTitle.text! = objLang?["LBL_PICK_UP_PASSENGER"] as? String ?? ""
        lblMessage.text! = objLang?["LBL_ARRIVED_CONFIRM_DIALOG_TXT"] as? String ?? ""

        btnShowPassengerDetail.setTitle(objLang?["LBL_PASSENGER_DETAIL"] as? String ?? "", for: .normal)
        btnShowWayBill.setTitle(objLang?["LBL_MENU_WAY_BILL"] as? String ?? "", for: .normal)
        btnShowCancelTrip.setTitle(objLang?["LBL_CANCEL_TRIP"] as? String ?? "", for: .normal)
        btnCancelTripNow.setTitle(objLang?["LBL_CANCEL_TRIP_NOW"] as? String ?? "", for: .normal)
        btnMessageOk.setTitle(objLang?["LBL_BTN_OK_TXT"] as? String ?? "", for: .normal)

        btnContinueTrip.setTitle(objLang?["LBL_CONTINUE_TRIP_CANCEL_TRIP"] as? String ?? "", for: .normal)
        btnArrived.setTitle(objLang?["LBL_BTN_ARRIVED_TXT"] as? String ?? "", for: .normal)
        lblPassengerDetailL.text! = objLang?["LBL_PASSENGER_DETAIL"] as? String ?? ""

        lblCallL.text! = objLang?["LBL_CALL_ACTIVE_TRIP"] as? String ?? ""
        lblMessageL.text! = objLang?["LBL_MESSAGE_TXT"] as? String ?? ""
        lblNavigateL.text! = objLang?["LBL_NAVIGATE"] as? String ?? ""

        lblCancelTripL.text! = objLang?["LBL_CANCEL_TRIP"] as? String ?? ""
        txtCancelTripReason.placeholder = objLang?["LBL_ENTER_REASON"] as? String ?? ""
        txtCancelTripComment.placeholder = objLang?["LBL_WRITE_COMMENT_HINT_CANCEL_TRIP"] as? String ?? ""

        self.lblBeginEndTrip.text! = objLang?["LBL_BTN_SLIDE_BEGIN_TRIP_ACTIVE_TRIP"] as? String ?? ""

        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft
            self.viewBtnContinueCancel.semanticContentAttribute = .forceRightToLeft
            self.viewPassengerPickL.semanticContentAttribute = .forceRightToLeft
            self.viewBtnMessageOk.semanticContentAttribute = .forceRightToLeft

            txtCancelTripReason.textAlignment = .right
            txtCancelTripComment.textAlignment = .right
            lblMessage.textAlignment = .right

            trailingViewOption.isActive = false
            leadingViewOption.isActive = true
            leadingViewOption.constant = 0
            
        }else{
            trailingViewOption.isActive = true
            leadingViewOption.isActive = false
            leadingViewOption.constant = 0

        }

        
        
    }
    
}





//MARK:- Draw polyLine Google map
extension EnRouteVC{
    
    func PlaceAPIWork(){
        
       
        locManager.requestAlwaysAuthorization()
        
        var currentLocation: CLLocation!
        
        if
            CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways
        {
            currentLocation = locManager.location
        }

        self.draw_googleMapDirection()
    }
    
    func call_webServiceOfGoogleMapDirection(){
        
        if tripStatusIntial == "Active"{
            let source = self.driverInitialLat+","+self.driverInitialLong
            let destination = self.passengerInitialLat+","+self.passengerInitailLong
            
            self.source = source
            self.destination = destination
            
            
        }else{
            let source =  strCurrentLat+","+strCurrentLong
            let destination = strDestinationLat+","+strDestinationLong
            
            self.source = source
            self.destination = destination
        }
        
        

        
        
//        let source =  pSourceLatitude+","+pSourceLongitude
//        let destination = destLatitude+","+destLongitude
        
        autoreleasepool{
            
            var drivingMode:String = "driving"
            
            
            drivingMode = "driving"
            
            let distanceUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(self.source)&destinations=\(self.destination)&mode=\(drivingMode)&key=\(kGoogleApiKey)"
            AF.request(distanceUrl).responseJSON { responseObject in
                
                
                            switch responseObject.result {
                            case .success( _):
                                
                                do {
                                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : Any]
                                    
                                    guard let status = dictionary["status"] as? String, status == "OK" else{
                                        return
                                    }
                                    
                                    guard let element = (((dictionary["rows"] as? [[String: Any]])?[0])?["elements"] as? [[String: Any]])? [0] else {
                                        return
                                    }
                                    
                                    guard let e_status = element["status"] as? String, e_status == "OK" else{
                                        return
                                    }
                                    
                                    
                                    let distance = (element["distance"] as? [String:Any])?["text"] as? String ?? ""
                                    print(distance)
                                    
                                    let time = (element["duration"] as? [String:Any])?["text"] as? String ?? ""
                                    print(time)
                                    
//                                    self.lblDistanceTime.text! = "\(time) to reach & \(distance) away"
                                    
                                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                                    let textToReach = objLang?["LBL_TO_REACH"] as? String ?? ""
                                    let textAway = objLang?["LBL_AWAY_TXT_ACTIVE_TRIP"] as? String ?? ""

                                    self.lblDistanceTime.text! = "\(time) \(textToReach) & \(distance) \(textAway)"

                                    
                                }catch{
                                }
                                
                            case .failure(let error):
                                print(error)
                }
                
                
            }
        }
    }
    
    func draw_googleMapDirection(){
        
        self.mapView.clear()
        
        if tripStatusIntial == "Active"{
            let source = self.driverInitialLat+","+self.driverInitialLong
            let destination = self.passengerInitialLat+","+self.passengerInitailLong
            
            self.source = source
            self.destination = destination
            
            let meetinglat = (self.driverInitialLat as NSString).doubleValue
            let meetinglong = (self.driverInitialLong as NSString).doubleValue
            
            self.intialLat = meetinglat
            self.intialLong = meetinglong
            
            let position = CLLocationCoordinate2D(latitude: self.intialLat, longitude: self.intialLong)
            let marker = GMSMarker(position: position)
            //        marker.icon = UIImage(named: "ico_map_pin_m")
            marker.icon = UIImage(named: "car_driver-web")
            marker.map = self.mapView

            let UserLat:Double = (passengerInitialLat as NSString).doubleValue
            let UserLong:Double = (passengerInitailLong as NSString).doubleValue
            
            let position_1 = CLLocationCoordinate2D(latitude: UserLat, longitude: UserLong)
            let marker_1 = GMSMarker(position: position_1)
            marker_1.map = self.mapView
            
            marker_1.icon = UIImage(named: "passenger waiting")

            let camera = GMSCameraPosition.camera(withLatitude: UserLat, longitude: UserLong, zoom: self.zoomLevel)
            
            self.mapView.camera = camera
            
        }else{
            let source = strCurrentLat+","+strCurrentLong
            let destination = strDestinationLat+","+strDestinationLong
            
            self.source = source
            self.destination = destination
            
            let meetinglat = (passengerInitialLat as NSString).doubleValue
            let meetinglong = (passengerInitailLong as NSString).doubleValue
            
            self.intialLat = meetinglat
            self.intialLong = meetinglong
            
            let position = CLLocationCoordinate2D(latitude: self.intialLat, longitude: self.intialLong)
            let marker = GMSMarker(position: position)
            marker.icon = UIImage(named: "car_driver-web")
            marker.map = self.mapView
            
            let UserLat:Double = (strDestinationLat as NSString).doubleValue
            let UserLong:Double = (strDestinationLong as NSString).doubleValue
            
            let position_1 = CLLocationCoordinate2D(latitude: UserLat, longitude: UserLong)
            let marker_1 = GMSMarker(position: position_1)
            marker_1.map = self.mapView
            
            marker_1.icon = UIImage(named: "ico_map_pin_m")

            let camera = GMSCameraPosition.camera(withLatitude: self.intialLat, longitude: self.intialLong, zoom: self.zoomLevel)
            self.mapView.camera = camera
            
        }

        
        //        let source = pSourceLatitude+","+pSourceLongitude
        //        let destination = destLatitude+","+destLongitude
        
        //        let meetinglat = (strDestinationLat as NSString).doubleValue
        //        let meetinglong = (strDestinationLong as NSString).doubleValue
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(self.source)&destination=\(self.destination)&mode=driving&key="+kGoogleApiKey
        AF.request(url).responseJSON { response in
            
            let json = try? JSON(data: response.data!)
            let routes = json?["routes"].arrayValue
            
            if routes?.count == 0{
                
                //                    objAppShareData.showAlertVC(title: "Apoim", message: "Locations specified seems to be non-existent so a proper route between origin and destination cannot be determined.", controller: self)
            }
            
            for route in routes!
            {
                let routeOverviewPolyline = route["overview_polyline"].dictionary
                let points = routeOverviewPolyline?["points"]?.stringValue
                let path = GMSPath.init(fromEncodedPath: points!)
                let polyline = GMSPolyline.init(path: path)
                polyline.map = self.mapView
                
                polyline.strokeWidth = 3
                polyline.strokeColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                

                
                
            }
            
        }
        
        
        
        self.call_webServiceOfGoogleMapDirection()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let zoom = mapView.camera.zoom
        
        self.cameraPosition = mapView.camera
        
        self.zoomLevel = zoom
        
        
    }
    
    
    func my_current_location(){
        
        //        self.mapView.clear()
        
        //        let source = strCurrentLat+","+strCurrentLong
        //
        //        let destination = strDestinationLat+","+strDestinationLong
        
        let meetinglat = (strCurrentLat as NSString).doubleValue
        let meetinglong = (strCurrentLong as NSString).doubleValue
        
        let camera = GMSCameraPosition.camera(withLatitude: meetinglat, longitude: meetinglong, zoom: self.zoomLevel)
        
        self.mapView.camera = camera
        let position = CLLocationCoordinate2D(latitude: meetinglat, longitude: meetinglong)
        let marker = GMSMarker(position: position)
        marker.icon = UIImage(named: "car_driver-web")
        
        //        UserDefaults.standard.set(self.strCurrentLat, forKey: "currentLat")
        //        UserDefaults.standard.set(self.strCurrentLong, forKey: "currentLong")
        
        self.post_currentStatus()
        marker.map = self.mapView
        
    }
    
}

// MARK: - Webservice calling

extension EnRouteVC{
    
    func post_getCurrentTripInfo(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&vLang=EN&vDeviceType=ios&type=getDetail&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios&AppVersion=1.0.4&GeneralMemberId=\(userId)&iUserId=\(userId)&vUserDeviceCountry=\(country)&vDeviceToken=\(kDeviceToken)&vTimeZone=Asia/Kolkata&UserType=Driver"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                
                let action = json["Action"].stringValue
                if action == "1"{
                    let message = json["message"].dictionaryObject
                    let tripStatus = message?["vTripStatus"] as? String ?? ""
                    UserDefaults.standard.setValue(tripStatus, forKey: "TripStatus")

                    self.tripStatusIntial = tripStatus
                    self.pTripId = message?["iTripId"] as? String ?? ""
                    let obj = ModalCurrentTrip.init(dict: message ?? [:])
                    self.objTripDetail = obj
                    
                    if tripStatus == "Arrived"{
                        self.viewMessage.isHidden = true
                        self.viewEnterOtp.isHidden = true
                        self.viewBtnEnterOtp.isHidden = true
                        self.viewBtnArrived.isHidden = true
                        self.viewSlideBeginTrip.isHidden = false
//                        self.lblTitle.text! = "EN ROUTE"
                        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                        self.lblMessage.text! = objLang?["LBL_ARRIVED_CONFIRM_DIALOG_TXT"] as? String ?? ""
                        self.lblTitle.text! = objLang?["LBL_RIDER_EN_ROUTE_MAIN_SCREEN"] as? String ?? ""
                        self.lblPassengerPickUp.text! = self.pDestLocAddress
                        self.timerCurrentStatus = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.updateCurrentStatus), userInfo: nil, repeats: true)
                        
                        UserDefaults.standard.setValue(tripStatus, forKey: "TripStatus")

                    }
                    if tripStatus == "On Going Trip"{
                        self.viewMessage.isHidden = true
                        self.viewEnterOtp.isHidden = true
                        self.viewBtnEnterOtp.isHidden = true
                        self.viewBtnArrived.isHidden = true
                        self.viewSlideBeginTrip.isHidden = false
                        self.lblTitle.text! = "EN ROUTE"
                        self.isTripBegin = true
//                        self.lblBeginEndTrip.text! = "SLIDE TO END TRIP"
                        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                        self.lblBeginEndTrip.text! = objLang?["LBL_BTN_SLIDE_END_TRIP_TXT"] as? String ?? ""
                        self.timerCurrentStatus = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.updateCurrentStatus), userInfo: nil, repeats: true)
                        self.imgSlideBeginEnd.image = UIImage(named: "passenger end")
                        
                        UserDefaults.standard.setValue(tripStatus, forKey: "TripStatus")

                    }
                    if tripStatus == "Cancelled"{
                        self.viewMessage.isHidden = false
                        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                        self.lblMessage.text! = objLang?["LBL_SUCCESS_TRIP_CANCELED"] as? String ?? ""
                        
                        UserDefaults.standard.setValue(tripStatus, forKey: "TripStatus")

                    }
                    
                    self.passenger_details()
                    self.driver_details()
                    self.location_details()
                }
                
                
                
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    func post_driverArrived(){
        ValidationManager.sharedManager.showLoader()
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=DriverArrived&GeneralUserType=Driver&TripId=\(self.pTripId)&tSessionId=\(sessionId)&iDriverId=\(userId)&GeneralDeviceType=ios"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let message = json["message"].dictionaryObject
                self.destLatitude = message?["DLatitude"] as? String ?? ""
                self.destLongitude = message?["DLongitude"] as? String ?? ""
                self.destAddress = message?["DAddress"] as? String ?? ""
                
                let action = json["Action"].stringValue
                if action == "1"{
                    self.lblPassengerPickUp.text! = self.destAddress
                    self.imgSlideBeginEnd.image = UIImage(named: "passenger begin")
                    self.post_getCurrentTripInfo()
                    
                }
                
                
                ValidationManager.sharedManager.hideLoader()
                
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    func post_startTrip(){
        ValidationManager.sharedManager.showLoader()

        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&iVerificationCode=\("")&type=StartTrip&GeneralUserType=Driver&TripID=\(self.pTripId)&tSessionId=\(sessionId)&iDriverId=\(userId)&GeneralDeviceType=Android&GeneralMemberId=\(userId)&iUserId=\(self.pPassengerId)&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&UserType=Driver"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        //
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    self.isTripBegin = true
                    self.lblBeginEndTrip.text! = "SLIDE TO END TRIP"
                    self.timerCurrentStatus = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.updateCurrentStatus), userInfo: nil, repeats: true)
                    
                    self.imgSlideBeginEnd.image = UIImage(named: "passenger end")
                    
                }
                
                
                ValidationManager.sharedManager.hideLoader()
                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
                
            }
        }
        
    }
    
    func post_endTrip(){
        ValidationManager.sharedManager.showLoader()
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&PassengerId=\(self.pPassengerId)&fMaterialFee=0.0&GeneralAppVersion=1.0.4&fDriverDiscount=0.0&waitingTime=0&type=ProcessEndTrip&GeneralUserType=Driver&TripId=\(self.pTripId)&tSessionId=\(sessionId)&GeneralDeviceType=Android&latList=\(self.strCurrentLat)+\(self.strDestinationLat)&GeneralMemberId=\(userId)&DriverId=\(userId)&fMiscFee=0.0&lonList=\(self.strCurrentLong)+\(self.strDestinationLong)&vUserDeviceCountry=\(country)&dAddress=\(self.pDestLocAddress)&dest_lat=\(strDestinationLat)&dest_lon=\(strDestinationLong)"
        
        //        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&PassengerId=\(self.pPassengerId)&fMaterialFee=0.0&GeneralAppVersion=1.0.4&fDriverDiscount=0.0&waitingTime=0&type=ProcessEndTrip&GeneralUserType=Driver&TripId=\(self.pTripId)&tSessionId=\(sessionId)&GeneralDeviceType=Android&latList=22.818017+22.7195687&GeneralMemberId=\(userId)&DriverId=\(userId)&fMiscFee=0.0&lonList=75.927865+75.8577258&vUserDeviceCountry=\(country)&dAddress=\(self.pDestLocAddress)&dest_lat=22.6885304&dest_lon=75.86788"
        
        //        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&PassengerId=\(self.pPassengerId)&fMaterialFee=0.0&GeneralAppVersion=1.0.4&fDriverDiscount=0.0&waitingTime=0&type=ProcessEndTrip&GeneralUserType=Driver&TripId=\(self.pTripId)&tSessionId=\(sessionId)&GeneralDeviceType=Android&GeneralMemberId=\(userId)&DriverId=\(userId)&fMiscFee=0.0&tStartLat=22.818017&tStartLong=75.927865&tEndLat=22.7195687&tEndLong=75.8577258vUserDeviceCountry=\(country)&dAddress=\(self.pDestLocAddress)"
        
        //    let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&PassengerId=\(pPassengerId)&fMaterialFee=0.0&GeneralAppVersion=1.0.4&fDriverDiscount=0.0&waitingTime=0&type=ProcessEndTrip&GeneralUserType=Driver&TripId=\(pTripId)&tSessionId=\(sessionId)&GeneralDeviceType=Android&latList=22.6884813%2C+22.6884851%2C+22.6885239&GeneralMemberId=\(userId)&DriverId=\(userId)&fMiscFee=0.0&lonList=75.8679141%2C+75.8679145%2C+75.867888&vUserDeviceCountry=in&dAddress=+Indrapuri+Colony%2C+Indra+Puri+Colony%2C+Indore%2C+Madhya+Pradesh+452014%2C+India&dest_lat=22.6885304&dest_lon=75.86788&vTimeZone=Asia%2FKolkata"
        
        
        
        
        print(urlString)
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                self.endTotalFare = json["TotalFare"].stringValue
                self.currencySymbol = json["CurrencySymbol"].stringValue
                
                let action = json["Action"].stringValue
                
                DispatchQueue.main.asyncAfter(deadline: .now()+4){
                    if action == "1"{
                        ValidationManager.sharedManager.showLoader()

                        let vc:FinalFareVC = self.storyboard?.instantiateViewController(withIdentifier: "FinalFareVC") as! FinalFareVC
                        vc.endTotalFare = self.endTotalFare
                        vc.strTripId = self.pTripId
                        self.navigationController?.pushViewController(vc, animated: true)
                        

                    }
                }
                


                
                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()


            }
        }
        
    }
    
    
    func post_cancelTrip(){
        ValidationManager.sharedManager.showLoader()
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&Comment=\(txtCancelTripComment.text!)&GeneralAppVersion=1.0.4&type=cancelTrip&GeneralUserType=Driver&Reason=\(txtCancelTripReason.text!)&tSessionId=\(sessionId)&iDriverId=\(userId)&GeneralDeviceType=Android&General MemberId=\(userId)&iUserId=\(pPassengerId)&iTripId=\(pTripId)&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&UserType=Driver"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    self.navigate_session()
                }
                
                ValidationManager.sharedManager.hideLoader()
                
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    func post_currentStatus(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
//        self.strCurrentLat = "22.703752"
//        self.strCurrentLong = "75.873387"
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralAppVersion=1.0.4&vLongitude=\(strCurrentLong)&iMemberId=\(userId)&type=configDriverTripStatus&GeneralUserType=Driver&isSubsToCabReq=false&tSessionId=\(sessionId)&GeneralDeviceType=ios&GeneralMemberId=\(userId)&vLatitude=\(strCurrentLat)&iTripId=\(pTripId)&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&UserType=Driver"
        
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralAppVersion=1.0.4&vLongitude=\(simulateCurrentLong)&iMemberId=\(userId)&type=configDriverTripStatus&GeneralUserType=Driver&isSubsToCabReq=false&tSessionId=\(sessionId)&GeneralDeviceType=ios&GeneralMemberId=\(userId)&vLatitude=\(simulateCurrentLat)&iTripId=\(pTripId)&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&UserType=Driver"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                
                if action == "1"{
                    print("action of current status \(action)")
                }else{
                    print("action of current status is car not moving")
                }
                
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    func post_emergencyContact(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?iUserId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=loadEmergencyContacts&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let actionContactList = json["Action"].stringValue
                print("action is \(actionContactList)")
                
                if let message = json["message"].arrayObject{
                    print(message)
                    
                    for index in 0..<message.count{
                        let obj = message[index]
                        let modalObj = ModalEmergencyContactList.init(dict: obj as? [String : Any] ?? [:])
                        self.arrEmergencyContactList.append(modalObj)
                    }
                    self.tableViewContactList.reloadData()
                    
                }
                
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
}
