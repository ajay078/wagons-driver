//
//  AcceptRequestVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 17/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView


class AcceptRequestVC: UIViewController {

    let shapeLayer = CAShapeLayer()
    let shapeLayerWhite = CAShapeLayer()
    
    
    var passengerId = ""
    var pName = ""
    var pRating = ""
    var pSourceLatitude = ""
    var pSourceLongitude = ""
    var pDestLatitude = ""
    var pDestLongitude = ""
    var pITripId = ""
    var pICabRequestId:Int = 0
    var pMsgCode = ""
    var pTripId = ""
    
    var pPicName = ""
    var pPassengerId = ""
    var pDestLocAddress = ""
    
    var pCurrentAddress = ""
    var strTripRating:String = ""

    var iCabBookingId = ""
    
    
    var secounds = 30
    var timerCountDown = Timer()
    
    @IBOutlet weak var lblPickUp: UILabel!
    @IBOutlet weak var lblDestination: UILabel!
    @IBOutlet weak var lblPassengerName: UILabel!
    
    @IBOutlet weak var viewBtnDecline: UIView!
    @IBOutlet weak var viewMidLogo: UIView!
    @IBOutlet weak var viewBtnAccept: UIView!

    @IBOutlet weak var lblCountDown: UILabel!
    
    @IBOutlet weak var imgRating1: UIImageView!
    @IBOutlet weak var imgRating2: UIImageView!
    @IBOutlet weak var imgRating3: UIImageView!
    @IBOutlet weak var imgRating4: UIImageView!
    @IBOutlet weak var imgRating5: UIImageView!
    
    @IBOutlet weak var viewUpar: UIView!


    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
    
    
// MARK: - View Did Load

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.corner_radius()
//        self.post_passengerDetails()
        
        self.timerCountDown = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        DispatchQueue.main.asyncAfter(deadline: .now()+0){
            self.loadCircle()
        }
        let center = view.center
        
        let circularPathWhite = UIBezierPath(arcCenter: center, radius: 120, startAngle: -CGFloat.pi/2, endAngle: 2 * CGFloat.pi, clockwise: true)
        shapeLayerWhite.path = circularPathWhite.cgPath
        
        shapeLayerWhite.strokeColor = UIColor.white.cgColor
        shapeLayerWhite.lineWidth = 20
        shapeLayerWhite.fillColor = UIColor.clear.cgColor

        view.layer.addSublayer(shapeLayerWhite)

        let circularPath = UIBezierPath(arcCenter: center, radius: 120, startAngle: -CGFloat.pi/2, endAngle: 2 * CGFloat.pi, clockwise: true)
        shapeLayer.path = circularPath.cgPath

        shapeLayer.strokeColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        shapeLayer.lineWidth = 20
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeEnd = 0
        view.layer.addSublayer(shapeLayer)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }
    
    @objc func updateTimer() {
        
        secounds -= 1
        lblCountDown.text! = "\(secounds)"
        
        if lblCountDown.text! == "0"{
            self.timerCountDown.invalidate()
            self.post_declineRequest()
            
        }
        
        
    }
    
    @objc func handleTap(){
        print("start animate")

    }
    
    
    
    func loadCircle(){
        print("start animate")
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 1
        basicAnimation.duration = 38
        
//        basicAnimation.fillMode =
        basicAnimation.isRemovedOnCompletion = false
        
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
        
    }
    
    
    
    
    @IBAction func btnDecline(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnDecline.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        self.post_declineRequest()
    }
    @IBAction func btnAccept(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnAccept.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        self.post_acceptRequest()
        
//        let vc:EnRouteVC = self.storyboard?.instantiateViewController(withIdentifier: "EnRouteVC") as! EnRouteVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func post_passengerDetails(){
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?iCabRequestId=\(self.pICabRequestId)&GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=getCabRequestAddress&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                }
                
                let message = json["message"].dictionaryObject
                self.lblPickUp.text! = message?["tSourceAddress"] as? String ?? ""
                self.pCurrentAddress = message?["tSourceAddress"] as? String ?? ""
                self.lblDestination.text! = message?["tDestAddress"] as? String ?? ""
                self.details()

                
            case .failure(let error):
                print(error)
            }
        }
        
    }

    
    func post_declineRequest(){
        
        self.activityIndicatorView.startAnimating()
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&vFirebaseDeviceToken=\(kDeviceToken)&DriverID=\(userId)&PassengerID=\(self.passengerId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&vMsgCode=\(pMsgCode)&type=DeclineTripRequest&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
//                let json = JSON(value)
//                print(json)
                    
                let response = value as? Int ?? 0
                print(response)
                
                if response == 1{
                    let vc:HomePageVC = self.storyboard?.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
                    self.navigationController?.pushViewController(vc, animated: false)
                }else{
                    
                }
                
                self.activityIndicatorView.stopAnimating()
                
            case .failure(let error):
                print(error)
                self.activityIndicatorView.stopAnimating()
                
            }
        }
        
    }
    
    
    func post_acceptRequest(){
        
        if CheckInternet.connection(){
            
            self.activityIndicatorView.startAnimating()
            let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
            let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
            let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
            
            let urlString = "https://apps.wagonstaxi.com/webservice.php?iCabRequestId=\("")&vFirebaseDeviceToken=\(kDeviceToken)&PassengerID=\(self.passengerId)&GeneralAppVersion=1.0.4&vMsgCode=\(self.pMsgCode)&type=GenerateTrip&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android &GeneralMemberId=\(userId)&DriverID=\(userId)&GoogleServerKey=\(kGoogleApiKey)&start_lon=\(self.pSourceLongitude)&iCabBookingId=\(iCabBookingId)&vUserDeviceCountry=\(country)&sAddress=\(self.pCurrentAddress)&start_lat=\(self.pSourceLatitude)&vTimeZone=Asia/Kolkata&UserType=Driver"
            
            let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            let searchURL = URL(string: newURL ?? "")!
            
            let headers:HTTPHeaders = ["Authorization" : "Bearer",
            "Content-Type": "application/json"]
            
            AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
                
                
                switch responseObject.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    
                    _ = json["message"].dictionaryObject
                    self.pName = json["PName"].stringValue
                    self.pRating = json["PRating"].stringValue
                    self.pPicName = json["PPicName"].stringValue
                    self.pPassengerId = json["PassengerId"].stringValue
                    self.pDestLocAddress = json["DestLocAddress"].stringValue
                    self.pTripId = json["TripId"].stringValue
                    self.pSourceLatitude = json["sourceLatitude"].stringValue
                    self.pSourceLongitude = json["sourceLongitude"].stringValue
                    
                    let action = json["Action"].stringValue
                    if action == "1"{
                        self.timerCountDown.invalidate()
                        let vc:EnRouteVC = self.storyboard?.instantiateViewController(withIdentifier: "EnRouteVC") as! EnRouteVC
//                        vc.pPassengerPickUp = self.pCurrentAddress
//                        vc.pName = self.pName
//                        vc.pRating = self.pRating
//                        vc.pPicName = self.pPicName
//                        vc.pPassengerId = self.pPassengerId
//                        vc.pDestLocAddress = self.pDestLocAddress
//                        vc.pTripId = self.pTripId
//                        vc.pSourceLatitude = self.pSourceLatitude
//                        vc.pSourceLongitude = self.pSourceLongitude
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    
                    self.activityIndicatorView.stopAnimating()
                    
                case .failure(let error):
                    print(error)
                    self.activityIndicatorView.stopAnimating()
                    
                }
            }
            
        }else{
            alert_show(message: "Please check your internet connection")
            
        }
        
    }
    
    
    func corner_radius(){
        
        viewMidLogo.layer.cornerRadius = viewMidLogo.frame.size.height/2
        //        viewCircleOuter.layer.cornerRadius = viewCircleOuter.frame.size.height/2
        //        viewCircleInner.layer.cornerRadius = viewCircleInner.frame.size.height/2
        
    }
    
    func colorMenuButton(){
        
        self.viewBtnDecline.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.viewBtnAccept.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

    }
    
    func details(){
        
        self.lblPassengerName.text! = self.pName
        
        self.strTripRating = self.pRating
        
        let doubleRating = Double(strTripRating) ?? 0.0
        let IntRating:Int = Int(round(doubleRating))
        self.get_rating(rating: IntRating)
        
        
    }
    
    
    func get_rating(rating:Int){
        
        switch rating {
        case 1:
            imgRating1.image = UIImage(named: "star yellow")
        case 2:
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            
        case 3:
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            imgRating3.image = UIImage(named: "star yellow")
            
            
        case 4:
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            imgRating3.image = UIImage(named: "star yellow")
            imgRating4.image = UIImage(named: "star yellow")
            
            
        case 5:
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            imgRating3.image = UIImage(named: "star yellow")
            imgRating4.image = UIImage(named: "star yellow")
            imgRating5.image = UIImage(named: "star yellow")
            
            
        default:
            break
        }
        
    }
    
    func progressCircle(){
        

        
        
    }
    

}
