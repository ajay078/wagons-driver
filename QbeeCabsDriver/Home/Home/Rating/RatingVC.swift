//
//  RatingVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 22/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleMaps
import NVActivityIndicatorView

class RatingVC: UIViewController, UITextViewDelegate {

    var window: UIWindow?
    var navigationcontroller: UINavigationController?
    var objTripDetail = ModalCurrentTrip(dict: [:])

    var pRating = ""
    var strTripRating:String = ""

    var strRating:String = ""
    var strTripId:String = ""
    
    var strCurrentLat:String = ""
    var strCurrentLong:String = ""
    var strDestinationLat:String = ""
    var strDestinationLong:String = ""
    
    
    let locManager = CLLocationManager()
    var zoomLevel:Float = 15
    var cameraPosition = GMSCameraPosition()
    
    @IBOutlet weak var lblPassengerName: UILabel!
    @IBOutlet weak var textComment: UITextView!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    
    @IBOutlet weak var imgRating1: UIImageView!
    @IBOutlet weak var imgRating2: UIImageView!
    @IBOutlet weak var imgRating3: UIImageView!
    @IBOutlet weak var imgRating4: UIImageView!
    @IBOutlet weak var imgRating5: UIImageView!
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var viewPleaseRateL: UIView!
    @IBOutlet weak var lblPleaseRateL: UILabel!
    @IBOutlet weak var viewPassengerNameL: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
    
//MARK:- View Did Load

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.post_tripDetails()
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    
//MARK:- Button Actions

    
    @IBAction func btnSubmit(_ sender: Any) {
        self.view.endEditing(true)
        if strRating.isEmpty{
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            let textAlert = objLang?["LBL_RIDER_ERROR_RATING_DIALOG_RATING"] as? String ?? ""
            let textOk = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
            ValidationManager.sharedManager.showAlert(message: textAlert, title: "", btnTitle: textOk, controller: self)
            
        }else{
            self.post_giveRating()
        }
        
        
    }
    

    @IBAction func btnRating(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            self.giveRating()
            imgRating1.image = UIImage(named: "star yellow")
            self.strRating = "1"
        case 1:
            self.giveRating()
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            self.strRating = "2"

        case 2:
            self.giveRating()
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            imgRating3.image = UIImage(named: "star yellow")
            self.strRating = "3"

            
        case 3:
            self.giveRating()
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            imgRating3.image = UIImage(named: "star yellow")
            imgRating4.image = UIImage(named: "star yellow")
            self.strRating = "4"

            
        case 4:
            self.giveRating()
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            imgRating3.image = UIImage(named: "star yellow")
            imgRating4.image = UIImage(named: "star yellow")
            imgRating5.image = UIImage(named: "star yellow")
            self.strRating = "5"

            
            
            
        default:
            break
        }
        
        
        
        
    }
    
    
    // MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        

        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_RATING_RIDE_DETAIL"] as? String ?? ""
        textComment.text! = objLang?["LBL_WRITE_COMMENT_HINT_RATING"] as? String ?? ""
        lblPleaseRateL.text! = objLang?["LBL_RIDER_ERROR_RATING_DIALOG_RIDE_DETAIL"] as? String ?? ""
        btnSubmit.setTitle(objLang?["LBL_BTN_SUBMIT_TXT_DELIVERY_DETAIL"] as? String ?? "", for: .normal)

        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            self.textComment.textAlignment = .right
            self.lblPleaseRateL.textAlignment = .right
            self.lblPassengerName.textAlignment = .right

            self.viewUpar.semanticContentAttribute = .forceRightToLeft
            self.viewPleaseRateL.semanticContentAttribute = .forceRightToLeft
            self.viewPassengerNameL.semanticContentAttribute = .forceRightToLeft


        }else{

        }
        
        textComment.delegate = self
        textComment.text! = objLang?["LBL_RIDER_WRITE_COMMENT_HINT_RATING"] as? String ?? ""
        textComment.textColor = UIColor.lightGray
        
    }
    
    
    
    // MARK: - Text View Delegate

    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textComment.textColor == UIColor.lightGray {
            textComment.text = nil
            textComment.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textComment.text.isEmpty {
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            textComment.text! = objLang?["LBL_RIDER_WRITE_COMMENT_HINT_RATING"] as? String ?? ""
            textComment.textColor = UIColor.lightGray
        }
    }
    
    
    func giveRating(){
        
        imgRating1.image = UIImage(named: "star")
        imgRating2.image = UIImage(named: "star")
        imgRating3.image = UIImage(named: "star")
        imgRating4.image = UIImage(named: "star")
        imgRating5.image = UIImage(named: "star")
        
        
    }
    
    func details(){
        
        
        
        
        let obj = objTripDetail.tripDetails.passengerDetails
        let pName = "\(obj.firstName) \(obj.lastName)"
        self.lblPassengerName.text! = pName
        
        self.strTripId = objTripDetail.tripDetails.iTripId
    }
    
    func location_details(){
        
        let obj = objTripDetail.tripDetails
        
        strCurrentLat = obj.tStartLat
        strCurrentLong = obj.tStartLong
        strDestinationLat = obj.tEndLat
        strDestinationLong = obj.tEndLong

        DispatchQueue.main.asyncAfter(deadline: .now()+0.5){
            self.draw_googleMapDirection()
        }
        
    }
    
    func navigate_home(){
//        let sb: UIStoryboard = UIStoryboard(name: "HomeSB", bundle:Bundle.main)
//        navigationcontroller = sb.instantiateViewController(withIdentifier: "navHome") as? UINavigationController
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window?.rootViewController = navigationcontroller
//        window?.makeKeyAndVisible()
        
        let sb: UIStoryboard = UIStoryboard(name: "HomeSB", bundle:Bundle.main)
        navigationcontroller = sb.instantiateViewController(withIdentifier: "navSession") as? UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationcontroller
        window?.makeKeyAndVisible()
    }
 

    
    
}



//MARK:- Draw polyLine Google map
extension RatingVC{
    
    func PlaceAPIWork(){
        
       
        locManager.requestAlwaysAuthorization()
        
        var currentLocation: CLLocation!
        
        if
            CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways
        {
            currentLocation = locManager.location
        }

        self.draw_googleMapDirection()
    }
    
    func draw_googleMapDirection(){

        self.mapView.clear()

        let source = strCurrentLat+","+strCurrentLong
        let destination = strDestinationLat+","+strDestinationLong
        
        let intialLat = (strCurrentLat as NSString).doubleValue
        let intialLong = (strCurrentLong as NSString).doubleValue
        
        let position = CLLocationCoordinate2D(latitude: intialLat, longitude: intialLong)
        let marker = GMSMarker(position: position)
        marker.icon = UIImage(named: "car_driver-web")
        marker.map = self.mapView
        
        let UserLat:Double = (strDestinationLat as NSString).doubleValue
        let UserLong:Double = (strDestinationLong as NSString).doubleValue
        
        let position_1 = CLLocationCoordinate2D(latitude: UserLat, longitude: UserLong)
        let marker_1 = GMSMarker(position: position_1)
        marker_1.map = self.mapView
        
        marker_1.icon = UIImage(named: "ico_map_pin_m")

        let camera = GMSCameraPosition.camera(withLatitude: UserLat, longitude: UserLong, zoom: self.zoomLevel)
        self.mapView.camera = camera

            let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source)&destination=\(destination)&mode=driving&key="+kGoogleApiKey
            AF.request(url).responseJSON { response in

                    let json = try? JSON(data: response.data!)
                let routes = json?["routes"].arrayValue

                if routes?.count == 0{
                    
//                    objAppShareData.showAlertVC(title: "Apoim", message: "Locations specified seems to be non-existent so a proper route between origin and destination cannot be determined.", controller: self)
                }

                for route in routes!
                    {
                        let routeOverviewPolyline = route["overview_polyline"].dictionary
                        let points = routeOverviewPolyline?["points"]?.stringValue
                        let path = GMSPath.init(fromEncodedPath: points!)
                        let polyline = GMSPolyline.init(path: path)
                        polyline.map = self.mapView
                        marker_1.map = self.mapView

                        polyline.strokeWidth = 3
                        polyline.strokeColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//                        marker_1.icon = UIImage(named: "ico_current_red")


                }

            }

       

        marker.map = self.mapView
    }

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let zoom = mapView.camera.zoom

        self.cameraPosition = mapView.camera

        self.zoomLevel = zoom
    }
}


// MARK: - Webservice calling

extension RatingVC{
    
    func post_tripDetails(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        self.activityIndicatorView.startAnimating()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&vLang=EN&vDeviceType=ios&type=getDetail&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios&AppVersion=1.0.4&GeneralMemberId=\(userId)&iUserId=\(userId)&vUserDeviceCountry=\(country)&vDeviceToken=\(kDeviceToken)&vTimeZone=Asia/Kolkata&UserType=Driver"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                
                let action = json["Action"].stringValue
                if action == "1"{
                    let message = json["message"].dictionaryObject
                    let obj = ModalCurrentTrip.init(dict: message ?? [:])
                    self.objTripDetail = obj
                    

                    self.details()
                    self.location_details()
                    self.activityIndicatorView.stopAnimating()
                }
                
                
                
                
            case .failure(let error):
                print(error)
                self.activityIndicatorView.stopAnimating()
            }
        }
        
    }
    
    
    
    
    
    func post_giveRating(){
        
        self.activityIndicatorView.startAnimating()
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&rating=\(self.strRating)&tripID=\(self.strTripId)&type=submitRating&message=\(textComment.text!)&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android&General MemberId=\(userId)&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&iGeneralUserId=\(userId)&UserType=Driver"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    
                    UserDefaults.standard.setValue("Done", forKey: "Ratings_From_Driver")
                    
                    self.navigate_home()
                }
                
                
                
                
                self.activityIndicatorView.stopAnimating()

                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
}
