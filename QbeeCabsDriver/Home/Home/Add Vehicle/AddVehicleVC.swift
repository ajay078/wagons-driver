//
//  AddVehicleVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 22/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddVehicleVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var objEditVehicle = ModalHomeSelectVehicle(dict: [:])
    var isFromEditVehicle:Bool = false
    
    
    var arrListVehicle = [MListVehicle]()
    var arrListVehicleModal = [MListVehicleModal]()
    var arrListYear = [""]
    
    var iMakeId:String = ""

    var isFromMake:Bool =  false
    var isFromModal:Bool = false
    var isFromYear:Bool = false
    
    var isBike:Bool = false
    var isSuv:Bool = false
    var isSedan:Bool = false
    
    var addMakeId = ""
    var addModalId = ""
    var addYear = ""
    var addCarType = ""
    
    
    var selectedIndex:Int = 10000
    
    // MARK: - Outlets

    @IBOutlet weak var lblMake: UILabel!
    @IBOutlet weak var lblSelectedVehicle: UILabel!
    @IBOutlet weak var lblHeader: UILabel!

    @IBOutlet weak var lblModal: UILabel!
    @IBOutlet weak var lblSelectedModal: UILabel!
    
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblSelectedYear: UILabel!
    
    @IBOutlet weak var txtLicenceNumber: UITextField!
    @IBOutlet weak var txtColor: UITextField!
    
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewListVehicle: UIView!
    @IBOutlet var viewListVehicleIn: UIView!
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewMessageIn: UIView!
    
    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var lblAlert: UILabel!
    @IBOutlet weak var bottomViewAlert: NSLayoutConstraint!
    
    @IBOutlet weak var bottomMake: NSLayoutConstraint!
    @IBOutlet weak var bottomModal: NSLayoutConstraint!
    @IBOutlet weak var bottomYear: NSLayoutConstraint!
    
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    
    @IBOutlet weak var imgCheckBike: UIImageView!
    @IBOutlet weak var imgCheckSuv: UIImageView!
    @IBOutlet weak var imgCheckSedan: UIImageView!
    
    
    @IBOutlet weak var viewUpar: UIView!

    @IBOutlet weak var viewMakeL: UIView!
    @IBOutlet weak var viewModalL: UIView!
    @IBOutlet weak var viewYearL: UIView!
    @IBOutlet weak var viewLicensePlateL: UIView!
    @IBOutlet weak var viewColorL: UIView!
    @IBOutlet weak var viewBikeL: UIView!
    @IBOutlet weak var viewSuvL: UIView!
    @IBOutlet weak var viewSedanL: UIView!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var lblLicencePlateL: UILabel!
    @IBOutlet weak var lblColorL: UILabel!
    @IBOutlet weak var lblBikeL: UILabel!
    @IBOutlet weak var lblSuvL: UILabel!
    @IBOutlet weak var lblSedanL: UILabel!
    
    @IBOutlet var lblSelectMakeL: UILabel!
    @IBOutlet var viewSelectMakeL: UIView!
    
    
    
    @IBOutlet weak var tableViewListVehicle: UITableView!
    
    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.viewListVehicle.isHidden = true
        self.selected_vehicle()
        self.post_vehicleDetails()
        self.corner_radius()
        self.fromEditVehicle()


    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBackListVehicle(_ sender: Any) {
//        self.viewListVehicle.isHidden = true
        ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: self.viewListVehicle, viewPopUP: self.viewListVehicleIn)


    }
    @IBAction func btnSelectVehicle(_ sender: Any) {
        self.view.endEditing(true)
        
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblSelectMakeL.text! = objLang?["LBL_CHOOSE_MAKE"] as? String ?? ""
        
        isFromMake = true
        isFromModal = false
        isFromYear = false
        self.updateTableViewHeightConstraint(arrayCount: arrListVehicle.count)
        
        ValidationManager.sharedManager.showMainViewNN(viewMain: viewListVehicle, viewInside: viewListVehicleIn)
        
//        self.viewListVehicle.isHidden = false
        
        self.tableViewListVehicle.reloadData()
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func btnSelectedModal(_ sender: Any) {
        self.view.endEditing(true)
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblSelectMakeL.text! = objLang?["LBL_SELECT_MODEL"] as? String ?? ""
        
        if let objVehicle = arrListVehicle.filter({$0.iMakeId == self.iMakeId}).first{
            self.arrListVehicleModal = objVehicle.arrListVehicleModal
        }
        isFromMake = false
        isFromModal = true
        isFromYear = false
        
        self.updateTableViewHeightConstraint(arrayCount: arrListVehicleModal.count)
        ValidationManager.sharedManager.showMainViewNN(viewMain: viewListVehicle, viewInside: viewListVehicleIn)

//        self.viewListVehicle.isHidden = false
        

        self.tableViewListVehicle.reloadData()
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func btnSelectedYear(_ sender: Any) {
        self.view.endEditing(true)
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblSelectMakeL.text! = objLang?["LBL_CHOOSE_YEAR"] as? String ?? ""
        
        isFromMake = false
        isFromModal = false
        isFromYear = true
        self.updateTableViewHeightConstraint(arrayCount: arrListYear.count)
//        self.viewListVehicle.isHidden = false
        ValidationManager.sharedManager.showMainViewNN(viewMain: viewListVehicle, viewInside: viewListVehicleIn)

        self.tableViewListVehicle.reloadData()
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func checkBike(_ sender: Any) {
        self.view.endEditing(true)
        if isBike == false{
            self.imgCheckBike.image = UIImage(named: "check yellow")
            self.addCarType = "5"
//            widthCheckBike.constant = 20
//            heightCheckBike.constant = 20
//            UIView.animate(withDuration: 1) {
//                self.view.layoutIfNeeded()
//            }
            isBike = true
        }else{
            self.imgCheckBike.image = nil
            isBike = false
        }

        
    }
    @IBAction func checkSuv(_ sender: Any) {
        self.view.endEditing(true)
        if isSuv == false{
            self.imgCheckSuv.image = UIImage(named: "check yellow")
            self.addCarType = "16"
            isSuv = true
        }else{
            self.imgCheckSuv.image = nil
            isSuv = false
        }
    }
    @IBAction func checkSedan(_ sender: Any) {
        self.view.endEditing(true)
        if isSedan == false{
            self.imgCheckSedan.image = UIImage(named: "check yellow")
            self.addCarType = "23"
            isSedan = true
        }else{
            self.imgCheckSedan.image = nil
            isSedan = false
        }
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        self.view.endEditing(true)
        self.empty_selection()
//        self.viewMessage.isHidden = false
    }
    
    
    @IBAction func btnUploadDocument(_ sender: Any) {
        let vc:SelectDocumentVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectDocumentVC") as! SelectDocumentVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnSkip(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
//        let vc:ManageVehicleVC = self.storyboard?.instantiateViewController(withIdentifier: "ManageVehicleVC") as! ManageVehicleVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    

// MARK: - Table View Methods

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFromMake == true{
            return arrListVehicle.count
        }else if isFromModal == true{
            return arrListVehicleModal.count
        }else{
            return arrListYear.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AddListVehicleCell = tableView.dequeueReusableCell(withIdentifier: "AddListVehicleCell", for: indexPath) as! AddListVehicleCell
        
        if isFromMake == true{
            let obj = arrListVehicle[indexPath.row]
            cell.lblText.text! = obj.vMake
            
            
        }else if isFromModal == true{
            let obj = arrListVehicleModal[indexPath.row]
            cell.lblText.text! = obj.vTitle
            
        }else if isFromYear == true {
            let obj = arrListYear[indexPath.row]
            cell.lblText.text! = obj
            
        }
        
        if selectedIndex == indexPath.row{
            cell.viewCell.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        }else{
            cell.viewCell.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath.row
        self.tableViewListVehicle.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2){
//            self.viewListVehicle.isHidden = true
            ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: self.viewListVehicle, viewPopUP: self.viewListVehicleIn)
        }
        
        if isFromMake == true{
            self.lblSelectedVehicle.text! = arrListVehicle[indexPath.row].vMake
            self.addMakeId = arrListVehicle[indexPath.row].iMakeId
            self.iMakeId = arrListVehicle[indexPath.row].iMakeId
            bottomMake.constant = 25
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
            lblMake.font = lblMake.font.withSize(15)
            isFromMake = false
        }else if isFromModal == true{
            self.lblSelectedModal.text! = arrListVehicleModal[indexPath.row].vTitle
            self.addModalId = arrListVehicleModal[indexPath.row].iModelId
            bottomModal.constant = 25
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
            lblModal.font = lblMake.font.withSize(15)
            isFromModal = false
        }else if isFromYear == true{
            self.lblSelectedYear.text! = arrListYear[indexPath.row]
            self.addYear = arrListYear[indexPath.row]
            bottomYear.constant = 25
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
            lblYear.font = lblMake.font.withSize(15)
            isFromYear = false
        }
        self.tableViewListVehicle.reloadData()
    }
    
    
    
    
    
    func selected_vehicle(){
        if lblSelectedVehicle.text!.isEmpty{
            bottomMake.constant = 0
            lblMake.font = lblMake.font.withSize(20)
            
            bottomModal.constant = 0
            lblModal.font = lblMake.font.withSize(20)
            
            bottomYear.constant = 0
            lblYear.font = lblMake.font.withSize(20)
        }else{
            bottomMake.constant = 25
            lblMake.font = lblMake.font.withSize(5)
            lblMake.text! = ""


        }
    }
    

    
    func moveLable(){
        bottomMake.constant = 25
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
        lblMake.font = lblMake.font.withSize(15)
        
        bottomModal.constant = 25
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
        lblModal.font = lblMake.font.withSize(15)
        
        bottomYear.constant = 25
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
        lblYear.font = lblMake.font.withSize(15)
    }
    

    
// MARK: - set Language Labels
       
       func setLanguageLabel(){
           
           let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
           lblHeader.text! = objLang?["LBL_ADD_VEHICLE"] as? String ?? ""

           let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft
            self.viewMakeL.semanticContentAttribute = .forceRightToLeft
            self.viewModalL.semanticContentAttribute = .forceRightToLeft
            self.viewYearL.semanticContentAttribute = .forceRightToLeft
            self.viewLicensePlateL.semanticContentAttribute = .forceRightToLeft
            self.viewColorL.semanticContentAttribute = .forceRightToLeft
            self.viewBikeL.semanticContentAttribute = .forceRightToLeft
            self.viewSuvL.semanticContentAttribute = .forceRightToLeft
            self.viewSedanL.semanticContentAttribute = .forceRightToLeft
            self.viewSelectMakeL.semanticContentAttribute = .forceRightToLeft

            
        }else{
    
           }
           
        lblMake.text! = objLang?["LBL_MAKE"] as? String ?? ""
        lblModal.text! = objLang?["LBL_MODEL"] as? String ?? ""
        lblYear.text! = objLang?["LBL_YEAR"] as? String ?? ""
        lblLicencePlateL.text! = objLang?["LBL_LICENCE_PLATE_WAY_BILL"] as? String ?? ""
        lblColorL.text! = objLang?["LBL_COLOR_ADD_VEHICLES"] as? String ?? ""
//        lblBikeL.text! = objLang?["LBL_YEAR"] as? String ?? ""
//        lblSuvL.text! = objLang?["LBL_YEAR"] as? String ?? ""
//        lblSedanL.text! = objLang?["LBL_YEAR"] as? String ?? ""
        btnSubmit.setTitle(objLang?["LBL_RIDER_BTN_SUBMIT_RATING"] as? String ?? "", for: .normal)


            
        
           
       }

    

    
    func corner_radius(){
        
        viewListVehicle.layer.cornerRadius = 5
        viewListVehicle.layer.masksToBounds = true
        
        viewMessage.isHidden = true
        viewMessageIn.layer.cornerRadius = 5
        viewMessageIn.layer.masksToBounds = true
        
        
        imgCheckBike.layer.borderWidth = 1
        imgCheckBike.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        imgCheckSuv.layer.borderWidth = 1
        imgCheckSuv.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        imgCheckSedan.layer.borderWidth = 1
        imgCheckSedan.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        
    }
    
    
    func fromEditVehicle(){
        
        if isFromEditVehicle == true{
            self.lblSelectedVehicle.text! = objEditVehicle.vMake
            self.addMakeId = objEditVehicle.iMakeId
            self.lblMake.textColor = UIColor.lightGray
            bottomMake.constant = 25
            lblMake.font = lblMake.font.withSize(15)
            self.lblSelectedModal.text! = objEditVehicle.vTitle
            self.addModalId = objEditVehicle.iModelId
            self.lblModal.textColor = UIColor.lightGray
            bottomModal.constant = 25
            lblModal.font = lblModal.font.withSize(15)
            self.lblSelectedYear.text! = objEditVehicle.iYear
            self.addYear = objEditVehicle.iYear
            self.lblYear.textColor = UIColor.lightGray
            lblYear.font = lblYear.font.withSize(15)
            bottomYear.constant = 25
            self.txtLicenceNumber.text! = objEditVehicle.vLicencePlate
            self.txtLicenceNumber.textColor = UIColor.black
            self.txtColor.text! = objEditVehicle.vColour
            self.txtColor.textColor = UIColor.black

            
            let vCarType = objEditVehicle.vCarType
            if vCarType == "5"{
                self.imgCheckBike.image = UIImage(named: "check yellow")
                self.addCarType = "5"
            }else if vCarType == "16"{
                self.imgCheckSuv.image = UIImage(named: "check yellow")
                self.addCarType = "16"
            }else if vCarType == "23"{
                self.imgCheckSedan.image = UIImage(named: "check yellow")
                self.addCarType = "23"
            }
            
            
        }
        
    }
    
    
    func empty_selection(){
        
        if lblSelectedVehicle.text!.isEmpty{
            self.bottomViewAlert.constant = 0
            self.lblAlert.text! = "Select Make"
            print(lblSelectedVehicle.text!)
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
            DispatchQueue.main.asyncAfter(deadline: .now()+3){
                self.bottomViewAlert.constant = -60
                UIView.animate(withDuration: 0.4) {
                    self.view.layoutIfNeeded()
                }
            }
        }else if lblSelectedModal.text!.isEmpty{
            self.bottomViewAlert.constant = 0
            self.lblAlert.text! = "Select Vehicle Modal"
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
            DispatchQueue.main.asyncAfter(deadline: .now()+3){
                self.bottomViewAlert.constant = -60
                UIView.animate(withDuration: 0.4) {
                    self.view.layoutIfNeeded()
                }
            }
        }else if lblSelectedYear.text!.isEmpty{
            self.bottomViewAlert.constant = 0
            self.lblAlert.text! = "Select Year"
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
            DispatchQueue.main.asyncAfter(deadline: .now()+3){
                self.bottomViewAlert.constant = -60
                UIView.animate(withDuration: 0.4) {
                    self.view.layoutIfNeeded()
                }
            }
        }else if txtLicenceNumber.text!.isEmpty{
            self.bottomViewAlert.constant = 0
            self.lblAlert.text! = "Please add your car's licence plate no."
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
            DispatchQueue.main.asyncAfter(deadline: .now()+3){
                self.bottomViewAlert.constant = -60
                UIView.animate(withDuration: 0.4) {
                    self.view.layoutIfNeeded()
                }
            }
        }else{
            self.post_addVehicle()
        }
        
        
    }
    
    func updateTableViewHeightConstraint(arrayCount:Int){
        
        var height = CGFloat(arrayCount ) * 60

        
        let screenHeight = UIScreen.main.bounds.height
        if height > (screenHeight - 220) {
            height = screenHeight - 220
        }
        
        heightTableView.constant = height
    }
    
    
}

// MARK: - Webservice calling


extension AddVehicleVC{
    
    func post_vehicleDetails(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        ValidationManager.sharedManager.showLoader()

        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&iMemberId=\(userId)&eType=Ride&vTimeZone=Asia %2FCalcutta&type=getUserVehicleDetails&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                }
                
                if let message = json["message"].dictionaryObject{
                    let arrTempYear = message["year"] as? [String] ?? [""]
                    self.arrListYear.removeAll()
                    for index in 0..<arrTempYear.count{
                        let obj = arrTempYear[index]
                        self.arrListYear.append(obj)
                    }
                    
                    let arrTempCarList = message["carlist"] as? [[String:Any]] ?? [[:]]
                    for index in 0..<arrTempCarList.count{
                        let obj = arrTempCarList[index]
                        let modalObj = MListVehicle.init(dict: obj)
                        self.arrListVehicle.append(modalObj)
                        
                    }

                    
                }

                self.tableViewListVehicle.reloadData()
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
    func post_addVehicle(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        ValidationManager.sharedManager.showLoader()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?HandiCap=No&vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&vLicencePlate=\(txtLicenceNumber.text!)&iDriverVehicleId=&type=UpdateDriverVehicle&vColor=\(txtColor.text!)&GeneralUserType=Driver&iMakeId=\(self.addMakeId)&iModelId=\(self.addModalId)&tSessionId=\(sessionId)&iDriverId=\(userId)&GeneralDeviceType=Android&GeneralMemberId=\(userId)&vUserDeviceCountry=\(country)&iYear=\(self.addYear)&vCarType=\(self.addCarType)&vTimeZone=Asia/Kolkata&UserType=Driver"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                
                if action == "1"{
                    self.viewMessage.isHidden = false
                }
                
                
                
                
                
                self.tableViewListVehicle.reloadData()
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
    
}
