//
//  ManageVehicleVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 22/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ManageVehicleVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    var arrVehicleName = [ModalHomeSelectVehicle]()
    var selectedIndex:Int = 10000

    var iDriverVehicleId:String = ""
    
    var isFromDelete:Bool = true
    var isFromDeletedOk:Bool = true
    @IBOutlet weak var lblHeader: UILabel!

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnDeleteCancel: UIButton!
    
    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewMessageIn: UIView!
    @IBOutlet weak var tableViewManageVehicle: UITableView!
    
    @IBOutlet weak var viewUpar: UIView!

    
    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.corner_radius()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.post_selectVehicle()
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnAddVehicle(_ sender: Any) {
        let vc:AddVehicleVC = self.storyboard?.instantiateViewController(withIdentifier: "AddVehicleVC") as! AddVehicleVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func btnSelectDocument(_ sender: UIButton) {
        let obj = arrVehicleName[sender.tag]
        let vc:SelectDocumentVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectDocumentVC") as! SelectDocumentVC
        vc.iDriverVehicleId = obj.iDriverVehicleId
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func btnEditVehicle(_ sender: UIButton) {
        let obj = arrVehicleName[sender.tag]
        let vc:AddVehicleVC = self.storyboard?.instantiateViewController(withIdentifier: "AddVehicleVC") as! AddVehicleVC
        vc.isFromEditVehicle = true
        vc.objEditVehicle = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    @IBAction func btnDeleteVehicle(_ sender: UIButton) {
        isFromDelete = true
        let obj = arrVehicleName[sender.tag]
        self.iDriverVehicleId = obj.iDriverVehicleId
        self.lblMessage.text! = "Do you want delete this car?"
        self.viewMessage.isHidden = false
        
    }
    
    
    @IBAction func btnOk(_ sender: UIButton) {
        if isFromDelete == true{
            self.post_deleteVehicle()
        }else{
            self.viewMessage.isHidden = true
        }

        
    }
    @IBAction func btnDeleteCancel(_ sender: Any) {
        self.viewMessage.isHidden = true
    }
    
// MARK: - Table View Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrVehicleName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ManageVehicleCell = tableView.dequeueReusableCell(withIdentifier: "ManageVehicleCell", for: indexPath) as! ManageVehicleCell
        let obj = arrVehicleName[indexPath.row]
        cell.lblVehicleName.text! = "\(obj.vMake) \(obj.vTitle)"
        cell.lblVehicleNumber.text! = obj.vLicencePlate
        cell.lblStatus.text! = obj.eStatus
        
        cell.btnDeleteVehicle.tag = indexPath.row
        
        cell.btnSelectDocument.tag = indexPath.row
//        cell.btnSelectDocument.addTarget(self, action:#selector(btn_selectDcument(sender:)), for: .touchUpInside)
        
        return cell
        
        
//        if selectedIndex == indexPath.row{
//            cell.viewCell.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
//        }else{
//            cell.viewCell.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        self.selectedIndex = indexPath.row
//        self.tableViewManageVehicle.reloadData()
        
    }
    
    

//    @objc func btn_selectDcument(sender: UIButton!) {
//        let vc:SelectDocumentVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectDocumentVC") as! SelectDocumentVC
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
    

// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_MANAGE_VEHICLES"] as? String ?? ""

        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft

            
        }else{
 
        }
        
        
        
    }
    

    
    
    func corner_radius(){
        viewMessage.isHidden = true
        viewMessageIn.layer.cornerRadius = 5
        viewMessageIn.layer.masksToBounds = true
    }
    
}

// MARK: - Webservice calling


extension ManageVehicleVC{
    
    
    func post_selectVehicle(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        
        ValidationManager.sharedManager.showLoader()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?vUserDeviceCountry=&MemberType=Driver&iMemberId=\(userId)&vTim eZone=Asia /Kolkata&type=displaydrivervehicles&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                    
                }
                self.arrVehicleName.removeAll()
                if let message = json["message"].arrayObject{
                    print(message)
                    for index in 0..<message.count{
                        let obj = message[index]
                        let modalObj = ModalHomeSelectVehicle.init(dict: obj as? [String : Any] ?? [:])
                        self.arrVehicleName.append(modalObj)
                    }
                    
                    
                }
                
                self.tableViewManageVehicle.reloadData()
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
    func post_deleteVehicle(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        ValidationManager.sharedManager.showLoader()

        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&vFirebaseDeviceToken=\("")&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia %2FCalcutta&iDriverVehicleId=\(self.iDriverVehicleId)&type=deletedrivervehicle&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&iDriverId=\(userId)&GeneralDeviceType=Android"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                    self.lblMessage.text! = "Vehicle Deleted succesfully"
                    self.btnDeleteCancel.isHidden = true
                    self.isFromDeletedOk = true
                    self.isFromDelete = false
                }
                

                
                
                
                
                self.tableViewManageVehicle.reloadData()
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
}
