//
//  ManageVehicleCell.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 22/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class ManageVehicleCell: UITableViewCell {

    
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var lblVehicleNumber: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var viewDocument: UIView!
    @IBOutlet weak var imgDocument: UIImageView!
    
    @IBOutlet weak var viewEditVehicle: UIView!
    @IBOutlet weak var imgEditVehicle: UIImageView!
    
    @IBOutlet weak var viewDeleteVehicle: UIView!
    @IBOutlet weak var imgDeleteVehicle: UIImageView!
    
    
    @IBOutlet weak var btnSelectDocument: UIButton!
    @IBOutlet weak var btnEditVehicle: UIButton!
    @IBOutlet weak var btnDeleteVehicle: UIButton!
    
    @IBOutlet weak var viewCell: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.corner_radius()
        self.setLanguageLabel()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    

    
    func corner_radius(){
        
        self.viewCell.layer.cornerRadius = 5
        self.viewCell.layer.masksToBounds = true
    }
    
    // MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblStatus.text! = objLang?["LBL_Status"] as? String ?? ""
        
        
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewCell.semanticContentAttribute = .forceRightToLeft
            self.lblStatus.textAlignment = .right
            self.lblVehicleName.textAlignment = .right
            self.lblVehicleNumber.textAlignment = .right

            
        }
        
    }
    
}
