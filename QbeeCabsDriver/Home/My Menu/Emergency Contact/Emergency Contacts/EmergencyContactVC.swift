//
//  EmergencyContactVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 14/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Contacts
import ContactsUI

class EmergencyContactVC: UIViewController, UITableViewDelegate, UITableViewDataSource, SendDataBackDelegate {

    var arrEmergencyContactList = [ModalEmergencyContactList]()

    var strName:String = ""
    var strMobile:String = ""
    
    var strEmergencyId:String = ""
    
    // MARK: - Web service variable

    let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    //MARK: - Outlets
    
    @IBOutlet weak var tableViewContacts: UITableView!
    @IBOutlet weak var heightAddContacts: NSLayoutConstraint!
    @IBOutlet weak var btnAddContacts: UIButton!
    
    @IBOutlet weak var viewNoContacts: UIView!
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var viewUpar: UIView!
    
    @IBOutlet weak var lblMakeYourTravel: UILabel!
    @IBOutlet weak var lblSendAlertToYour: UILabel!
    @IBOutlet weak var lblPleaseAddThem: UILabel!
    @IBOutlet weak var lblYouCanAdd: UILabel!
    
    
    
    

// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.get_emergencyContactList()
        self.viewNoContacts.isHidden = true
        self.viewMain.isHidden = true

        if arrEmergencyContactList.count >= 5{
            heightAddContacts.constant = 0
            btnAddContacts.isHidden = true
        }
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

// MARK: - Button Actions

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddContact(_ sender: Any) {
        let vc:AddContactsVC = self.storyboard?.instantiateViewController(withIdentifier: "AddContactsVC") as! AddContactsVC
        vc.delegateContact = self
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func btnDeleteContact(_ sender: UIButton) {
        let obj = arrEmergencyContactList[sender.tag]
        self.strEmergencyId = obj.strEmergencyId
        self.post_deleteContact()
    }
    
// MARK: - Table View Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEmergencyContactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:EmergencyCell = tableView.dequeueReusableCell(withIdentifier: "EmergencyCell", for: indexPath) as! EmergencyCell
        
        let obj = arrEmergencyContactList[indexPath.row]
        cell.lblName.text! = obj.strName
        cell.lblNumber.text! = obj.strPhone
        cell.btnDeleteContact.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = arrEmergencyContactList[indexPath.row]
        let number = obj.strPhone
        
        let strNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        
        //  UIApplication.shared.open(NSURL(string: "tel://1234567891")! as URL)
        UIApplication.shared.open(NSURL(string: "tel://\(strNumber)")! as URL)
        
    }
    
    func sendDataContacts(addContacts: String, addNumber: String) {
        
        self.strName = addContacts
        self.strMobile = addNumber
        self.post_addContacts()
    }
    
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_EMERGENCY_CONTACT_MENU_SCREEN"] as? String ?? ""
        lblMakeYourTravel.text! = objLang?["LBL_EMERGENCY_CONTACT_TITLE"] as? String ?? ""
        lblSendAlertToYour.text! = objLang?["LBL_EMERGENCY_CONTACT_SUB_TITLE1"] as? String ?? ""
        lblPleaseAddThem.text! = objLang?["LBL_EMERGENCY_CONTACT_SUB_TITLE2"] as? String ?? ""
        lblYouCanAdd.text! = objLang?["LBL_ADD_EMERGENCY_UP_TO_COUNT_EMERGENCY_CONTACTS"] as? String ?? ""
        btnAddContacts.setTitle(objLang?["LBL_ADD_CONTACTS_EMERGENCY_CONTACTS"] as? String ?? "", for: .normal)

        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft

            
        }
        
    }
    

    
}

// MARK: - Webservice calling

extension EmergencyContactVC{
    
    func get_emergencyContactList(){
        
        ValidationManager.sharedManager.showLoader()
        
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?iUserId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=loadEmergencyContacts&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kLoadEmergencyContacts),
                          URLQueryItem(name: "iUserId", value: self.userId),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone)]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                print("action is \(action)")
                
                if action == "1"{
                    self.viewMain.isHidden = false
                    if let message = json["message"].arrayObject{
                        print(message)
                        self.arrEmergencyContactList.removeAll()
                        for index in 0..<message.count{
                            let obj = message[index]
                            let modalObj = ModalEmergencyContactList.init(dict: obj as? [String : Any] ?? [:])
                            self.arrEmergencyContactList.append(modalObj)
                            
                        }
                        
                        if self.arrEmergencyContactList.count >= 5{
                            self.heightAddContacts.constant = 0
                            self.btnAddContacts.isHidden = true
                        }else{
                            self.heightAddContacts.constant = 110
                            self.btnAddContacts.isHidden = false
                        }
                        self.tableViewContacts.reloadData()
                        ValidationManager.sharedManager.hideLoader()

                    }
                }else{
                    self.viewNoContacts.isHidden = false
                    self.viewMain.isHidden = true
                }
                

                ValidationManager.sharedManager.hideLoader()


                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
    func post_addContacts(){

        ValidationManager.sharedManager.showLoader()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&vName=\(self.strName)&GeneralAppVersion=1.0.4&type=addEmergencyContacts&GeneralUserTy pe=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android&GeneralMemberId=\(userId)&iUserId=\(userId)&vUserDeviceCountry=\(country)&Phone=\(self.strMobile)&vTimeZone=Asia/Kolkata&UserType=Driver"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kAddEmergencyContacts),
                          URLQueryItem(name: "iUserId", value: self.userId),
                          URLQueryItem(name: "GeneralMemberId", value: self.userId),
                          URLQueryItem(name: "vFirebaseDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone),
                          URLQueryItem(name: "vName", value: self.strName),
                          URLQueryItem(name: "Phone", value: self.strMobile)]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                let message = json["message"].stringValue
                
                if action == "1"{
                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    let textAlert = objLang?["LBL_EME_CONTACT_LIST_UPDATE"] as? String ?? ""
                    let textOk = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
                    ValidationManager.sharedManager.showAlert(message: textAlert, title: "", btnTitle: textOk, controller: self)
                    self.get_emergencyContactList()
                }
                ValidationManager.sharedManager.hideLoader()
                if self.arrEmergencyContactList.count >= 5{
                    self.heightAddContacts.constant = 0
                    self.btnAddContacts.isHidden = true

                }else{
                    self.heightAddContacts.constant = 110
                    self.btnAddContacts.isHidden = false
                }

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
    func post_deleteContact(){

        ValidationManager.sharedManager.showLoader()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&vFirebaseDeviceToken=\(kDeviceToken)&iUserId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry\(country)=&iEmergencyId=\(self.strEmergencyId)&vTimeZone=Asia%2FCalcutta&type=deleteEmergencyContacts&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kDeleteEmergencyContacts),
                          URLQueryItem(name: "iUserId", value: self.userId),
                          URLQueryItem(name: "GeneralMemberId", value: self.userId),
                          URLQueryItem(name: "vFirebaseDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone),
                          URLQueryItem(name: "iEmergencyId", value: self.strEmergencyId)]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                    
                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    let textAlert = objLang?["LBL_EME_CONTACT_LIST_UPDATE"] as? String ?? ""
                    let textOk = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
                    ValidationManager.sharedManager.showAlert(message: textAlert, title: "", btnTitle: textOk, controller: self)
                    
                    self.get_emergencyContactList()
                }
                if self.arrEmergencyContactList.count >= 5{
                    self.heightAddContacts.constant = 0
                    self.btnAddContacts.isHidden = true

                }else{
                    self.heightAddContacts.constant = 110
                    self.btnAddContacts.isHidden = false
                }
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
}
