//
//  EmergencyCell.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 14/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class EmergencyCell: UITableViewCell {

    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var btnDeleteContact: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.corner_radius()
        self.setLanguageLabel()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

    @IBAction func btnClose(_ sender: Any) {
    }
    
    func corner_radius(){
        viewCell.layer.borderWidth = 1
        viewCell.layer.borderColor = UIColor.lightGray.cgColor
        viewCell.layer.cornerRadius = 5
    }
    
    
// MARK: - set Language Labels

    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
//        lblHeader.text! = objLang?["LBL_Transaction_HISTORY"] as? String ?? ""
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewCell.semanticContentAttribute = .forceRightToLeft
            self.lblName.textAlignment = .right
            self.lblNumber.textAlignment = .right


            
        }else{

        }
        
        
        
        
        
    }
}
