//
//  AddContactsCell.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 22/05/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class AddContactsCell: UITableViewCell {

    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
