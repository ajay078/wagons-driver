//
//  AddContactsVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 22/05/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Contacts

class AddContactsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    var arrContacts:[[String:Any]] = [[:]]
    var delegateContact:SendDataBackDelegate!

    
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    
    @IBOutlet weak var tableViewContacts: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contactsList()
       

    }
    

    @IBAction func btnBack(_ sender: Any) {
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:AddContactsCell = tableView.dequeueReusableCell(withIdentifier: "AddContactsCell", for: indexPath) as! AddContactsCell
        
        let obj = arrContacts[indexPath.row]
        
        let firstName = obj["firstName"] as? String ?? ""
        let lastName = obj["lastName"] as? String ?? ""
        let number = obj["number"] as? String ?? ""
        
        cell.lblName.text! = firstName + " " + lastName
        cell.lblNumber.text! = number
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = arrContacts[indexPath.row]
        
        let firstName = obj["firstName"] as? String ?? ""
        let lastName = obj["lastName"] as? String ?? ""
        
        let name = firstName + "" + lastName
        let number = obj["number"] as? String ?? ""
        
        delegateContact.sendDataContacts?(addContacts: name, addNumber: number)
        dismiss(animated: true, completion: nil)
        
        
    }
    
    func contactsList(){
        self.arrContacts.removeAll()

        let contactStore = CNContactStore()
        var contacts = [CNContact]()
        let keys = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey
            ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                contacts.append(contact)
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
                        print("\(contact.givenName) \(contact.familyName) tel:\(localizedLabel) -- \(number.stringValue), email: \(contact.emailAddresses)")
                        let dict = ["firstName":contact.givenName, "lastName":contact.familyName, "number":number.stringValue]
                        
                        
                        self.arrContacts.append(dict)
                        self.tableViewContacts.reloadData()
                    }
                }
            }
            print(contacts)
        } catch {
            print("unable to fetch contacts")
        }
    }
    
    
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
 //       lblHeader.text! = objLang?["LBL_EMERGENCY_CONTACT_MENU_SCREEN"] as? String ?? ""


        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft

            
        }
        
    }
}
