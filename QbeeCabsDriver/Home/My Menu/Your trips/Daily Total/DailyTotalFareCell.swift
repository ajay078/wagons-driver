//
//  DailyTotalFareCell.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 04/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class DailyTotalFareCell: UITableViewCell {

    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblRide: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    
    @IBOutlet weak var viewCell: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setLanguageLabel()
        viewCell.layer.cornerRadius = 5
        viewCell.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

    // MARK: - set Language Labels

    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        //        lblHeader.text! = objLang?["LBL_SUPPORT_HEADER_TXT"] as? String ?? ""
        
        
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewCell.semanticContentAttribute = .forceRightToLeft
            self.lblRide.textAlignment = .right
            
        }
        
        
        
    }
    
}
