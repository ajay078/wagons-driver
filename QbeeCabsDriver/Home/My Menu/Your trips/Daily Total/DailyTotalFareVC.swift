//
//  DailyTotalFareVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 04/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DailyTotalFareVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    

    var arrDailyTotal = [MTripList]()
    
    var selectedDate:String = ""
    
    var totalEarning = ""
    var avgRating = ""
    var completedTrip = ""
    var tripDate = ""
    
    // MARK: - Web service variable

    let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    //MARK:- Outlets
    
    @IBOutlet weak var lblSelectedDate: UILabel!
    
    @IBOutlet weak var lblTotalFare: UILabel!
    @IBOutlet weak var lblTotalTrips: UILabel!
    
    
    @IBOutlet weak var imgRating1: UIImageView!
    @IBOutlet weak var imgRating2: UIImageView!
    @IBOutlet weak var imgRating3: UIImageView!
    @IBOutlet weak var imgRating4: UIImageView!
    @IBOutlet weak var imgRating5: UIImageView!
    
    @IBOutlet weak var lblTotalFareL: UILabel!
    @IBOutlet weak var lblAvgRatingL: UILabel!
    @IBOutlet weak var lblCompletedTripsL: UILabel!
    @IBOutlet weak var lblTripEarningL: UILabel!
    @IBOutlet weak var viewUpar: UIView!
    
    @IBOutlet weak var tableViewDailyTotalFare: UITableView!
   

// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.post_rideHistory()
        self.setLanguageLabel()

        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
// MARK: - Table View Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDailyTotal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DailyTotalFareCell = tableView.dequeueReusableCell(withIdentifier: "DailyTotalFareCell", for: indexPath) as! DailyTotalFareCell
        
        let obj = arrDailyTotal[indexPath.row]
        
        
        cell.lblTime.text! = obj.TripTime
        cell.lblRide.text! = obj.eType
        cell.lblAmount.text! = "$\(obj.iFare)"
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc:ReceiptVC = self.storyboard?.instantiateViewController(withIdentifier: "ReceiptVC") as! ReceiptVC
        vc.objTripDetails = arrDailyTotal[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblTotalFareL.text! = objLang?["LBL_RIDER_TOTAL_FARE_RATING"] as? String ?? ""
        lblAvgRatingL.text! = objLang?["LBL_AVG_RATING_SELECTED_DAY_RIDE_HISTORY"] as? String ?? ""
        lblCompletedTripsL.text! = objLang?["LBL_COMPLETED_TRIPS"] as? String ?? ""

        lblTripEarningL.text! = objLang?["LBL_TRIP_EARNING_SELECTED_DAY_RIDE_HISTORY"] as? String ?? ""



        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft //SMP: LTR to RTL

            
        }else{

        }
        
        
        
    }
    
    
    func details(){
        
        self.lblSelectedDate.text! = self.tripDate
        self.lblTotalFare.text! = "$\(self.totalEarning)"
        self.lblTotalTrips.text! = self.completedTrip
        
        
        let doubleRating = Double(avgRating) ?? 0.0
        let IntRating:Int = Int(round(doubleRating))
        self.get_rating(rating: IntRating)

        
        
        
        
        
    }
    
    func get_rating(rating:Int){
        
        switch rating {
        case 1:
            imgRating1.image = UIImage(named: "star white")
        case 2:
            imgRating1.image = UIImage(named: "star white")
            imgRating2.image = UIImage(named: "star white")

        case 3:
            imgRating1.image = UIImage(named: "star white")
            imgRating2.image = UIImage(named: "star white")
            imgRating3.image = UIImage(named: "star white")

            
        case 4:
            imgRating1.image = UIImage(named: "star white")
            imgRating2.image = UIImage(named: "star white")
            imgRating3.image = UIImage(named: "star white")
            imgRating4.image = UIImage(named: "star white")

            
        case 5:
            imgRating1.image = UIImage(named: "star white")
            imgRating2.image = UIImage(named: "star white")
            imgRating3.image = UIImage(named: "star white")
            imgRating4.image = UIImage(named: "star white")
            imgRating5.image = UIImage(named: "star white")

            
        default:
            break
        }
        
    }
    
}

// MARK: - Webservice calling

extension DailyTotalFareVC{
    
    func post_rideHistory(){
        
        
        ValidationManager.sharedManager.showLoader()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?date=\(selectedDate)&GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=getDriverRideHistory&GeneralUserType=Driver&tSessionId=\(sessionId)&iDriverId=\(userId)&GeneralDeviceType=ios"
        
        
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?pickUpLatitude=22.688400129728446&iVehicleTypeId=23&pickUpLocAdd=+Indrapuri+Colony+Main+Rd%2C+Indrapuri+Colony%2C+Bhanwar+Kuwa%2C+Indore%2C+Madhya+Pradesh+452001%2C+India&GeneralAppVersion=1.0.4&distance=43351.0&PromoCode=BCVS34&destLongitude=76.0534454&CashPayment=true&eType=Ride&destLatitude=22.9675929&type=ScheduleARide&GeneralUserType=Passenger&tSessionId=b1130b5f8448307f38684f96a5b2db831601034179&GeneralMemberId=\("1")&HandicapPrefEnabled=No&iCabBookingId=&scheduleDate=2020-09-29+16:33:41&eTollSkipped=No&vFirebaseDeviceToken=er2MQim2QYav4l57FdmVcu%3AAPA91bFanUb_TFk6JQF2lrhiXqDrJz3D0oFkGv8f00x5JiNRWp6G3AALYXb92rYIEOJ-EqeeBmpAhAX0fuU8180RHKibOK9TentzsLMFKQz_80ZWJl0tZksprou0-h6Ogm-r9MHDl88f&pickUpLongitude=75.86785241961479&destLocAdd=Dewas%2C+Madhya+Pradesh%2C+India&SelectedDriverId=&fTollPrice=0.0&vTollPriceCurrencyCode=&GeneralDeviceType=Android&iUserId=\("1")&PreferFemaleDriverEnable=No&vUserDeviceCountry=in&vTimeZone=Asia%2FKolkata&time=3735.0"

        
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "" )
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kGetDriverRideHistory),
                          URLQueryItem(name: "GeneralMemberId", value: self.userId),
                          URLQueryItem(name: "iDriverId", value: self.userId),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone),
                          URLQueryItem(name: "date", value: self.selectedDate)]

        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                }
                
                
                if let message = json["message"].arrayObject{
                    for index in 0..<message.count{
                        let obj = message[index]
                        let modalObj = MTripList.init(dict: obj as? [String:Any] ?? [:])
                        self.arrDailyTotal.append(modalObj)
                        
                    }
                }
                self.tableViewDailyTotalFare.reloadData()
                
                self.totalEarning = json["TotalEarning"].stringValue
                self.avgRating = json["AvgRating"].stringValue
                self.tripDate = json["TripDate"].stringValue
                self.completedTrip = json["TripCount"].stringValue
                
                self.details()

                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
}
