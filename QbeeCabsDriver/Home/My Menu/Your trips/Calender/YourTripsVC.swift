//
//  YourTripsVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 03/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import FSCalendar
class YourTripsVC: UIViewController, FSCalendarDelegate, FSCalendarDataSource {

    
    var selectedDate = ""
    
    var passengerId = ""
    var pName = ""
    var pRating = ""
    var pSourceLatitude = ""
    var pSourceLongitude = ""
    var pDestLatitude = ""
    var pDestLongitude = ""
    var pITripId = ""
    var pICabRequestId:Int = 0
    var pMsgCode = ""
    var pTripId = ""
    
    var pPicName = ""
    var pPassengerId = ""
    var pDestLocAddress = ""
    
    var pCurrentAddress = ""
    var strTripRating:String = ""

    var iCabBookingId = ""
    
    @IBOutlet weak var btnPast: UIButton!
    @IBOutlet weak var btnUpComing: UIButton!
    
    @IBOutlet weak var viewBtnPast: UIView!
    @IBOutlet weak var viewBtnUpComing: UIView!
    

    @IBOutlet weak var viewMainPast: UIView!
    @IBOutlet weak var viewMainUpcoming: UIView!
    
    
    
    @IBOutlet weak var lblBookingNo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPickUp: UILabel!
    @IBOutlet weak var lblDestination: UILabel!
    
    
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet var lblNoDataAvailable: UILabel!
    @IBOutlet weak var viewLoaction: UIView!
    @IBOutlet weak var viewNoData: UIView!

    @IBOutlet weak var calendar: FSCalendar!
   
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.corner_radius()
        calendar.dataSource = self
        calendar.delegate = self
    }
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("date is selected")
        print(date)
        
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        

        
        let myString = formatter.string(from: date) // string purpose I add here
        
        formatter.timeZone = NSTimeZone(abbreviation: "GMT") as TimeZone?
        
        
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
//        formatter.dateFormat = "dd-MMM-yyyy"
        formatter.dateFormat = "yyyy/MM/dd"

        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        self.selectedDate = myStringafd
        
        let vc:DailyTotalFareVC = self.storyboard?.instantiateViewController(withIdentifier: "DailyTotalFareVC") as! DailyTotalFareVC
        vc.selectedDate = self.selectedDate
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }

// MARK: - Button Actions

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func btnPast(_ sender: Any) {
        btnPast.setTitleColor(.black, for: .normal)
        btnUpComing.setTitleColor(.darkGray, for: .normal)
        
        viewMainPast.isHidden = false
        viewMainUpcoming.isHidden = true
        viewBtnPast.isHidden = false
        viewBtnUpComing.isHidden = true
        viewNoData.isHidden = true
        
    }
    @IBAction func btnUpComing(_ sender: Any) {
        viewNoData.isHidden = false
        btnPast.setTitleColor(.darkGray, for: .normal)
        btnUpComing.setTitleColor(.black, for: .normal)
        viewMainPast.isHidden = true
        viewMainUpcoming.isHidden = false
        viewBtnPast.isHidden = true
        viewBtnUpComing.isHidden = false
        self.post_UpcomingTrip()

        
    }
    
    
    @IBAction func btnBeginTrip(_ sender: Any) {
        self.post_acceptRequest()
    }
    @IBAction func btnCancelTrip(_ sender: Any) {
        self.post_declineRequest()
    }
    
    
    
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_YOUR_TRIPS"] as? String ?? ""
        btnPast.setTitle(objLang?["LBL_PAST"] as? String ?? "", for: .normal)
        btnUpComing.setTitle(objLang?["LBL_UPCOMING"] as? String ?? "", for: .normal)
        self.lblNoDataAvailable.text! = objLang?["LBL_NO_DATA_AVAIL"] as? String ?? ""


        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft

            
        }else{

        }
        
        
        
    }
    
    
    
    
    
    
    func corner_radius(){
        viewNoData.isHidden = true
        viewMainUpcoming.isHidden = true
        viewBtnPast.isHidden = false
        viewBtnUpComing.isHidden = true
        viewLoaction.layer.borderWidth = 1
        viewLoaction.layer.borderColor = UIColor.lightGray.cgColor
        viewLoaction.layer.cornerRadius = 5
        viewLoaction.layer.masksToBounds = true
    }
    
    

    

    
    

    
    
}

// MARK: - Webservice calling

extension YourTripsVC{
    
    func post_UpcomingTrip(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        
        ValidationManager.sharedManager.showLoader()

        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&iDriverId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&bookingType=checkBookings&vTimeZone=Asia/Kolkata&type=checkBookings&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                
                let action = json["Action"].stringValue
                
                if action == "1"{
                    self.viewNoData.isHidden = true
                    let message = json["message"].arrayValue
                    
                    for index in 0..<message.count{
                        let obj = message[index]
                        
                        self.lblBookingNo.text! = "Booking# \(obj["iCabBookingId"].stringValue)"
                        self.iCabBookingId = obj["iCabBookingId"].stringValue
                        self.lblDate.text! = obj["dBooking_date"].stringValue
                        self.lblPickUp.text! = obj["vSourceAddresss"].stringValue
                        self.lblDestination.text! = obj["tDestAddress"].stringValue
                        self.passengerId = obj["iUserId"].stringValue
                        self.pSourceLatitude = obj["vSourceLatitude"].stringValue
                        self.pSourceLongitude = obj["vSourceLongitude"].stringValue
                        self.pDestLatitude = obj["vDestLatitude"].stringValue
                        self.pDestLongitude = obj["vDestLongitude"].stringValue
                        self.pCurrentAddress = obj["vSourceAddresss"].stringValue
                    }
                    
                }else{
                    self.viewNoData.isHidden = false
                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    self.lblNoDataAvailable.text! = objLang?["LBL_NO_DATA_AVAIL"] as? String ?? ""
                }
                
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
    func post_acceptRequest(){
        
        if CheckInternet.connection(){
            
            ValidationManager.sharedManager.showLoader()
            let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
            let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
            let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
            
            //            let urlString = "https://apps.wagonstaxi.com/webservice.php?iCabRequestId=\("")&vFirebaseDeviceToken=\(kDeviceToken)&PassengerID=\(self.passengerId)&GeneralAppVersion=1.0.4&vMsgCode=\(self.pMsgCode)&type=GenerateTrip&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android &GeneralMemberId=\(userId)&DriverID=\(userId)&GoogleServerKey=\(kGoogleApiKey)&start_lon=\(self.pSourceLongitude)&iCabBookingId=\(iCabBookingId)&vUserDeviceCountry=\(country)&sAddress=\(self.pCurrentAddress)&start_lat=\(self.pSourceLatitude)&vTimeZone=Asia/Kolkata&UserType=Driver"
            
            
            let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&type=GenerateTrip&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android&GeneralMemberId=\(userId)&DriverID=\(userId)&GoogleServerKey=\(kGoogleApiKey)&iCabBookingId=\(iCabBookingId)&vUserDeviceCountry=in&vTimeZone=Asia%2FKolkata&UserType=Driver"
            
            
            let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            let searchURL = URL(string: newURL ?? "")!
            
            let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                       "Content-Type": "application/json"]
            
            AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
                
                
                switch responseObject.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    
                    _ = json["message"].dictionaryObject
                    self.pName = json["PName"].stringValue
                    self.pRating = json["PRating"].stringValue
                    self.pPicName = json["PPicName"].stringValue
                    self.pPassengerId = json["PassengerId"].stringValue
                    self.pDestLocAddress = json["DestLocAddress"].stringValue
                    self.pTripId = json["TripId"].stringValue
                    self.pSourceLatitude = json["sourceLatitude"].stringValue
                    self.pSourceLongitude = json["sourceLongitude"].stringValue
                    
                    let action = json["Action"].stringValue
                    if action == "1"{
                        let vc:EnRouteVC = self.storyboard?.instantiateViewController(withIdentifier: "EnRouteVC") as! EnRouteVC
                        //                        vc.pPassengerPickUp = self.pCurrentAddress
                        //                        vc.pName = self.pName
                        //                        vc.pRating = self.pRating
                        //                        vc.pPicName = self.pPicName
                        //                        vc.pPassengerId = self.pPassengerId
                        //                        vc.pDestLocAddress = self.pDestLocAddress
                        //                        vc.pTripId = self.pTripId
                        //                        vc.pSourceLatitude = self.pSourceLatitude
                        //                        vc.pSourceLongitude = self.pSourceLongitude
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    
                    ValidationManager.sharedManager.hideLoader()

                case .failure(let error):
                    print(error)
                    ValidationManager.sharedManager.hideLoader()

                }
            }
            
        }else{
            alert_show(message: "Please check your internet connection")
            
        }
        
    }
    
    
    func post_declineRequest(){
        
        ValidationManager.sharedManager.showLoader()
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&vFirebaseDeviceToken=\(kDeviceToken)&DriverID=\(userId)&PassengerID=\(self.passengerId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&vMsgCode=\(pMsgCode)&type=DeclineTripRequest&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                //                let json = JSON(value)
                //                print(json)
                
                let response = value as? Int ?? 0
                print(response)
                
                if response == 1{
                    let vc:HomePageVC = self.storyboard?.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
                    self.navigationController?.pushViewController(vc, animated: false)
                }else{
                    
                }
                
                ValidationManager.sharedManager.hideLoader()

            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()

            }
        }
        
    }
    
}
