//
//  ReceiptVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 05/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class ReceiptVC: UIViewController {

    var objTripDetails = MTripList(dict: [:])
    
    var strTripRating:String = ""
    var strTripStatus:String = ""
    
    
    // MARK: - Outlets

    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgProfileBack: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var imgRating1: UIImageView!
    @IBOutlet weak var imgRating2: UIImageView!
    @IBOutlet weak var imgRating3: UIImageView!
    @IBOutlet weak var imgRating4: UIImageView!
    @IBOutlet weak var imgRating5: UIImageView!
    
    @IBOutlet weak var lblBookinNo: UILabel!
    @IBOutlet weak var lblPickUp: UILabel!
    @IBOutlet weak var lblDestination: UILabel!
    
    @IBOutlet weak var lblBaseFare: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMinimum: UILabel!
    @IBOutlet weak var lblCommission: UILabel!
    @IBOutlet weak var lblEarned: UILabel!
    
    @IBOutlet weak var lblAmountBaseFare: UILabel!
    @IBOutlet weak var lblAmountDistance: UILabel!
    @IBOutlet weak var lblAmountTime: UILabel!
    @IBOutlet weak var lblAmountMinimum: UILabel!
    @IBOutlet weak var lblAmountCommission: UILabel!
    @IBOutlet weak var lblAmountEarned: UILabel!
    
    @IBOutlet weak var imgPayment: UIImageView!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblTripStatus: UILabel!
    
    @IBOutlet weak var lblTripDate: UILabel!
    
    @IBOutlet weak var viewPaymentDetails: UIView!
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet var viewProfileL: UIView!
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var lblPassengerL: UILabel!
    @IBOutlet var lblRatingL: UILabel!
    @IBOutlet var lblThankForRidingL: UILabel!
    @IBOutlet var lblTripRequestDateL: UILabel!
    @IBOutlet var lblPickUpLocationL: UILabel!
    @IBOutlet var lblDestinationLocationL: UILabel!
    @IBOutlet var lblChargesL: UILabel!
    
    @IBOutlet var viewBaseFareL: UIView!
    @IBOutlet var viewDistanceL: UIView!
    @IBOutlet var viewTimeL: UIView!
    @IBOutlet var viewMinimumL: UIView!
    @IBOutlet var viewCommissionL: UIView!
    @IBOutlet var viewEarnedAmountL: UIView!
 
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.corner_radius()
        self.details()
//        self.get_fareDetails()
        self.get_fareNew()
        self.setLanguageLabel()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
// MARK: - Button Actions

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func corner_radius(){
        self.viewPaymentDetails.layer.cornerRadius = 10
        self.viewPaymentDetails.layer.masksToBounds = true
        
        imgProfile.layer.cornerRadius = imgProfile.frame.size.height/2
        
        
        viewBaseFareL.isHidden = true
        viewDistanceL.isHidden = true
        viewTimeL.isHidden = true
        viewMinimumL.isHidden = true
        viewCommissionL.isHidden = true
        viewEarnedAmountL.isHidden = true
        

    }
    
    
    
    
    func details(){
        
        let obj = objTripDetails
        
        self.lblName.text! = "\(obj.vName) \(obj.vLastName)"
        self.lblTripDate.text! = obj.tTripRequestDate
        self.lblPickUp.text! = obj.tSaddress
        self.lblDestination.text! = obj.tDaddress
        self.lblBookinNo.text! = "Booking# \(obj.vRideNo)"
        self.lblPaymentMode.text! = obj.vTripPaymentMode
        
        self.strTripStatus = obj.iActive
        
        if strTripStatus == "Finished"{
            self.lblTripStatus.text! = "This trip was successfully finished"
        }
        
        
        
        self.strTripRating = obj.vRating1
        
        let doubleRating = Double(strTripRating) ?? 0.0
        let IntRating:Int = Int(round(doubleRating))
        self.get_rating(rating: IntRating)
        
        
        let imageUrl = obj.baseImageUrl+"\(obj.PassengerUserId)/"+"\(obj.vImageId)"
        print(imageUrl)
        
        DispatchQueue.global(qos: .background).async {
            do
            {
                let url = URL(string: imageUrl)
                if let data = try? Data(contentsOf: url!){
                    DispatchQueue.main.async {
                        self.imgProfile.image = UIImage(data: data)
                        self.imgProfileBack.image = UIImage(data: data)
                        
                    }
                }
                
            }

        }
        
        
        
    }
    
    func get_rating(rating:Int){
        
        switch rating {
        case 1:
            imgRating1.image = UIImage(named: "star yellow")
        case 2:
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            
        case 3:
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            imgRating3.image = UIImage(named: "star yellow")
            
            
        case 4:
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            imgRating3.image = UIImage(named: "star yellow")
            imgRating4.image = UIImage(named: "star yellow")
            
            
        case 5:
            imgRating1.image = UIImage(named: "star yellow")
            imgRating2.image = UIImage(named: "star yellow")
            imgRating3.image = UIImage(named: "star yellow")
            imgRating4.image = UIImage(named: "star yellow")
            imgRating5.image = UIImage(named: "star yellow")
            
            
        default:
            break
        }
        
    }
    
    func get_fareNew(){
        
        let newdetalisArr = objTripDetails.dictFareDetails
        print(newdetalisArr)
        
        
        for (key, value) in objTripDetails.dictFareDetails {

            let obj = objTripDetails.dictFareDetails

            if key.contains("Base"){
                viewBaseFareL.isHidden = false
                lblBaseFare.text = key
                lblAmountBaseFare.text = obj[key] as? String ?? ""
            }
            if key.contains("Distance"){
                viewDistanceL.isHidden = false
                lblDistance.text = key
                lblAmountDistance.text = obj[key] as? String ?? ""

            }
            if key.contains("Time"){
                viewTimeL.isHidden = false
                lblTime.text = key
                lblAmountTime.text = obj[key] as? String ?? ""

            }
            if key.contains("Minimum"){
                viewMinimumL.isHidden = false
                lblMinimum.text = key
                lblAmountMinimum.text = obj[key] as? String ?? ""

            }
            if key.contains("Commission"){
                viewCommissionL.isHidden = false
                lblCommission.text = key
                lblAmountCommission.text = obj[key] as? String ?? ""
            }
            if key.contains("Earned"){
                viewEarnedAmountL.isHidden = false
                lblEarned.text = key
                lblAmountEarned.text = obj[key] as? String ?? ""
            }

            print(key)
            print(value)
            print(index)


        }
        
        
    }
    
    
    func get_fareDetails(){
        
        var indexOfDict:Int = -1
        
        for (key, value) in objTripDetails.dictFareDetails {
            indexOfDict = indexOfDict + 1
            switch indexOfDict {
            case 0:
                lblBaseFare.text = key
                lblAmountBaseFare.text = value as? String ?? ""
            case 1:
                lblDistance.text = key
                lblAmountDistance.text = value as? String ?? ""
            case 2:
                lblTime.text = key
                lblAmountTime.text! = value as? String ?? ""
            case 3:
                lblMinimum.text = key
                lblAmountMinimum.text = value as? String ?? ""
            case 4:
                lblCommission.text = key
                lblAmountCommission.text = value as? String ?? ""
            case 5:
                lblEarned.text = key
                lblAmountEarned.text = value as? String ?? ""
                
            default:
                break
            }
            
            //            if let index = Int(key) {
            //            let index = Index()
            print(key)
            print(value)
            print(index)
            
            
            
            
            
            //            }
        }
    }
    
    // MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_RECEIPT_HEADER_TXT"] as? String ?? ""
        lblPassengerL.text! = objLang?["LBL_PASSENGER_TXT"] as? String ?? ""
        lblRatingL.text! = objLang?["LBL_RATING"] as? String ?? ""
        lblThankForRidingL.text! = objLang?["LBL_RIDER_THANKS_RIDING_RIDE_DETAIL"] as? String ?? ""
        lblTripRequestDateL.text! = objLang?["LBL_TRIP_REQUEST_DATE_RIDE_DETAIL"] as? String ?? ""
        lblPickUpLocationL.text! = objLang?["LBL_PICK_UP_LOCATION_BOOKING_FRAG"] as? String ?? ""
        lblDestinationLocationL.text! = objLang?["LBL_DEST_LOCATION_BOOKING_FRAG"] as? String ?? ""
        lblChargesL.text! = objLang?["LBL_CHARGES_TXT"] as? String ?? ""
        
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft
            self.viewProfileL.semanticContentAttribute = .forceRightToLeft
            lblTripRequestDateL.textAlignment = .right
            lblTripDate.textAlignment = .right
            lblPickUpLocationL.textAlignment = .right
            lblPickUp.textAlignment = .right
            lblDestinationLocationL.textAlignment = .right
            lblDestination.textAlignment = .right
            lblChargesL.textAlignment = .right

            
        }
         

     }
    
    
}
