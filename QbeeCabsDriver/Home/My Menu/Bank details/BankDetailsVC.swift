//
//  BankDetailsVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 03/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BankDetailsVC: UIViewController, UITextFieldDelegate {

    
    var dictBankDetails = MBankDetails(dict: [:])
    
    
    // MARK: - Web service variable

    let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    //MARK:- Outlets
    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewMessageIn: UIView!

    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtBankAccount: UITextField!
    @IBOutlet weak var txtAccountNumber: UITextField!
    @IBOutlet weak var txtBankLocation: UITextField!
    @IBOutlet weak var txtBankName: UITextField!
    @IBOutlet weak var txtBicCode: UITextField!
    
    
    @IBOutlet weak var heightViewEmailUp: NSLayoutConstraint!
    @IBOutlet weak var heightViewAccountUp: NSLayoutConstraint!
    @IBOutlet weak var heightViewNumberUp: NSLayoutConstraint!
    @IBOutlet weak var heightViewLocationUp: NSLayoutConstraint!
    @IBOutlet weak var heightViewBankNameUp: NSLayoutConstraint!
    @IBOutlet weak var heightViewBicUp: NSLayoutConstraint!
    

    
    @IBOutlet weak var viewREmailIn: UIView!
    @IBOutlet weak var viewRAccountIn: UIView!
    @IBOutlet weak var viewRNumberIn: UIView!
    @IBOutlet weak var viewRLocationIn: UIView!
    @IBOutlet weak var viewRBankNameIn: UIView!
    @IBOutlet weak var viewRBicIn: UIView!
    
    
    
    @IBOutlet weak var viewREmailDown: UIView!
    @IBOutlet weak var viewRAccountDown: UIView!
    @IBOutlet weak var viewRNumberDown: UIView!
    @IBOutlet weak var viewRLocationDown: UIView!
    @IBOutlet weak var viewRBankNameDown: UIView!
    @IBOutlet weak var viewRBicDown: UIView!
    
    
    @IBOutlet weak var heightViewREmailDown: NSLayoutConstraint!
    @IBOutlet weak var heightViewRAccountDown: NSLayoutConstraint!
    @IBOutlet weak var heightViewRNumberDown: NSLayoutConstraint!
    @IBOutlet weak var heightViewRLocationDown: NSLayoutConstraint!
    @IBOutlet weak var heightViewRBankNameDown: NSLayoutConstraint!
    @IBOutlet weak var heightViewRBicDown: NSLayoutConstraint!
    
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var lblPaymentEmailL: UILabel!
    @IBOutlet weak var lblAccountHolder: UILabel!
    @IBOutlet weak var lblAccountNumber: UILabel!
    @IBOutlet weak var lblBankLocation: UILabel!
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblBicL: UILabel!
    
    @IBOutlet var lblREmailL: UILabel!
    @IBOutlet var lblRAccountL: UILabel!
    @IBOutlet var lblRAccountNumberL: UILabel!
    @IBOutlet var lblRBankLocationL: UILabel!
    @IBOutlet var lblRBankNameL: UILabel!
    @IBOutlet var lblRBicL: UILabel!
    
    
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var btnMessageOk: UIButton!
    @IBOutlet var viewButtonL: UIView!
    
    
    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.alert_height()
        self.post_bankDetails()

        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }
    
// MARK: - Button Actions

    @IBAction func btnBack(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        self.view.endEditing(true)
        self.alert_emptyTextField()
    }
    
    @IBAction func btnMessageOk(_ sender: Any) {
        viewMessage.isHidden = true
        self.post_bankDetails()

    }
    
// MARK: - Text Field Methods

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            
        if textField == txtEmail{
            txtBankAccount.becomeFirstResponder()
        }else if textField == txtBankAccount{
            txtAccountNumber.becomeFirstResponder()
        }else if textField == txtAccountNumber{
            txtBankLocation.becomeFirstResponder()
        }else if textField == txtBankLocation{
            txtBankName.becomeFirstResponder()
        }else if textField == txtBankName{
            txtBicCode.becomeFirstResponder()
        }else if textField == txtBicCode{
            textField.resignFirstResponder()
        }


        return true

    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtEmail{
            viewREmailIn.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)

            self.heightViewREmailDown.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }

        }else if textField == txtBankAccount{
            viewRAccountIn.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)

            self.heightViewRAccountDown.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtAccountNumber{
            viewRNumberIn.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            
            self.heightViewRNumberDown.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }

        }else if textField == txtBankLocation{
            viewRLocationIn.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            
            self.heightViewRLocationDown.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }

        }else if textField == txtBankName{
            viewRBankNameIn.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            
            self.heightViewRBankNameDown.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }

        }else if textField == txtBicCode{
            viewRBicIn.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            
            self.heightViewRBicDown.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
        
        
        return true
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtEmail{
            
            let currentText = txtEmail.text ?? ""
            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
            
            if replacementText.count > 0{
                heightViewEmailUp.constant = 20
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                
            }else{
                
                heightViewEmailUp.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }

                
            }
            
            
        }else if textField == txtBankAccount{
            
            let currentText = txtBankAccount.text ?? ""
            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
            
            if replacementText.count > 0{
                heightViewAccountUp.constant = 20
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                
            }else{
                
                heightViewAccountUp.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }

                
            }
            
        }else if textField == txtAccountNumber{
            
            let currentText = txtAccountNumber.text ?? ""
            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
            
            if replacementText.count > 0{
                heightViewNumberUp.constant = 20
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                
            }else{
                
                heightViewNumberUp.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }

                
            }
            
        }else if textField == txtBankLocation{
            
            let currentText = txtBankLocation.text ?? ""
            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
            
            if replacementText.count > 0{
                heightViewLocationUp.constant = 20
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                
            }else{
                
                heightViewLocationUp.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }

                
            }
            
        }else if textField == txtBankName{
            
            let currentText = txtBankName.text ?? ""
            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
            
            if replacementText.count > 0{
                heightViewBankNameUp.constant = 20
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                
            }else{
                
                heightViewBankNameUp.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }

                
            }
            
        }else if textField == txtBicCode{
            
            let currentText = txtBicCode.text ?? ""
            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
            
            if replacementText.count > 0{
                heightViewBicUp.constant = 20
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                
            }else{
                
                heightViewBicUp.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }

                
            }
            
        }
        
        
        return true
        
        
    }
    
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_BANK_DETAILS_TXT"] as? String ?? ""

        lblPaymentEmailL.text! = objLang?["LBL_PAYMENT_EMAIL_TXT"] as? String ?? ""
        lblAccountHolder.text! = objLang?["LBL_PROFILE_BANK_HOLDER_TXT"] as? String ?? ""
        lblAccountNumber.text! = objLang?["LBL_ACCOUNT_NUMBER"] as? String ?? ""
        lblBankLocation.text! = objLang?["LBL_BANK_LOCATION"] as? String ?? ""
        lblBankName.text! = objLang?["LBL_BANK_NAME"] as? String ?? ""
        lblBicL.text! = objLang?["LBL_BIC_SWIFT_CODE"] as? String ?? ""

        txtEmail.placeholder = objLang?["LBL_PAYMENT_EMAIL_TXT"] as? String ?? ""
        txtBankAccount.placeholder = objLang?["LBL_PROFILE_BANK_HOLDER_TXT"] as? String ?? ""
        txtAccountNumber.placeholder = objLang?["LBL_ACCOUNT_NUMBER"] as? String ?? ""
        txtBankLocation.placeholder = objLang?["LBL_BANK_LOCATION"] as? String ?? ""
        txtBankName.placeholder = objLang?["LBL_BANK_NAME"] as? String ?? ""
        txtBicCode.placeholder = objLang?["LBL_BIC_SWIFT_CODE"] as? String ?? ""
        lblMessage.text! = objLang?["LBL_BANK_DETAILS_UPDATED"] as? String ?? ""
        btnMessageOk.setTitle(objLang?["LBL_BTN_OK_TXT"] as? String ?? "", for: .normal)
        btnSubmit.setTitle(objLang?["LBL_SUBMIT_BUTTON_TXT"] as? String ?? "", for: .normal)

        
        lblREmailL.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
        lblRAccountL.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
        lblRAccountNumberL.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
        lblRBankLocationL.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
        lblRBankNameL.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
        lblRBicL.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""

        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft
            self.viewButtonL.semanticContentAttribute = .forceRightToLeft

            self.txtEmail.textAlignment = .right
            self.txtBankAccount.textAlignment = .right
            self.txtAccountNumber.textAlignment = .right
            self.txtBankLocation.textAlignment = .right
            self.txtBankName.textAlignment = .right
            self.txtBicCode.textAlignment = .right
            
            self.lblPaymentEmailL.textAlignment = .right
            self.lblAccountHolder.textAlignment = .right
            self.lblAccountNumber.textAlignment = .right
            self.lblBankLocation.textAlignment = .right
            self.lblBankName.textAlignment = .right
            self.lblBicL.textAlignment = .right
            self.lblMessage.textAlignment = .right
            
            self.lblREmailL.textAlignment = .right
            self.lblRAccountL.textAlignment = .right
            self.lblRAccountNumberL.textAlignment = .right
            self.lblRBankLocationL.textAlignment = .right
            self.lblRBankNameL.textAlignment = .right
            self.lblRBicL.textAlignment = .right


            
        }else{

        }
        
        
        
        
        
    }
    
    func alert_height(){
        
        heightViewREmailDown.constant = 0
        heightViewRAccountDown.constant = 0
        heightViewRNumberDown.constant = 0
        heightViewRLocationDown.constant = 0
        heightViewRBankNameDown.constant = 0
        heightViewRBicDown.constant = 0

        
        
        if !txtEmail.text!.isEmpty{
            heightViewEmailUp.constant = 20
            
        }else if !txtBankAccount.text!.isEmpty{
            heightViewAccountUp.constant = 20
        }else if !txtAccountNumber.text!.isEmpty{
            heightViewNumberUp.constant = 20
        }else if !txtBankLocation.text!.isEmpty{
            heightViewLocationUp.constant = 20
        }else if !txtBankName.text!.isEmpty{
            heightViewBankNameUp.constant = 20
        }else if !txtBicCode.text!.isEmpty{
            heightViewBicUp.constant = 20
        }
        
        
        
        txtEmail.delegate = self
        txtBankAccount.delegate = self
        txtAccountNumber.delegate = self
        txtBankLocation.delegate = self
        txtBankName.delegate = self
        txtBicCode.delegate = self
        
        viewMessage.isHidden = true
        viewMessageIn.layer.cornerRadius = 5
    }
    
    
    func viewColorDown(){
        viewREmailIn.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        viewRAccountIn.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        viewRNumberIn.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        viewRLocationIn.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        viewRBankNameIn.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        viewRBicIn.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)


        
        
    }
    
    
    func heightViewUp(){
        
        heightViewEmailUp.constant = 20
        heightViewAccountUp.constant = 20
        heightViewNumberUp.constant = 20
        heightViewLocationUp.constant = 20
        heightViewBankNameUp.constant = 20
        heightViewBicUp.constant = 20
    }
    
    func alert_emptyTextField(){
        
        var emptyField:Bool = false

        
        if txtEmail.text!.isEmpty{
            heightViewREmailDown.constant = 20
            self.viewREmailIn.backgroundColor = UIColor.red

            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            emptyField = true

        }
        if txtBankAccount.text!.isEmpty{
            heightViewRAccountDown.constant = 20
            self.viewRAccountIn.backgroundColor = UIColor.red

            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            emptyField = true

            
        }
        if txtAccountNumber.text!.isEmpty{
            heightViewRNumberDown.constant = 20
            self.viewRNumberIn.backgroundColor = UIColor.red

            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            emptyField = true

            
        }
        if txtBankLocation.text!.isEmpty{
            heightViewRLocationDown.constant = 20
            self.viewRLocationIn.backgroundColor = UIColor.red

            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            
        }
        if txtBankName.text!.isEmpty{
            heightViewRBankNameDown.constant = 20
            self.viewRBankNameIn.backgroundColor = UIColor.red

            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            emptyField = true

            
        }
        if txtBicCode.text!.isEmpty{
            heightViewRBicDown.constant = 20
            self.viewRBicIn.backgroundColor = UIColor.red

            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            emptyField = true

            
        }
        
        if emptyField == false{
            self.post_addBankDetails()
        }
        
    }
    
    func get_bankDetails(){
        
        self.txtEmail.text! = dictBankDetails.vPaymentEmail
        self.txtBankAccount.text! = dictBankDetails.vBankAccountHolderName
        self.txtAccountNumber.text! = dictBankDetails.vAccountNumber
        self.txtBankLocation.text! = dictBankDetails.vBankLocation
        self.txtBankName.text! = dictBankDetails.vBankName
        self.txtBicCode.text! = dictBankDetails.vBIC_SWIFT_Code
    }
    
    




    
    
    
}


// MARK: - Webservice calling

extension BankDetailsVC{
    
    func post_bankDetails(){

        ValidationManager.sharedManager.showLoader()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\("")&GeneralAppVersion=1.0.4&vBIC_SWIFT_Code=\(txtBicCode.text!)&eDisplay=Yes&vBankLocation=&type=DriverBankDetails&GeneralUserType=Driver&vBankName=\(txtBankName.text!)&tSessionId=\(sessionId)&iDriverId=\(userId)&GeneralDeviceType=Android&vBankAccountHolderName=\(txtBankAccount.text!)&GeneralMemberId=\(userId)&vPaymentEmail=\(txtEmail.text!)&vAccountNumber=\(txtAccountNumber.text!)&vUserDeviceCountry=\(country)&vDeviceToken=\("")&vTimeZone=Asia %2FCalcutta&userType=APP_TYPE"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kGetDetail),
                          URLQueryItem(name: "GeneralMemberId", value: self.userId),
                          URLQueryItem(name: "iUserId", value: self.userId),
                          URLQueryItem(name: "vFirebaseDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "vDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "vDeviceType", value: kDeviceType),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone)]

        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        
        
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)


                let action = json["Action"].stringValue
                
                if action == "1"{
                }
                
                if let message = json["message"].dictionaryObject{
                    self.dictBankDetails = MBankDetails.init(dict: message)
                    self.get_bankDetails()

                }
                
                
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    func post_addBankDetails(){

        
        ValidationManager.sharedManager.showLoader()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\("")&GeneralAppVersion=1.0.4&vBIC_SWIFT_Code=\(txtBicCode.text!)&eDisplay=No&vBankLocation=\(txtBankLocation.text!)&type=DriverBankDetails&GeneralUserType=Driver&vBankName=\(txtBankName.text!)&tSessionId=\(sessionId)&iDriverId=\(userId)&GeneralDeviceType=Android&vBankAccountHolderName=\(txtBankAccount.text!)&GeneralMe mberId=\(userId)&vPaymentEmail=\(txtEmail.text!)&vAccountNumber=\(txtAccountNumber.text!)&vUserDeviceCountry=\(country)&vDeviceToken=\("")&vTimeZone=Asia/Kolkata&userType=APP_TYPE"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kDriverBankDetails),
                          URLQueryItem(name: "eDisplay", value: "No"),
                          URLQueryItem(name: "GeneralMemberId", value: self.userId),
                          URLQueryItem(name: "iDriverId", value: self.userId),
                          URLQueryItem(name: "vFirebaseDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "vDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "vDeviceType", value: kDeviceType),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone),
                          URLQueryItem(name: "vBankLocation", value: self.txtBankLocation.text!),
                          URLQueryItem(name: "vBankName", value: self.txtBankName.text!),
                          URLQueryItem(name: "vBankAccountHolderName", value: self.txtBankAccount.text!),
                          URLQueryItem(name: "vPaymentEmail", value: self.txtEmail.text!),
                          URLQueryItem(name: "vAccountNumber", value: self.txtAccountNumber.text!),
                          URLQueryItem(name: "vBIC_SWIFT_Code", value: self.txtBicCode.text!)]

        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)


                let action = json["Action"].stringValue
                
                if action == "1"{
                    self.viewMessage.isHidden = false
                    
                }
                ValidationManager.sharedManager.hideLoader()
 
                
                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
}
