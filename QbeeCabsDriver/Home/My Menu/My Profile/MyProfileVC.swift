//
//  MyProfileVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 13/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON



var isOffline:Bool = false

class MyProfileVC: UIViewController, UITextFieldDelegate {

    
    // MARK: - Web service variable

    let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    // MARK: - Outlets

    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgProfileBack: UIImageView!
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet weak var txtCurrentPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtNewPasswordAgain: UITextField!
    @IBOutlet weak var viewOption: UIView!
    @IBOutlet weak var viewBtnEdit: UIView!
    @IBOutlet weak var viewBtnChange: UIView!
    @IBOutlet weak var viewChange: UIView!
    @IBOutlet weak var viewChangePassword: UIView!
    @IBOutlet weak var viewOffline: UIView!
    @IBOutlet weak var viewOfflineIn: UIView!
    @IBOutlet weak var bottomChangePass: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblFirstNameL: UILabel!
    @IBOutlet weak var lblLastNameL: UILabel!
    @IBOutlet weak var lblEmailL: UILabel!
    @IBOutlet weak var lblMobileL: UILabel!
    @IBOutlet weak var lblLanguageL: UILabel!
    @IBOutlet weak var lblCurrencyL: UILabel!
    @IBOutlet weak var btnEditProfile: UIButton!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var lblChangePasswordL: UILabel!
    @IBOutlet weak var btnOkL: UIButton!
    @IBOutlet weak var btnCancelL: UIButton!
    @IBOutlet weak var lblYouCantEdit: UILabel!
    @IBOutlet weak var btnOfflineOk: UIButton!
    @IBOutlet var viewButton: UIView!
    
    @IBOutlet weak var heightRCurrent: NSLayoutConstraint!
    @IBOutlet weak var heightREnteNew: NSLayoutConstraint!
    @IBOutlet weak var heightREnterNewAgain: NSLayoutConstraint!

    @IBOutlet weak var viewInCurrent: UIView!
    @IBOutlet weak var viewInEnteNew: UIView!
    @IBOutlet weak var viewInEnteNewAgain: UIView!
    
    @IBOutlet weak var lblRCurrent: UILabel!
    @IBOutlet weak var lblREnterNew: UILabel!
    @IBOutlet weak var lblREnterNewAgain: UILabel!


    @IBOutlet var viewButtonL: UIView!
    
    
    @IBOutlet weak var leadingViewOption: NSLayoutConstraint!
    @IBOutlet weak var trailingViewOption: NSLayoutConstraint!
    
    @IBOutlet weak var viewUpar: UIView!

    
    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.corner_radius()
        self.profile_details()
        self.bottomChangePass.constant = 0
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Button Actions


    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
        
    }
    @IBAction func btnOption(_ sender: Any) {
        if viewOption.isHidden == true{
            viewOption.isHidden = false
        }else{
            viewOption.isHidden = true
        }
    }
    
    @IBAction func btnEditProfile(_ sender: Any) {
        
        if isOffline == false{
            self.viewBtnEdit.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            self.viewBtnChange.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            self.viewOffline.isHidden = false
            ValidationManager.sharedManager.showMainViewNN(viewMain: viewOffline, viewInside: viewOfflineIn)
        }else{
            self.viewBtnEdit.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            self.viewBtnChange.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

            DispatchQueue.main.asyncAfter(deadline: .now()+0.3){
                ValidationManager.sharedManager.showLoader()
                
                let vc:EditProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
                self.navigationController?.pushViewController(vc, animated: true)
                self.viewOption.isHidden = true
                ValidationManager.sharedManager.hideLoader()

            }
        }
        


        
//        let vc:EditProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
//        self.present(vc, animated: true, completion: nil)

    }
    @IBAction func btnChangePassword(_ sender: Any) {
        
        
        self.viewBtnEdit.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnChange.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3){
            ValidationManager.sharedManager.showMainViewNN(viewMain: self.viewChange, viewInside: self.viewChangePassword)
//            self.bottomChangePass.constant = 0
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            
            self.viewOption.isHidden = true
        }
        


    }
    @IBAction func btnOk(_ sender: Any) {
        self.view.endEditing(true)
        if txtCurrentPassword.text!.isEmpty{
            
            self.viewInCurrent.backgroundColor = UIColor.red
            self.heightRCurrent.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblRCurrent.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
            
        }else if txtCurrentPassword.text!.count < 6 {
            
            self.viewInCurrent.backgroundColor = UIColor.red
            self.heightRCurrent.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            let text1 = objLang?["LBL_ERROR_PASS_LENGTH_PREFIX_SIGN_IN"] as? String ?? ""
            let text2 = objLang?["LBL_ERROR_PASS_LENGTH_SUFFIX_SIGN_IN"] as? String ?? ""

            lblRCurrent.text! = text1 + " 6 " + text2
            
            
        }else if txtNewPassword.text!.isEmpty{
            
            self.viewInEnteNew.backgroundColor = UIColor.red
            self.heightREnteNew.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblREnterNew.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
            
        }else if txtNewPassword.text!.count < 6 {
            
            self.viewInEnteNew.backgroundColor = UIColor.red
            self.heightREnteNew.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            let text1 = objLang?["LBL_ERROR_PASS_LENGTH_PREFIX_SIGN_IN"] as? String ?? ""
            let text2 = objLang?["LBL_ERROR_PASS_LENGTH_SUFFIX_SIGN_IN"] as? String ?? ""

            lblREnterNew.text! = text1 + " 6 " + text2
            
            
        }else if txtNewPasswordAgain.text!.isEmpty{
            self.viewInEnteNewAgain.backgroundColor = UIColor.red
            self.heightREnterNewAgain.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblREnterNewAgain.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
            
        }else if txtNewPassword.text! != txtNewPasswordAgain.text!{
            
            self.viewInEnteNewAgain.backgroundColor = UIColor.red
            self.heightREnterNewAgain.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblREnterNewAgain.text! = objLang?["LBL_RIDER_VERIFY_PASSWORD_ERROR_CHANGE_PASSWORD"] as? String ?? ""
        }else{
            self.post_changePassword()
        }
    }
    @IBAction func btnCancel(_ sender: Any) {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: viewChange, viewPopUP: viewChangePassword)

    }
    
    @IBAction func btnOfflineOk(_ sender: Any) {
//        self.viewOffline.isHidden = true
        ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: viewOffline, viewPopUP: viewOfflineIn)
        self.viewOption.isHidden = true
    }
    
    
    
    func corner_radius(){
        viewOption.isHidden = true
        viewChange.isHidden = true
        self.viewOffline.isHidden = true
        viewChangePassword.layer.cornerRadius  = 5
        viewOfflineIn.layer.cornerRadius  = 5
        imgProfile.layer.cornerRadius = imgProfile.frame.size.height/2
        imgProfile.layer.borderWidth = 2
        imgProfile.layer.borderColor = #colorLiteral(red: 1, green: 0.8588235294, blue: 0.2392156863, alpha: 1)
        
        txtCurrentPassword.delegate = self
        txtNewPassword.delegate = self
        txtNewPasswordAgain.delegate = self
    }
    
    func  profile_details(){

        lblFirstName.text! = UserDefaults.standard.string(forKey: "vName")!
        lblLastName.text! = UserDefaults.standard.string(forKey: "vLastName")!
        lblEmail.text! = UserDefaults.standard.string(forKey: "vEmail")!
        lblMobile.text! = UserDefaults.standard.string(forKey: "vCode")!+UserDefaults.standard.string(forKey: "vPhone")!
        lblLanguage.text! = UserDefaults.standard.string(forKey: "vLang")!
        lblCurrency.text! = UserDefaults.standard.string(forKey: "vCurrencyDriver")!


    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let imageUrlId = "https://apps.wagonstaxi.com/webimages/upload/Driver/"
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let imgUserId:String = UserDefaults.standard.string(forKey: "vImage") ?? ""
        
        let imageUrl = imageUrlId+"\(userId)/"+imgUserId
        print(imageUrl)
        DispatchQueue.global(qos: .background).async {
            do
            {
                let url = URL(string: imageUrl)
                if let data = try? Data(contentsOf: url!){
                    DispatchQueue.main.async {
                        self.imgProfile.image = UIImage(data: data)
                        self.imgProfileBack.image = UIImage(data: data)

                    }
                }

            }

        }
        
        
    }

    
    
//    func changeImage(){
//
//        Alamofire.upload(.POST,URLString: fullUrl, // http://httpbin.org/post
//              multipartFormData: { multipartFormData in
//                  multipartFormData.appendBodyPart(fileURL: imagePathUrl!, name: "photo")
//                  multipartFormData.appendBodyPart(fileURL: videoPathUrl!, name: "video")
//                  multipartFormData.appendBodyPart(data: Constants.AuthKey.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"authKey")
//                  multipartFormData.appendBodyPart(data: "\(16)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"idUserChallenge")
//                  multipartFormData.appendBodyPart(data: "comment".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"comment")
//                  multipartFormData.appendBodyPart(data:"\(0.00)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"latitude")
//                  multipartFormData.appendBodyPart(data:"\(0.00)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"longitude")
//                  multipartFormData.appendBodyPart(data:"India".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"location")
//              },
//              encodingCompletion: { encodingResult in
//                  switch encodingResult {
//                  case .Success(let upload, _, _):
//                      upload.responseJSON { request, response, JSON, error in
//
//
//                      }
//                  case .Failure(let encodingError):
//
//                  }
//              }
//          )
//
//    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCurrentPassword{
            
            self.viewInCurrent.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            self.viewInEnteNew.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.viewInEnteNewAgain.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)

            self.heightRCurrent.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtNewPassword{
            
            self.viewInCurrent.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.viewInEnteNew.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            self.viewInEnteNewAgain.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            
            self.heightREnteNew.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtNewPasswordAgain{
            
            self.viewInCurrent.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.viewInEnteNew.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.viewInEnteNewAgain.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            
            self.heightREnterNewAgain.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
        return true
        
    }
    
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_TITLE_VIEW_PROFILE"] as? String ?? ""
        lblFirstNameL.text! = objLang?["LBL_YOUR_FIRST_NAME"] as? String ?? ""
        lblLastNameL.text! = objLang?["LBL_YOUR_LAST_NAME"] as? String ?? ""
        lblEmailL.text! = objLang?["LBL_EMAIL_TEXT"] as? String ?? ""
        lblMobileL.text! = objLang?["LBL_MOBILE_NUMBER_HEADER_TXT"] as? String ?? ""
        lblLanguageL.text! = objLang?["LBL_LANGUAGE_TXT"] as? String ?? ""
        lblCurrencyL.text! = objLang?["LBL_CURRENCY_APP_LOGIN"] as? String ?? ""
        btnEditProfile.setTitle(objLang?["LBL_EDIT_PROFILE_TXT"] as? String ?? "", for: .normal)
        btnChangePassword.setTitle(objLang?["LBL_CHANGE_PASSWORD_TXT"] as? String ?? "", for: .normal)
        lblChangePasswordL.text! = objLang?["LBL_CHANGE_PASSWORD_TXT"] as? String ?? ""
        btnCancelL.setTitle(objLang?["LBL_CANCEL_TXT"] as? String ?? "", for: .normal)
        btnOkL.setTitle(objLang?["LBL_BTN_OK_TXT"] as? String ?? "", for: .normal)
        txtCurrentPassword.placeholder = objLang?["LBL_CURR_PASS_HEADER"] as? String ?? ""
        txtNewPassword.placeholder = objLang?["LBL_RIDER_UPDATE_PASSWORD_HEADER_TXT"] as? String ?? ""
        txtNewPasswordAgain.placeholder = objLang?["LBL_UPDATE_CONFIRM_PASSWORD_HINT_TXT"] as? String ?? ""
        lblChangePasswordL.text! = objLang?["LBL_CHANGE_PASSWORD_TXT"] as? String ?? ""
        lblYouCantEdit.text! = objLang?["LBL_EDIT_PROFILE_BLOCK_DRIVER"] as? String ?? ""
        btnOfflineOk.setTitle(objLang?["LBL_BTN_OK_TXT"] as? String ?? "", for: .normal)

        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft
            leadingViewOption.constant = 0
            trailingViewOption.isActive = false
                        
            self.lblFirstNameL.textAlignment = .right
            self.lblFirstName.textAlignment = .right
            self.lblLastNameL.textAlignment = .right
            self.lblLastName.textAlignment = .right
            self.lblEmailL.textAlignment = .right
            self.lblEmail.textAlignment = .right
            self.lblMobileL.textAlignment = .right
            self.lblMobile.textAlignment = .right
            self.lblLanguageL.textAlignment = .right
            self.lblLanguage.textAlignment = .right
            self.lblCurrencyL.textAlignment = .right
            self.lblCurrency.textAlignment = .right
            self.txtCurrentPassword.textAlignment = .right
            self.txtNewPassword.textAlignment = .right
            self.txtNewPasswordAgain.textAlignment = .right
            self.lblChangePasswordL.textAlignment = .right
            self.lblYouCantEdit.textAlignment = .right

            
        }else{
            leadingViewOption.isActive = false
            trailingViewOption.constant = 0
        }
        
        
        
    }
    
    
}

// MARK: - Webservice calling

extension MyProfileVC{
    
    func post_changePassword(){
        
        ValidationManager.sharedManager.showLoader()
        
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&pass=\(txtNewPassword.text!)&type=updatePassword&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios&GeneralMemberId=\(userId)&vUser DeviceCountry=\(country)&UserID=\(userId)&vTimeZone=Asia%2FCalcutta&CurrentPassword=\(txtCurrentPassword.text!)&UserType=Driver"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kUpdatePassword),
                          URLQueryItem(name: "GeneralMemberId", value: self.userId),
                          URLQueryItem(name: "UserID", value: self.userId),
                          URLQueryItem(name: "vFirebaseDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "vDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "vDeviceType", value: kDeviceType),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone),
                          URLQueryItem(name: "CurrentPassword", value: self.txtCurrentPassword.text!),
                          URLQueryItem(name: "pass", value: self.txtNewPasswordAgain.text!)]

        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    let textAlert = objLang?["LBL_INFO_UPDATED_TXT_MY_PROFILE"] as? String ?? ""
                    let textOk = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
                    ValidationManager.sharedManager.showAlert(message: textAlert, title: "", btnTitle: textOk, controller: self)
                    self.viewChange.isHidden = true

                }else{
                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    let textAlert = objLang?["LBL_WRONG_PASSWORD"] as? String ?? ""
                    let textOk = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
                    ValidationManager.sharedManager.showAlert(message: textAlert, title: "", btnTitle: textOk, controller: self)
                }
                
                
                ValidationManager.sharedManager.hideLoader()

                
            case .failure( _): break
                
                //                failure(error)
            }
        }
        
    }
    
    
    
}
