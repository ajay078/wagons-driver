//
//  EditProfileVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 13/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EditProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, SendDataBackDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    func sendData(addCountryCode: String) {
        txtCoutryCode.text = addCountryCode
    }
    

    var imagePicker = UIImagePickerController()
    var imgUploadProfile:UIImage? = nil

    var selectedIndex:Int = 10000
    var isLanguage:Bool = false
    var isIndexSelected:Bool = false
    
    var selectedLanguage = ""
    var selectedCurrency = ""
    
    var arrListLang = [ModalListLang]()
    var arrListCurrency = [ModalListCurrency]()

    
    // MARK: - Web service variable

    let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    // MARK: - Outlets

    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgProfileBack: UIImageView!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtCoutryCode: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtLanguage: UITextField!
    @IBOutlet weak var txtCurrency: UITextField!
    @IBOutlet weak var txtCurrentPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtNewPasswordAgain: UITextField!
    
    @IBOutlet weak var viewOption: UIView!
    @IBOutlet weak var viewBtnEdit: UIView!
    @IBOutlet weak var viewBtnChange: UIView!
    
    @IBOutlet weak var viewChange: UIView!
    @IBOutlet weak var viewChangePassword: UIView!

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblFirstNameL: UILabel!
    @IBOutlet weak var lblLastNameL: UILabel!
    @IBOutlet weak var lblEmailL: UILabel!
    @IBOutlet weak var lblCountryCodeL: UILabel!
    @IBOutlet weak var lblMobileL: UILabel!
    @IBOutlet weak var lblLanguageL: UILabel!
    @IBOutlet weak var lblCurrencyL: UILabel!
    @IBOutlet weak var btnUpdateInformation: UIButton!
    @IBOutlet weak var btnViewProfile: UIButton!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var viewPen: UIView!
    
    @IBOutlet weak var lblChangePasswordL: UILabel!
    @IBOutlet weak var btnOkL: UIButton!
    @IBOutlet weak var btnCancelL: UIButton!
    @IBOutlet weak var leadingViewOption: NSLayoutConstraint!
    @IBOutlet weak var trailingViewOption: NSLayoutConstraint!
    @IBOutlet weak var viewUpar: UIView!
    
    @IBOutlet weak var heightRCurrent: NSLayoutConstraint!
    @IBOutlet weak var heightREnteNew: NSLayoutConstraint!
    @IBOutlet weak var heightREnterNewAgain: NSLayoutConstraint!

    @IBOutlet weak var viewInCurrent: UIView!
    @IBOutlet weak var viewInEnteNew: UIView!
    @IBOutlet weak var viewInEnteNewAgain: UIView!
    
    @IBOutlet weak var lblRCurrent: UILabel!
    @IBOutlet weak var lblREnterNew: UILabel!
    @IBOutlet weak var lblREnterNewAgain: UILabel!


    @IBOutlet weak var viewLang: UIView!
    @IBOutlet weak var viewLangIn: UIView!
    
    @IBOutlet var lblSelectLangCurrencyL: UILabel!

    
    
    @IBOutlet var viewButtonL: UIView!
    
    
    @IBOutlet weak var tableViewLang: UITableView!

 
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.corner_radius()
        self.profile_details()
        self.post_detailsUser()
        self.post_listLang()
        imagePicker.delegate = self
        imgProfile.image = UIImage(named: "profile placeHolder")
    }
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    // MARK: - Button Actions

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
//        navigationController?.popViewController(animated: true)
//        dismiss(animated: true, completion: nil)
        
//        let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
//
//               for aViewController in viewControllers {
//                   if(aViewController is LogInVC){
//                        self.navigationController!.popToViewController(aViewController, animated: true);
//                   }
//               }
    }
    @IBAction func btnOption(_ sender: Any) {
        if viewOption.isHidden == true{
            viewOption.isHidden = false
        }else{
            viewOption.isHidden = true
        }
    }
    @IBAction func btnViewProfile(_ sender: Any) {
        
        self.viewBtnEdit.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        self.viewBtnChange.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2){
            self.navigationController?.popViewController(animated: true)

        }
  
//        navigationController?.popViewController(animated: true)
//        dismiss(animated: true, completion: nil)
        

    }
    @IBAction func btnChangePassword(_ sender: Any) {
        
        self.viewBtnEdit.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnChange.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2){
            ValidationManager.sharedManager.showMainViewNN(viewMain: self.viewChange, viewInside: self.viewChangePassword)
            self.viewOption.isHidden = true
        }
        

    }
    
    @IBAction func btnProfile(_ sender: Any) {
        self.camera_photo()
    }
    
    @IBAction func btnUpdate(_ sender: Any) {
        self.post_editProfile()

    }
    @IBAction func btnCountryCode(_ sender: Any) {
        self.view.endEditing(true)
        let sb:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc:CountryCodeVC = sb.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        vc.delegateCountry = self
        self.present(vc, animated: true, completion: nil)
        
//        let vc:CountryCodeVC = self.storyboard?.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
//        vc.delegateCountry = self
//        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnOk(_ sender: Any) {
        self.view.endEditing(true)
        if txtCurrentPassword.text!.isEmpty{
            
            self.viewInCurrent.backgroundColor = UIColor.red
            self.heightRCurrent.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblRCurrent.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
            
        }else if txtCurrentPassword.text!.count < 6 {
            
            self.viewInCurrent.backgroundColor = UIColor.red
            self.heightRCurrent.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            let text1 = objLang?["LBL_ERROR_PASS_LENGTH_PREFIX_SIGN_IN"] as? String ?? ""
            let text2 = objLang?["LBL_ERROR_PASS_LENGTH_SUFFIX_SIGN_IN"] as? String ?? ""

            lblRCurrent.text! = text1 + " 6 " + text2
            
            
        }else if txtNewPassword.text!.isEmpty{
            
            self.viewInEnteNew.backgroundColor = UIColor.red
            self.heightREnteNew.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblREnterNew.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
            
        }else if txtNewPassword.text!.count < 6 {
            
            self.viewInEnteNew.backgroundColor = UIColor.red
            self.heightREnteNew.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            let text1 = objLang?["LBL_ERROR_PASS_LENGTH_PREFIX_SIGN_IN"] as? String ?? ""
            let text2 = objLang?["LBL_ERROR_PASS_LENGTH_SUFFIX_SIGN_IN"] as? String ?? ""

            lblREnterNew.text! = text1 + " 6 " + text2
            
            
        }else if txtNewPasswordAgain.text!.isEmpty{
            self.viewInEnteNewAgain.backgroundColor = UIColor.red
            self.heightREnterNewAgain.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblREnterNewAgain.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
            
        }else if txtNewPassword.text! != txtNewPasswordAgain.text!{
            
            self.viewInEnteNewAgain.backgroundColor = UIColor.red
            self.heightREnterNewAgain.constant = 20
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblREnterNewAgain.text! = objLang?["LBL_RIDER_VERIFY_PASSWORD_ERROR_CHANGE_PASSWORD"] as? String ?? ""
        }else{
            self.post_changePassword()
        }
    }
    @IBAction func btnCancel(_ sender: Any) {
        ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: viewChange, viewPopUP: viewChangePassword)

        self.view.endEditing(true)

    }
    
    
    @IBAction func btnLanguage(_ sender: Any) {
        isLanguage = true
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblSelectLangCurrencyL.text! = objLang?["LBL_SELECT_LANGUAGE_TXT"] as? String ?? ""
        
        ValidationManager.sharedManager.showMainViewNN(viewMain: viewLang, viewInside: viewLangIn)

        self.tableViewLang.reloadData()
        self.view.endEditing(true)
    }
    @IBAction func btnCurrency(_ sender: Any) {
        isLanguage = false
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblSelectLangCurrencyL.text! = objLang?["LBL_SELECT_CURRENCY"] as? String ?? ""
        
        self.tableViewLang.reloadData()
        ValidationManager.sharedManager.showMainViewNN(viewMain: viewLang, viewInside: viewLangIn)

        self.view.endEditing(true)

    }
    @IBAction func btnHideViewLang(_ sender: Any) {
        ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: viewLang, viewPopUP: viewLangIn)

    }
   
// MARK: - Text Field Methods

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtMobile{
            ValidationManager.sharedManager.addDoneButtonOnKeyboard(textType: txtMobile)
        }
        
        if textField == txtCurrentPassword{
            
            self.viewInCurrent.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            self.viewInEnteNew.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.viewInEnteNewAgain.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)

            self.heightRCurrent.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtNewPassword{
            
            self.viewInCurrent.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.viewInEnteNew.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            self.viewInEnteNewAgain.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            
            self.heightREnteNew.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }else if textField == txtNewPasswordAgain{
            
            self.viewInCurrent.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.viewInEnteNew.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.viewInEnteNewAgain.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
            
            self.heightREnterNewAgain.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
        return true
    }
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_EDIT_PROFILE_TXT"] as? String ?? ""
        lblFirstNameL.text! = objLang?["LBL_YOUR_FIRST_NAME"] as? String ?? ""
        lblLastNameL.text! = objLang?["LBL_YOUR_LAST_NAME"] as? String ?? ""
        lblEmailL.text! = objLang?["LBL_EMAIL_TEXT"] as? String ?? ""
        lblMobileL.text! = objLang?["LBL_MOBILE_NUMBER_HEADER_TXT"] as? String ?? ""
        lblLanguageL.text! = objLang?["LBL_LANGUAGE_TXT"] as? String ?? ""
        lblCurrencyL.text! = objLang?["LBL_CURRENCY_APP_LOGIN"] as? String ?? ""
        btnUpdateInformation.setTitle(objLang?["LBL_BTN_PROFILE_UPDATE_PAGE_TXT"] as? String ?? "", for: .normal)
        btnViewProfile.setTitle(objLang?["LBL_VIEW_PROFILE_TXT"] as? String ?? "", for: .normal)
        btnChangePassword.setTitle(objLang?["LBL_CHANGE_PASSWORD_TXT"] as? String ?? "", for: .normal)
        txtCurrentPassword.placeholder = objLang?["LBL_CURR_PASS_HEADER"] as? String ?? ""
        txtNewPassword.placeholder = objLang?["LBL_RIDER_UPDATE_PASSWORD_HEADER_TXT"] as? String ?? ""
        txtNewPasswordAgain.placeholder = objLang?["LBL_UPDATE_CONFIRM_PASSWORD_HINT_TXT"] as? String ?? ""
        lblChangePasswordL.text! = objLang?["LBL_CHANGE_PASSWORD_TXT"] as? String ?? ""
        btnCancelL.setTitle(objLang?["LBL_CANCEL_TXT"] as? String ?? "", for: .normal)
        btnOkL.setTitle(objLang?["LBL_BTN_OK_TXT"] as? String ?? "", for: .normal)
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft
//            self.viewLanguageSelect.semanticContentAttribute = .forceRightToLeft
//            self.viewCurrencySelect.semanticContentAttribute = .forceRightToLeft
            leadingViewOption.constant = 0
            trailingViewOption.isActive = false
            
            
            self.lblFirstNameL.textAlignment = .right
            self.txtFirstName.textAlignment = .right
            self.lblLastNameL.textAlignment = .right
            self.txtLastName.textAlignment = .right
            self.lblEmailL.textAlignment = .right
            self.txtEmail.textAlignment = .right
            self.lblMobileL.textAlignment = .right
            self.txtMobile.textAlignment = .right
            self.txtCurrentPassword.textAlignment = .right
            self.txtNewPassword.textAlignment = .right
            self.txtNewPasswordAgain.textAlignment = .right
            self.lblChangePasswordL.textAlignment = .right

//           self.lblLanguageL.textAlignment = .right
//            self.txtLanguage.textAlignment = .right
//            self.lblCurrencyL.textAlignment = .right
//            self.txtCurrency.textAlignment = .right
            
        }else{
            leadingViewOption.isActive = false
            trailingViewOption.constant = 0
        }
        
    }
    
    
    func corner_radius(){
        
        viewLang.isHidden = true
        viewLangIn.layer.cornerRadius  = 5
        
        viewOption.isHidden = true
        viewChange.isHidden = true
        viewChangePassword.layer.cornerRadius  = 5
        imgProfile.layer.cornerRadius = imgProfile.frame.size.height/2
        imgProfile.layer.borderWidth = 2
        imgProfile.layer.borderColor = #colorLiteral(red: 1, green: 0.8588235294, blue: 0.2392156863, alpha: 1)
        viewPen.layer.cornerRadius = viewPen.frame.size.height/2

        txtMobile.delegate = self
        txtCurrentPassword.delegate = self
        txtNewPassword.delegate = self
        txtNewPasswordAgain.delegate = self
    }

    
    func camera_photo(){

        if UIDevice.current.userInterfaceIdiom == .pad{
            let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        

    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
//        if imgProfile.image == UIImage(named: "profile placeHolder"){
            self.imgUploadProfile = image
            imgProfile.image = image
            
            //            self.imgProfile = UIImagePNGRepresentation(imgProfile.image!) as UIImage
            
//        }
        dismiss(animated: true, completion: nil)
        
    }
    
    
    func profile_details(){
        
        txtFirstName.text! = UserDefaults.standard.string(forKey: "vName")!
        txtLastName.text! = UserDefaults.standard.string(forKey: "vLastName")!
        txtEmail.text! = UserDefaults.standard.string(forKey: "vEmail")!
        txtCoutryCode.text! = UserDefaults.standard.string(forKey: "vCode")!
        txtMobile.text! = UserDefaults.standard.string(forKey: "vPhone")!
        txtLanguage.text! = UserDefaults.standard.string(forKey: "vLang")!
        txtCurrency.text! = UserDefaults.standard.string(forKey: "vCurrencyDriver")!
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        
        let imageUrlId = "https://apps.wagonstaxi.com/webimages/upload/Driver/"
        let imgUserId:String = UserDefaults.standard.string(forKey: "vImage") ?? ""
        
        let imageUrl = imageUrlId+"\(userId)/"+imgUserId
        DispatchQueue.global(qos: .background).async {
            do
            {
                let url = URL(string: imageUrl)
                if let data = try? Data(contentsOf: url!){
                    DispatchQueue.main.async {
                        self.imgProfile.image = UIImage(data: data)
                        self.imgProfileBack.image = UIImage(data: data)
                        
                    }
                }
                
            }

        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        
        let imageUrlId = "https://apps.wagonstaxi.com/webimages/upload/Driver/"
        let imgUserId:String = UserDefaults.standard.string(forKey: "vImage") ?? ""
        
        let imageUrl = imageUrlId+"\(userId)/"+imgUserId
        DispatchQueue.global(qos: .background).async {
            do
            {
                let url = URL(string: imageUrl)
                if let data = try? Data(contentsOf: url!){
                    DispatchQueue.main.async {
                        self.imgProfile.image = UIImage(data: data)
                        self.imgProfileBack.image = UIImage(data: data)
                        
                    }
                }
                
            }

        }
    }
    
    // MARK: - Table View Methods

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if isLanguage == true{
            return arrListLang.count
        }else{
            return arrListCurrency.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:EditProfileCell = tableView.dequeueReusableCell(withIdentifier: "EditProfileCell", for: indexPath) as! EditProfileCell
        
        

        

            if isLanguage == true{
                cell.lblTitle.text! = arrListLang[indexPath.row].vTitle
                
                let indexOfSelectedLang = self.arrListLang.firstIndex(where: {$0.vCode == self.selectedLanguage})

                if indexOfSelectedLang == indexPath.row{
                    cell.viewCell.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
                }else{
                    cell.viewCell.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
            }else{
                cell.lblTitle.text! = arrListCurrency[indexPath.row].vName
                
                let indexOfSelectedCurrency = self.arrListCurrency.firstIndex(where: {$0.vName == self.selectedCurrency})

                if indexOfSelectedCurrency == indexPath.row{
                    cell.viewCell.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
                }else{
                    cell.viewCell.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
            }
        

        
        
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isLanguage == true{
            txtLanguage.text! = arrListLang[indexPath.row].vTitle
            isIndexSelected = true
        }else{
            txtCurrency.text! = arrListCurrency[indexPath.row].vName
            isIndexSelected = true
        }
        
        
        
        self.selectedIndex = indexPath.row
        self.tableViewLang.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2){
//            self.viewLang.isHidden = true
            ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: self.viewLang, viewPopUP: self.viewLangIn)
        }
        
    }
    
}



// MARK: - Webservice calling

extension EditProfileVC{
    
    func post_editProfile(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        ValidationManager.sharedManager.showLoader()

        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&vName=\(txtFirstName.text!)&GeneralAppVersion=1.0.4&iMemberId=\(userId)&tProfileDescription=&vPhoneCode=\(txtCoutryCode.text!)&type=updateUserProfileDetail&vPhone=\(txtMobile.text!)&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android&CurrencyCode=INR&GeneralMemberId=\(userId)&LanguageCode= EN&vLastName=\(txtLastName.text!)&vUserDeviceCountry=\(country)&vTimeZone=Asia%2FCalcutta&vCountry=IN&vEmail=\(txtEmail.text!)&UserType=Driver"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    
                    UserDefaults.standard.set( self.txtFirstName.text!, forKey: "vName")
                    UserDefaults.standard.set( self.txtLastName.text!, forKey: "vLastName")
                    UserDefaults.standard.set( self.txtEmail.text!, forKey: "vEmail")
                    UserDefaults.standard.set( self.txtCoutryCode.text!, forKey: "vCode")
                    UserDefaults.standard.set( self.txtMobile.text!, forKey: "vPhone")
                    
                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    let text = objLang?["LBL_INFO_UPDATED_TXT"] as? String ?? ""
                    let textOk = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
                    ValidationManager.sharedManager.showAlert(message: text, title: "", btnTitle: textOk, controller: self)
                    
                    if self.imgUploadProfile != nil{
                        let urlImage = "https://apps.wagonstaxi.com/webservice.php?iMemberId=\(userId)&type=uploadImage&MemberType=Driver&GeneralUserType=Android&tSessionId=\(sessionId)&GeneralMemberId=\(userId)&UserType=Driver"
                        let newURL = urlImage.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
                        
                        let parameter:[String:Any] = [:]
                        let headers = ["Authorization" : "Bearer",
                                       "Content-Type": "application/json"]
                        self.uploadPhoto(newURL!, image: self.imgUploadProfile!, params: parameter, header: headers)
                    }
                }else{
                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    let text = objLang?["LBL_SOMETHING_WENT_WRONG"] as? String ?? ""
                    let textOk = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""

                    ValidationManager.sharedManager.showAlert(message: text, title: textOk, btnTitle: "", controller: self)

                }
                
                
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
        func uploadPhoto(_ url: String, image: UIImage, params: [String : Any], header: [String:String]) {
        let httpHeaders = HTTPHeaders(header)
        AF.upload(multipartFormData: { multiPart in
            for p in params {
                multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
            }
           multiPart.append(image.jpegData(compressionQuality: 0.4)!, withName: "vImage", fileName: "file.jpg", mimeType: "image/jpg")
        }, to: url, method: .post, headers: httpHeaders) .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
        }).responseJSON(completionHandler: { data in
            print("upload finished: \(data)")
        }).response { (response) in
            switch response.result {
            case .success(let resut):
                print("upload success result: \(String(describing: resut))")
                self.post_UpdatedDetails()
            case .failure(let err):
                print("upload err: \(err)")
            }
        }
    }
    
    func post_changePassword(){
        
        ValidationManager.sharedManager.showLoader()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&pass=\(txtNewPassword.text!)&type=updatePassword&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android&GeneralMemberId=\(userId)&vUser DeviceCountry=\(country)&UserID=\(userId)&vTimeZone=Asia%2FCalcutta&CurrentPassword=\(txtCurrentPassword.text!)&UserType=Driver"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kUpdatePassword),
                          URLQueryItem(name: "GeneralMemberId", value: self.userId),
                          URLQueryItem(name: "UserID", value: self.userId),
                          URLQueryItem(name: "vFirebaseDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "vDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "vDeviceType", value: kDeviceType),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone),
                          URLQueryItem(name: "CurrentPassword", value: self.txtCurrentPassword.text!),
                          URLQueryItem(name: "pass", value: self.txtNewPasswordAgain.text!)]

        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    let textAlert = objLang?["LBL_INFO_UPDATED_TXT_MY_PROFILE"] as? String ?? ""
                    let textOk = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
                    ValidationManager.sharedManager.showAlert(message: textAlert, title: "", btnTitle: textOk, controller: self)
                    self.viewChange.isHidden = true

                }else{
                    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
                    let textAlert = objLang?["LBL_WRONG_PASSWORD"] as? String ?? ""
                    let textOk = objLang?["LBL_BTN_OK_TXT"] as? String ?? ""
                    ValidationManager.sharedManager.showAlert(message: textAlert, title: "", btnTitle: textOk, controller: self)
                }
                
                
                ValidationManager.sharedManager.hideLoader()

                
            case .failure( _): break
                
                //                failure(error)
            }
        }
        
    }
    
    
    func post_listLang(){
        
        
        ValidationManager.sharedManager.showLoader()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?AppVersion=1.0.4&GeneralMemberId=&GeneralAppVersion=1.0.4&vUserDeviceCountry=US&vLang=\(self.selectedLanguage)&vTimeZone=Asia %2FKolkata&type=generalConfigData&UserType=Driver&GeneralUserType=Driver&tSessionId=&GeneralDeviceType=ios"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    if let listLang = json["LIST_LANGUAGES"].arrayObject{
                        for index in 0..<listLang.count{
                            let obj = listLang[index]
                            let modalObj = ModalListLang.init(dict: obj as? [String : Any] ?? [:])
                            self.arrListLang.append(modalObj)
                        }
                    }
                    
                    if let listCurrency = json["LIST_CURRENCY"].arrayObject{
                        for index in 0..<listCurrency.count{
                            let obj = listCurrency[index]
                            let modalObj = ModalListCurrency.init(dict: obj as? [String : Any] ?? [:])
                            self.arrListCurrency.append(modalObj)
                        }
                    }
                    
                    if let defaultLanguage = json["DefaultLanguageValues"].dictionaryObject{
                        self.selectedLanguage = defaultLanguage["vCode"] as? String ?? ""
                    }
                    
                    self.tableViewLang.reloadData()
                    
                }
                
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    func post_detailsUser(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        
        ValidationManager.sharedManager.showLoader()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&vLang=EN&vDeviceType=ios&type=getDetail&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios&AppVersion=1.0.4&GeneralMemberId=\(userId)&iUserId=\(userId)&vUserDeviceCountry=\(country)&vDeviceToken=\(kDeviceToken)&vTimeZone=Asia/Kolkata&UserType=Driver"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                 print("action is \(action)")
                 
                if action == "1"{
                    if let message = json["message"].dictionaryObject{
                        self.selectedLanguage = message["vLang"] as? String ?? ""
                        self.selectedCurrency = message["vCurrencyDriver"] as? String ?? ""


                    }
                }


                 ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
                print("\(error.localizedDescription)")

            }
        }
        
    }
    
    func post_UpdatedDetails(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        
        ValidationManager.sharedManager.showLoader()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&vLang=EN&vDeviceType=ios&type=getDetail&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios&AppVersion=1.0.4&GeneralMemberId=\(userId)&iUserId=\(userId)&vUserDeviceCountry=\(country)&vDeviceToken=\(kDeviceToken)&vTimeZone=Asia/Kolkata&UserType=Driver"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    
                    if let message = json["message"].dictionaryObject{
                        let vImage = message["vImage"] as? String ?? ""
                        UserDefaults.standard.setValue(vImage, forKey: "vImage")
                    }
                    self.profile_details()
                }else{
                    ValidationManager.sharedManager.hideLoader()
                }
                
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
}
