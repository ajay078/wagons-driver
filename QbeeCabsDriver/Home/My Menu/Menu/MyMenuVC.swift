//
//  MyMenuVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 11/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class MyMenuVC: UIViewController, SWRevealViewControllerDelegate {

    
    
    var window: UIWindow?
    var navigationcontroller:UINavigationController?
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblBalance: UILabel!

    @IBOutlet weak var btnLogOut: UIButton!

    @IBOutlet weak var viewBtnMyProfile: UIView!
    @IBOutlet weak var viewBtnManageVehicle: UIView!
    @IBOutlet weak var viewBtnManageDocument: UIView!
    @IBOutlet weak var viewBtnYourTrips: UIView!
    @IBOutlet weak var viewBtnBankDetails: UIView!
    @IBOutlet weak var viewBtnMyWallet: UIView!
    @IBOutlet weak var viewBtnWayBill: UIView!
    @IBOutlet weak var viewBtnHeatView: UIView!
    @IBOutlet weak var viewBtnEmergency: UIView!
    @IBOutlet weak var viewBtnRiderFeedback: UIView!
    @IBOutlet weak var viewBtnTripStatistics: UIView!
    @IBOutlet weak var viewBtnSupport: UIView!
    @IBOutlet weak var viewBtnLogOut: UIView!

    
    @IBOutlet weak var lblMyProfile: UILabel!
    @IBOutlet weak var lblManageVehicles: UILabel!
    @IBOutlet weak var lblManageDocument: UILabel!
    @IBOutlet weak var lblYourTrips: UILabel!
    @IBOutlet weak var lblBankDetails: UILabel!
    @IBOutlet weak var lblMyWallet: UILabel!
    @IBOutlet weak var lblWayBill: UILabel!
    @IBOutlet weak var lblHeatView: UILabel!
    @IBOutlet weak var lblEmergencyContact: UILabel!
    @IBOutlet weak var lblRiderFeedback: UILabel!
    @IBOutlet weak var lblTripStatistics: UILabel!
    @IBOutlet weak var lblSupport: UILabel!
    @IBOutlet weak var lblLogOut: UILabel!
    
    
    @IBOutlet weak var viewUpar: UIView!
    
    @IBOutlet weak var centerXName: NSLayoutConstraint!
    @IBOutlet weak var centerXImgProfile: NSLayoutConstraint!
    @IBOutlet weak var centerXBalance: NSLayoutConstraint!
    
    @IBOutlet weak var viewForSide: UIView!
    @IBOutlet weak var viewProfile: UIView!
    
    
// MARK: - View Did Load

    
    override func viewDidLoad() {
        super.viewDidLoad()


        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            btnLogOut.addTarget(revealViewController, action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
        }else{
            btnLogOut.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            
        }
        
        self.revealViewController().delegate=self
        self.setLanguageLabel()
        self.profile_details()
        self.corner_radius()
    }
    

    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
    }

// MARK: - Button Actions


    @IBAction func btnMyProfile(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnMyProfile.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:MyProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnManageVehicle(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnManageVehicle.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:ManageVehicleVC = self.storyboard?.instantiateViewController(withIdentifier: "ManageVehicleVC") as! ManageVehicleVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnMangeDocument(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnManageDocument.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:SelectDocumentVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectDocumentVC") as! SelectDocumentVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnYourTrips(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnYourTrips.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:YourTripsVC = self.storyboard?.instantiateViewController(withIdentifier: "YourTripsVC") as! YourTripsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnBankDetails(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnBankDetails.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:BankDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "BankDetailsVC") as! BankDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnMyWallet(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnMyWallet.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:WalletVC = self.storyboard?.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnWayBill(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnWayBill.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:WayBillVC = self.storyboard?.instantiateViewController(withIdentifier: "WayBillVC") as! WayBillVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnHeatView(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnHeatView.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:HeatViewVC = self.storyboard?.instantiateViewController(withIdentifier: "HeatViewVC") as! HeatViewVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnEmergencyContact(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnEmergency.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:EmergencyContactVC = self.storyboard?.instantiateViewController(withIdentifier: "EmergencyContactVC") as! EmergencyContactVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnRiderFeedback(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnRiderFeedback.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:RiderFeedbackVC = self.storyboard?.instantiateViewController(withIdentifier: "RiderFeedbackVC") as! RiderFeedbackVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnTripStatistics(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnTripStatistics.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:TripStatisticsVC = self.storyboard?.instantiateViewController(withIdentifier: "TripStatisticsVC") as! TripStatisticsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnSupport(_ sender: Any) {
        self.colorMenuButton()
        self.viewBtnSupport.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:SupportVC = self.storyboard?.instantiateViewController(withIdentifier: "SupportVC") as! SupportVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func btnLogOut(_ sender: Any) {
        
        
        let dictBool:[String: Bool] = ["isLogoutPopUp": true]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isViewLogoutPopShow"), object: nil, userInfo: dictBool)
        
        viewForSide.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
//        UserDefaults.standard.removeObject(forKey: "tSessionId")
//        UserDefaults.standard.removeObject(forKey: "iDriverId")
//        UserDefaults.standard.removeObject(forKey: "vName")
//        UserDefaults.standard.removeObject(forKey: "vLastName")
//        UserDefaults.standard.removeObject(forKey: "vCountry")
//        UserDefaults.standard.removeObject(forKey: "vImage")
//
//
//
//
//        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle:Bundle.main)
//        navigationcontroller = sb.instantiateViewController(withIdentifier: "navMain") as? UINavigationController
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window?.rootViewController = navigationcontroller
//        window?.makeKeyAndVisible()
        
        
        
        
    }
    
// MARK: - set Language Labels

        func setLanguageLabel(){
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")

            lblMyProfile.text! = objLang?["LBL_MY_PROFILE_HEADER_TXT"] as? String ?? ""
            lblManageVehicles.text! = objLang?["LBL_MANAGE_VEHICLES"] as? String ?? ""
            lblManageDocument.text! = objLang?["LBL_MANAGE_DOCUMENT"] as? String ?? ""
            lblYourTrips.text! = objLang?["LBL_YOUR_TRIPS"] as? String ?? ""
            lblBankDetails.text! = objLang?["LBL_BANK_DETAILS_TXT"] as? String ?? ""
            lblMyWallet.text! = objLang?["LBL_RIDER_WALLET"] as? String ?? ""
            lblWayBill.text! = objLang?["LBL_MENU_WAY_BILL"] as? String ?? ""
            lblHeatView.text! = objLang?["LBL_TITLE_HEAT_VIEW"] as? String ?? ""
            lblEmergencyContact.text! = objLang?["LBL_EMERGENCY_CONTACT_MENU_SCREEN"] as? String ?? ""
            lblRiderFeedback.text! = objLang?["LBL_RIDER_FEEDBACK"] as? String ?? ""
            lblTripStatistics.text! = objLang?["LBL_TRIP_STATISTICS_STATISTICS"] as? String ?? ""
            lblSupport.text! = objLang?["LBL_SUPPORT_HEADER_TXT"] as? String ?? ""
            lblLogOut.text! = objLang?["LBL_LOGOUT"] as? String ?? ""

            
            
            let langCode = UserDefaults.standard.string(forKey: "default_language")
            if langCode == "AR"{
                
                self.viewBtnMyProfile.semanticContentAttribute = .forceRightToLeft
                self.viewBtnManageVehicle.semanticContentAttribute = .forceRightToLeft
                self.viewBtnManageDocument.semanticContentAttribute = .forceRightToLeft
                self.viewBtnYourTrips.semanticContentAttribute = .forceRightToLeft
                self.viewBtnBankDetails.semanticContentAttribute = .forceRightToLeft
                self.viewBtnMyWallet.semanticContentAttribute = .forceRightToLeft
                self.viewBtnWayBill.semanticContentAttribute = .forceRightToLeft
                self.viewBtnHeatView.semanticContentAttribute = .forceRightToLeft
                self.viewBtnEmergency.semanticContentAttribute = .forceRightToLeft
                self.viewBtnRiderFeedback.semanticContentAttribute = .forceRightToLeft
                self.viewBtnTripStatistics.semanticContentAttribute = .forceRightToLeft
                self.viewBtnSupport.semanticContentAttribute = .forceRightToLeft
                self.viewBtnLogOut.semanticContentAttribute = .forceRightToLeft

                self.lblMyProfile.textAlignment = .right
                self.lblManageVehicles.textAlignment = .right
                self.lblManageDocument.textAlignment = .right
                self.lblYourTrips.textAlignment = .right
                self.lblBankDetails.textAlignment = .right
                self.lblMyWallet.textAlignment = .right
                self.lblWayBill.textAlignment = .right
                self.lblHeatView.textAlignment = .right
                self.lblEmergencyContact.textAlignment = .right
                self.lblRiderFeedback.textAlignment = .right
                self.lblTripStatistics.textAlignment = .right
                self.lblSupport.textAlignment = .right
                self.lblLogOut.textAlignment = .right

                centerXName.constant = 30
                centerXBalance.constant = 30
                centerXImgProfile.constant = 30
                
            }
            
        }
    
    
    func corner_radius(){
        imgProfile.layer.cornerRadius = imgProfile.frame.size.height/2
    }
    
    
    func profile_details(){



        
        


    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let firstName = UserDefaults.standard.string(forKey: "vName")!
        let lastName = UserDefaults.standard.string(forKey: "vLastName")!
        
        lblUserName.text! = "\(firstName) \(lastName)"
        
        
        let balance:String = UserDefaults.standard.string(forKey: "user_available_balance") ?? ""
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        let banlanceLbl = objLang?["LBL_WALLET_BALANCE"] as? String ?? ""
        self.lblBalance.text! = "\(banlanceLbl) \(balance)"
        
        let imageUrlId = "https://apps.wagonstaxi.com/webimages/upload/Driver/"
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let imgUserId:String = UserDefaults.standard.string(forKey: "vImage") ?? ""

        
        let imageUrl = imageUrlId+"\(userId)/"+(imgUserId )
        print(imageUrl)
        DispatchQueue.global(qos: .background).async {
            do
            {
                let url = URL(string: imageUrl)
                if let data = try? Data(contentsOf: url!){
                    DispatchQueue.main.async {
                        self.imgProfile.image = UIImage(data: data)

                    }
                }

            }

        }
    }
    
    func colorMenuButton(){
        
        self.viewBtnMyProfile.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnManageVehicle.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnManageDocument.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnYourTrips.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnBankDetails.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnMyWallet.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnWayBill.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnHeatView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnEmergency.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnRiderFeedback.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnTripStatistics.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewBtnSupport.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

        
    }
    
    
    
    
}


