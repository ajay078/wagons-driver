//
//  TripStatisticsVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 11/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftChart

class TripStatisticsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    var arrListYear = [""]
    var strSelectedYear = ""
    var selectedIndex:Int = 10000

    var arrMonthEarning = [MTripStatistics]()

    // MARK: - Outlets

    
    @IBOutlet weak var lblSelectedYear: UILabel!
    
    @IBOutlet weak var lblTrips: UILabel!
    @IBOutlet weak var lblTotalFare: UILabel!
    @IBOutlet weak var lblTotalDriverPayment: UILabel!
    @IBOutlet weak var lblTotalCommission: UILabel!
    @IBOutlet weak var lblCurrentYear: UILabel!
    
    @IBOutlet weak var viewListYear: UIView!
    @IBOutlet weak var viewListYearIn: UIView!
    @IBOutlet weak var tableViewYear: UITableView!
    
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var lblYearL: UILabel!
    @IBOutlet weak var viewSelectedYear: UIView!
    @IBOutlet weak var viewNumberOfTrips: UIView!
    @IBOutlet weak var viewTotalEarning: UIView!
    @IBOutlet weak var viewTripCount: UIView!
    @IBOutlet weak var viewYearCommission: UIView!
    @IBOutlet weak var lblNumberOfTripsL: UILabel!
    @IBOutlet weak var lblTotalEarningL: UILabel!
    @IBOutlet weak var lblTripCountL: UILabel!
    @IBOutlet weak var lblYearCommissionL: UILabel!
    
    @IBOutlet weak var viewChart: Chart!
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    
    
    var dataChart: [(x: Int, y: Double)] = [(x: 0, y: 0)]
    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.post_listYear()
        self.corner_radius()
        
        viewChart.gridColor = #colorLiteral(red: 1, green: 0.8588235294, blue: 0.2392156863, alpha: 1)
    
        self.dataChart = [
            (x: 0, y: 0),
            (x: 1, y: 0),
            (x: 2, y: 0),
            (x: 3, y: 0),
            (x: 4, y: 0),
            (x: 5, y: 0),
            (x: 6, y: 0),
            (x: 7, y: 0),
            (x: 8, y: 0),
            (x: 9, y: 0),
            (x: 10, y: 0),
            (x: 11, y: 0),
            (x: 12, y: 0)

        ]
        
        let series = ChartSeries(data: dataChart)
        viewChart.add(series)

        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnBackListYear(_ sender: Any) {
        ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: viewListYear, viewPopUP: viewListYearIn)
    }
    
    @IBAction func btnSelectYear(_ sender: Any) {
        
        ValidationManager.sharedManager.showMainViewNN(viewMain: viewListYear, viewInside: viewListYearIn)

    }
    @IBAction func btnHideViewListYear(_ sender: Any) {
        ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: viewListYear, viewPopUP: viewListYearIn)

    }
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_TRIP_STATISTICS_MENU_SCREEN"] as? String ?? ""
        lblYearL.text! = objLang?["LBL_YEAR"] as? String ?? ""
        lblNumberOfTripsL.text! = objLang?["LBL_NUMBER_OF_TRIPS"] as? String ?? ""
        lblTotalEarningL.text! = objLang?["LBL_TOTAL_EARNINGS_STATISTICS"] as? String ?? ""
        lblTripCountL.text! = "Trip Count"
        lblYearL.text! = objLang?["LBL_YEAR"] as? String ?? ""

        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.lblYearL.textAlignment = .right
            self.lblSelectedYear.textAlignment = .right

            self.viewUpar.semanticContentAttribute = .forceRightToLeft
            self.viewSelectedYear.semanticContentAttribute = .forceRightToLeft
            self.viewNumberOfTrips.semanticContentAttribute = .forceRightToLeft
            self.viewTotalEarning.semanticContentAttribute = .forceRightToLeft
            self.viewTripCount.semanticContentAttribute = .forceRightToLeft
            self.viewYearCommission.semanticContentAttribute = .forceRightToLeft

            
        }
        
    }
    
// MARK: - Table View Methods

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListYear.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AddListVehicleCell = tableView.dequeueReusableCell(withIdentifier: "AddListVehicleCell", for: indexPath) as! AddListVehicleCell
        let obj = arrListYear[indexPath.row]
        cell.lblText.text! = obj
        
        
        if selectedIndex == indexPath.row{
            cell.viewCell.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        }else{
            cell.viewCell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }

        let indexOfYear = self.arrListYear.firstIndex(where: {$0 ==  self.strSelectedYear})

        if indexOfYear == indexPath.row{
            cell.viewCell.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        }else{
            cell.viewCell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath.row

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3){
            ValidationManager.sharedManager.removeSubViewWithAnimation(viewMain: self.viewListYear, viewPopUP: self.viewListYearIn)

            let obj = self.arrListYear[indexPath.row]
            self.lblSelectedYear.text! = obj
            self.strSelectedYear = obj
            self.post_listYear()
        }


    }
    
    

    
    func corner_radius(){
        self.viewListYear.isHidden = true
        self.viewListYearIn.layer.cornerRadius = 5
        self.viewListYearIn.layer.masksToBounds = true
        
        self.viewBottom.layer.cornerRadius = 5
        self.viewBottom.layer.masksToBounds = true
    }
    
    func details(){
        
        
        
        
    }
    
    
}

// MARK: - Webservice calling

extension TripStatisticsVC{
    
    func post_listYear(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        ValidationManager.sharedManager.showLoader()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&year=\(self.strSelectedYear)&vTimeZone=Asia %2FCalcutta&type=getYearTotalEarnings&UserType=Driver&GeneralUserType=Driver&tSessionId=\(sessionId)&iDriverId=\(userId)&GeneralDeviceType=ios"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let action = json["Action"].stringValue
                if action == "1"{
                    
                    self.lblSelectedYear.text! = json["CurrentYear"].stringValue
                    self.lblTrips.text! = json["TripCount"].stringValue
                    self.lblTotalFare.text! = json["YearTotalOriginal"].stringValue
                    self.lblTotalCommission.text! = json["YearCommision"].stringValue
                    self.strSelectedYear = json["CurrentYear"].stringValue
                    
                    if let listYear = json["YearArr"].arrayObject{
                        self.arrListYear.removeAll()
                        for index in 0..<listYear.count{
                            let obj = listYear[index]
                            self.arrListYear.append(obj as? String ?? "")
                        }
                        
                        

                    }
                    
                    if let monthEarning = json["YearMonthArr"].arrayObject{
                        for index in 0..<monthEarning.count{
                            let obj = monthEarning[index]
                            let modalObj = MTripStatistics.init(dict: obj as? [String : Any] ?? [:])
                            self.arrMonthEarning.append(modalObj)
                            
                            let y = modalObj.TotalEarnings
                            if let yd = Double(y){
                                let x = (x:index+1, y:yd)
                                self.dataChart.append(x)
                            }
                            
                            
                            let month = modalObj.CurrentMonth
                            print(month)
                            
                            let series = ChartSeries(data: self.dataChart)
                            self.viewChart.add(series)
                        }
                    }
                }
                
                
                
                
                
                
                self.tableViewYear.reloadData()
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
    
}
