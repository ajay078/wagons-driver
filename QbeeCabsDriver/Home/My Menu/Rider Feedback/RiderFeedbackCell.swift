//
//  RiderFeedbackCell.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 09/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class RiderFeedbackCell: UITableViewCell {

    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewCell: UIView!
    
    
    @IBOutlet weak var imgRating1: UIImageView!
    @IBOutlet weak var imgRating2: UIImageView!
    @IBOutlet weak var imgRating3: UIImageView!
    @IBOutlet weak var imgRating4: UIImageView!
    @IBOutlet weak var imgRating5: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setLanguageLabel()
        self.corner_radius()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

    func corner_radius(){
        imgProfile.layer.cornerRadius = imgProfile.frame.size.height/2
        viewCell.layer.cornerRadius = 5
        viewCell.layer.masksToBounds = true
        
    }
    
    
    func get_rating(rating:Int){
        
        switch rating {
        case 1:
            self.imgRating1.image = UIImage(named: "star yellow")
        case 2:
            self.imgRating1.image = UIImage(named: "star yellow")
            self.imgRating2.image = UIImage(named: "star yellow")
            
        case 3:
            self.imgRating1.image = UIImage(named: "star yellow")
            self.imgRating2.image = UIImage(named: "star yellow")
            self.imgRating3.image = UIImage(named: "star yellow")
            
            
        case 4:
            self.imgRating1.image = UIImage(named: "star yellow")
            self.imgRating2.image = UIImage(named: "star yellow")
            self.imgRating3.image = UIImage(named: "star yellow")
            self.imgRating4.image = UIImage(named: "star yellow")
            
            
        case 5:
            self.imgRating1.image = UIImage(named: "star yellow")
            self.imgRating2.image = UIImage(named: "star yellow")
            self.imgRating3.image = UIImage(named: "star yellow")
            self.imgRating4.image = UIImage(named: "star yellow")
            self.imgRating5.image = UIImage(named: "star yellow")
            
            
        default:
            break
        }
        
    }
    
    // MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")


        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewCell.semanticContentAttribute = .forceRightToLeft

            
        }
        
    }
    
}
