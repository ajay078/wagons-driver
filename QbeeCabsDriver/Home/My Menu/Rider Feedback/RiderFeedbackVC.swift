//
//  RiderFeedbackVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 09/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RiderFeedbackVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var arrFeedback = [MRiderFeedback]()
    var strTripRating:String = ""
    var page:Int = 1
    

    // MARK: - Web service variable

    let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    
    //MARK:- Outlets
    
    @IBOutlet weak var tableViewRider: UITableView!
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.post_riderFeedback()
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
// MARK: - Table View Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFeedback.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RiderFeedbackCell = tableView.dequeueReusableCell(withIdentifier: "RiderFeedbackCell", for: indexPath) as! RiderFeedbackCell
        cell.lblName.text! = arrFeedback[indexPath.row].vName
        cell.lblDate.text! = arrFeedback[indexPath.row].tDate
        
        let imageUrl = arrFeedback[indexPath.row].vImage
        
        DispatchQueue.global(qos: .background).async {
            
            let url = URL(string: imageUrl)
            if let data = try? Data(contentsOf: url!){
                DispatchQueue.main.async {
                    cell.imgProfile.image = UIImage(data: data)
                }
            }
        }
        
        self.strTripRating = arrFeedback[indexPath.row].vRating1
        
        let doubleRating = Double(strTripRating) ?? 0.0
        let IntRating:Int = Int(round(doubleRating))
        
        cell.get_rating(rating: IntRating)
        
        

        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == arrFeedback.count-1 { //you might decide to load sooner than -1 I guess...
          //load more into data here
            
            
            self.page = self.page + 1
                
            self.post_riderFeedback()
            
        }
    }
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_RIDER_FEEDBACK_MENU_SCREEN"] as? String ?? ""


        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft

            
        }
        
    }


}

// MARK: - Webservice calling

extension RiderFeedbackVC{
    
    func post_riderFeedback(){
        
        ValidationManager.sharedManager.showLoader()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry\(country)=&vTimeZone=Asia/Kolkata&type=loadDriverFeedBack&GeneralUserType=Driver&tSessionId=\(sessionId)&iDriverId=\(userId)&GeneralDeviceType=Android"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kLoadDriverFeedBack),
                          URLQueryItem(name: "GeneralMemberId", value: self.userId),
                          URLQueryItem(name: "iDriverId", value: self.userId),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone),
                          URLQueryItem(name: "page", value: String(self.page))]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                    if let message = json["message"].arrayObject{
                        for index in 0..<message.count{
                            let obj = message[index]
                            let modalObj = MRiderFeedback.init(dict: obj as? [String : Any] ?? [:])
                            self.arrFeedback.append(modalObj)
                        }
                    }
                    
                    self.tableViewRider.reloadData()
                }else{
                    ValidationManager.sharedManager.hideLoader()
                    
                }
                

                
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
}
