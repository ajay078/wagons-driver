//
//  FAQVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 27/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FAQVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    var arrModalFaq = [ModalFaq]()
    var arrModalTripHelp = [ModalTripHelp]()

    var isFromYourTrips:Bool = false
    var uniqueId:String = ""
    var strTripId:String = ""
    
    @IBOutlet weak var viewUpar: UIView!

    @IBOutlet weak var tableViewFaq: UITableView!
    @IBOutlet weak var lblUpar: UILabel!
    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.post_query()
        self.lblUpar.text! = "FAQ"
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFromYourTrips == true{
            return arrModalTripHelp.count
        }else{
            return arrModalFaq.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFromYourTrips == true{
            let cell:FAQCell = tableView.dequeueReusableCell(withIdentifier: "FAQCell", for: indexPath) as! FAQCell

            let obj = arrModalTripHelp[indexPath.row]
            cell.lblName.text! = obj.vTitle
            
            
            return cell
        }else{
            let cell:FAQCell = tableView.dequeueReusableCell(withIdentifier: "FAQCell", for: indexPath) as! FAQCell

            let obj = arrModalFaq[indexPath.row]
            cell.lblName.text! = obj.strTitle
            
            return cell
        }
        
        

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isFromYourTrips == true{
            let vc:FaqDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "FaqDetailVC") as! FaqDetailVC
            vc.uniqueId = arrModalTripHelp[indexPath.row].iUniqueId
            vc.isFromYourTrips = true
            vc.strTripId = self.strTripId
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc:FaqDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "FaqDetailVC") as! FaqDetailVC
            vc.objDetail = arrModalFaq[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        

        
    }
    
    

    
    
    // MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        
        if isFromYourTrips == true{
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblUpar.text! = objLang?["LBL_HELP_TXT"] as? String ?? ""
            
        }else{
            self.post_query()
            let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
            lblUpar.text! = objLang?["LBL_FAQ_TEXT"] as? String ?? ""

        }
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft

            
        }
        

    }
    
    
}

// MARK: - Webservice calling

extension FAQVC{
    
    func post_query(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        ValidationManager.sharedManager.showLoader()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&appType=Driver&iMemberId=\(userId)&vTimeZone=Asia %2FCalcutta&type=getFAQ&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let ActionLogIn = json["Action"].stringValue
                print("action is \(ActionLogIn)")
                
                if let message = json["message"].arrayObject{
                    print(message)
                    self.arrModalFaq.removeAll()
                    for index in 0..<message.count{
                        let obj = message[index]
                        let modalObj = ModalFaq.init(dict: obj as? [String : Any] ?? [:])
                        self.arrModalFaq.append(modalObj)
                    }
                    
                    self.tableViewFaq.reloadData()
                    ValidationManager.sharedManager.hideLoader()

                }
                

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
}
