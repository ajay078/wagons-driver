//
//  FaqDetailCell.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 27/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class FaqDetailCell: UITableViewCell {
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var textAnswer: UITextView!
    @IBOutlet var viewCellAnswer: UIView!
    
    @IBOutlet weak var heightTextAnswer: NSLayoutConstraint!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setLanguageLabel()
        heightTextAnswer.constant = 0
        viewCell.layer.borderWidth = 0.5
        viewCell.layer.borderColor = UIColor.lightGray.cgColor
        viewCell.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    @IBAction func btnQuestion(_ sender: Any) {
        heightTextAnswer.constant = 300
    }
    
// MARK: - set Language Labels

    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        //        lblHeader.text! = objLang?["LBL_SUPPORT_HEADER_TXT"] as? String ?? ""
        
        
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewCell.semanticContentAttribute = .forceRightToLeft
            self.textAnswer.textAlignment = .right
            
        }
        
        
        
    }
}
