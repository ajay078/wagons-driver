//
//  FaqDetailVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 27/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class FaqDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var arrModalTripHelpDetail = [ModalTripHelpDetails]()

    var objDetail = ModalFaq(dict: [:])
    var isFromYourTrips:Bool = false
    var uniqueId:String = ""
    var strTripId:String = ""
    
    
    @IBOutlet weak var tableViewFaqDetail: UITableView!
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        if isFromYourTrips == true{
            post_tripHelpDetail()
        }
        
    }
  
    override func viewDidAppear(_ animated: Bool) {
        
        tableViewFaqDetail.estimatedRowHeight = 80
        tableViewFaqDetail.rowHeight = UITableView.automaticDimension
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFromYourTrips == true{
            return arrModalTripHelpDetail.count
        }else{
            return objDetail.arrQuestions.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isFromYourTrips == true{
            let cell:FaqDetailCell = tableView.dequeueReusableCell(withIdentifier: "FaqDetailCell", for: indexPath) as! FaqDetailCell
            
            let obj = arrModalTripHelpDetail[indexPath.row]
            cell.lblQuestion.text! = obj.vTitle
            cell.textAnswer.text! = obj.tAnswer
            
            return cell
        }else{
            let cell:FaqDetailCell = tableView.dequeueReusableCell(withIdentifier: "FaqDetailCell", for: indexPath) as! FaqDetailCell
            
            let obj = objDetail.arrQuestions[indexPath.row]
            cell.lblQuestion.text! = obj.strTitle
            cell.textAnswer.text! = obj.strAnswer
            
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3){
            let cell = tableView.cellForRow(at: indexPath)
                if (cell?.isKind(of: FaqDetailCell.self))!{
                    let selectedCell  = cell as? FaqDetailCell
                    selectedCell?.heightTextAnswer.constant = selectedCell?.textAnswer.contentSize.height ?? 100
                    
            }
            
            self.tableViewFaqDetail.reloadData()
        }
        


    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        

        return UITableView.automaticDimension


    }

    // MARK: - set Language Labels
     
     func setLanguageLabel(){
         
         
         let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
         lblHeader.text! = objLang?["LBL_FAQ_TEXT"] as? String ?? ""
         
         let langCode = UserDefaults.standard.string(forKey: "default_language")
         if langCode == "AR"{
             
             self.viewUpar.semanticContentAttribute = .forceRightToLeft

             
         }
         

     }
    
    
    func post_tripHelpDetail(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        ValidationManager.sharedManager.showLoader()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&appType=Driver&iMemberId=\(userId)&vTimeZone=Asia%2FCalcutta&iUniqueId=\(self.uniqueId)&type=getsubHelpdetail&GeneralUserType=Passenger&tSessionId=\(sessionId)&GeneralDeviceType=Android"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                
                let ActionLogIn = json["Action"].stringValue
                print("action is \(ActionLogIn)")
                
                if let message = json["message"].arrayObject{
                    print(message)
                    
                    for index in 0..<message.count{
                        let obj = message[index]
                        let modalObj = ModalTripHelpDetails.init(dict: obj as? [String : Any] ?? [:])
                        self.arrModalTripHelpDetail.append(modalObj)
                    }
                    
                    self.tableViewFaqDetail.reloadData()
                    ValidationManager.sharedManager.hideLoader()
                }
                
                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
}
