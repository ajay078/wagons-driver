//
//  ContactUsVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 26/05/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ContactUsVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    
    
    
    // MARK: - Web service variable

    let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    //MARK:- Outlets
    
    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var lblQuery: UILabel!
    
    @IBOutlet weak var txtReason: UITextField!
    @IBOutlet weak var textQuery: UITextView!
    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewMessageIn: UIView!
    
    @IBOutlet weak var viewReasonRIn: UIView!
    @IBOutlet weak var viewReasonR: UIView!
    
    @IBOutlet weak var viewQueryRIn: UIView!
    @IBOutlet weak var viewQueryR: UIView!
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblRequiredReason: UILabel!
    @IBOutlet weak var lblRequiredQuery: UILabel!

    
// MARK: - View Did Load

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.corner_radius()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        self.emtpy_textField()
        
    }
    @IBAction func btnSubmitOk(_ sender: Any) {
        self.viewMessage.isHidden = true
        self.txtReason.text! = ""
        self.textQuery.text! = ""
        
    }
 
    func post_sendQuery(){

        ValidationManager.sharedManager.showLoader()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&subject=\(txtReason.text!)&type=sendContactQuery&message=\(textQuery.text!)&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android&GeneralMemberId=\(userId)&vUserDeviceCountry=\(country)&UserId=\(userId)&vTimeZone=Asia/Kolkata&UserType=Driver"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "UserType", value: kUserType),
                          URLQueryItem(name: "type", value: kSendContactQuery),
                          URLQueryItem(name: "GeneralMemberId", value: self.userId),
                          URLQueryItem(name: "UserId", value: self.userId),
                          URLQueryItem(name: "vFirebaseDeviceToken", value: kDeviceToken),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone),
                          URLQueryItem(name: "subject", value: self.txtReason.text!),
                          URLQueryItem(name: "message", value: self.textQuery.text!)]

        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                    self.viewMessage.isHidden = false
                }

                ValidationManager.sharedManager.hideLoader()

            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    func emtpy_textField(){
        
        if txtReason.text!.isEmpty{
            self.viewReasonRIn.backgroundColor = UIColor.red
            viewReasonR.isHidden = false
        }else{
            self.post_sendQuery()

        }
        
        
    }
    
// MARK: - set Language Labels

func setLanguageLabel(){
    
    let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
    lblHeader.text! = objLang?["LBL_CONTACT_US_HEADER_TXT"] as? String ?? ""
    lblReason.text! = objLang?["LBL_RES_TO_CONTACT"] as? String ?? ""
    txtReason.placeholder = objLang?["LBL_ADD_SUBJECT_HINT_CONTACT_TXT"] as? String ?? ""
    lblQuery.text! = objLang?["LBL_YOUR_QUERY"] as? String ?? ""
    textQuery.text! = objLang?["LBL_RIDER_CONTACT_US_WRITE_EMAIL_CONTACT_US"] as? String ?? ""
    btnSubmit.setTitle(objLang?["LBL_SUBMIT"] as? String ?? "", for: .normal)
    lblRequiredReason.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""
    lblRequiredQuery.text! = objLang?["LBL_FEILD_REQUIRD_ERROR_TXT"] as? String ?? ""


    let langCode = UserDefaults.standard.string(forKey: "default_language")
    if langCode == "AR"{
        
        self.viewUpar.semanticContentAttribute = .forceRightToLeft
        self.lblReason.textAlignment = .right
        self.lblQuery.textAlignment = .right
        self.txtReason.textAlignment = .right
        self.textQuery.textAlignment = .right
        self.lblRequiredReason.textAlignment = .right
        self.lblRequiredQuery.textAlignment = .right


        
    }
    
}

    func corner_radius(){
        
        self.txtReason.delegate = self
        self.textQuery.delegate = self
        self.viewMessage.isHidden = true
        viewMessageIn.layer.cornerRadius = 5
        viewMessageIn.layer.masksToBounds = true
        viewReasonR.isHidden = true
        viewQueryR.isHidden = true

    }

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtReason{
            self.lblQuery.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.lblReason.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

            self.lblReason.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
        
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if textView == textQuery{
            self.lblQuery.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.lblReason.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

            self.lblQuery.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            if self.textQuery.text! == "Write your query here"{
                self.textQuery.text! = ""
            }
        }
        return true
    }
    
    

}
