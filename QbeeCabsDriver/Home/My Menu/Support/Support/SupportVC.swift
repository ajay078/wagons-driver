//
//  SupportVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 14/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SupportVC: UIViewController {

    var isFromRegister:Bool = false
    
    @IBOutlet weak var viewAboutUs: UIView!
    @IBOutlet weak var viewPrivacy: UIView!
    @IBOutlet weak var viewTerms: UIView!
    @IBOutlet weak var viewContactUs: UIView!
    @IBOutlet weak var viewFaq: UIView!
    
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    
    @IBOutlet weak var heightAboutUs: NSLayoutConstraint!
    
    @IBOutlet weak var lblAboutUs: UILabel!
    @IBOutlet weak var lblPrivacyPolicy: UILabel!
    @IBOutlet weak var lblTermsAndConditions: UILabel!
    @IBOutlet weak var lblContactUs: UILabel!
    @IBOutlet weak var lblFaq: UILabel!
    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        if isFromRegister == true{
            self.heightAboutUs.constant = 0
            self.viewAboutUs.isHidden = true
            self.viewContactUs.isHidden = true
            self.viewFaq.isHidden = true
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }


    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAboutUs(_ sender: Any) {
        self.colorSupportButton()
        self.viewAboutUs.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:AboutUsVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
        vc.isFromAboutUs = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnPrivacy(_ sender: Any) {
        self.colorSupportButton()
        self.viewPrivacy.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:AboutUsVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
        vc.isFromPrivacy = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnTerms(_ sender: Any) {
        self.colorSupportButton()
        self.viewTerms.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:AboutUsVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
        vc.isFromTerms = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnContact(_ sender: Any) {
        self.colorSupportButton()
        self.viewContactUs.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:ContactUsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnFAQ(_ sender: Any) {
        self.colorSupportButton()
        self.viewFaq.layer.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9019607843, blue: 0.168627451, alpha: 1)
        let vc:FAQVC = self.storyboard?.instantiateViewController(withIdentifier: "FAQVC") as! FAQVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func colorSupportButton(){
        
        self.viewAboutUs.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewPrivacy.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewTerms.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewContactUs.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.viewFaq.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    // MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_SUPPORT_HEADER_TXT"] as? String ?? ""
        lblAboutUs.text! = objLang?["LBL_ABOUT_US_SUPPORT"] as? String ?? ""
        lblPrivacyPolicy.text! = objLang?["LBL_PRIVACY_POLICY_SUPPORT"] as? String ?? ""
        lblTermsAndConditions.text! = objLang?["LBL_TERMS_AND_CONDITION_STATIC_PAGE"] as? String ?? ""
        lblContactUs.text! = objLang?["LBL_CONTACT_US_MENU_SCREEN"] as? String ?? ""
        lblFaq.text! = objLang?["LBL_FAQ_TXT"] as? String ?? ""
        
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft
            self.viewAboutUs.semanticContentAttribute = .forceRightToLeft
            self.viewPrivacy.semanticContentAttribute = .forceRightToLeft
            self.viewTerms.semanticContentAttribute = .forceRightToLeft
            self.viewContactUs.semanticContentAttribute = .forceRightToLeft
            self.viewFaq.semanticContentAttribute = .forceRightToLeft
            
            self.lblAboutUs.textAlignment = .right
            self.lblPrivacyPolicy.textAlignment = .right
            self.lblTermsAndConditions.textAlignment = .right
            self.lblContactUs.textAlignment = .right
            self.lblFaq.textAlignment = .right
            
        }
        
        
        
    }
    
    
}
