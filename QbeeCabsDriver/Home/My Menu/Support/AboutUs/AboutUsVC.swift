//
//  AboutUsVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 25/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class AboutUsVC: UIViewController {

    var isFromAboutUs:Bool = false
    var isFromPrivacy:Bool = false
    var isFromTerms:Bool = false

    // MARK: - Web service variable

    let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    //MARK: - Outlets
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var viewUpar: UIView!

    
    //MARK: - View Did Load
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")

        textView.isEditable = false

        if isFromAboutUs == true{
            lblTitle.text! = objLang?["LBL_RIDER_ABOUT_US_SUPPORT"] as? String ?? ""
            self.aboutUs()
        }else if isFromPrivacy == true{
            lblTitle.text! = objLang?["LBL_RIDER_PRIVACY_POLICY_SUPPORT"] as? String ?? ""
            self.privacy_policy()
        }else if isFromTerms == true{
            lblTitle.text! = objLang?["LBL_TERMS_AND_CONDITION"] as? String ?? ""
            self.Terms()
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    

    
    

    
    

    
    // MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblTitle.text! = objLang?["LBL_ABOUT_US_TXT"] as? String ?? ""

        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft

            
        }
        

    }

}


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


//MARK:- Web service calling

extension AboutUsVC{
    
    func aboutUs(){
        
        ValidationManager.sharedManager.showLoader()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralAppVersion=1.0.4&vUserDeviceCountry\(country)=&appType=Driver&iMemberId=\(userId)&vTimeZone=Asia %2FCalcutta&iPageId=1&type=staticPage&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "type", value: kStaticPage),
                          URLQueryItem(name: "iMemberId", value: self.userId),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "appType", value: "Passenger"),
                          URLQueryItem(name: "iPageId", value: "1"),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone)]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                let htmlText = json["page_desc"].stringValue
                
                //                    self.textView.attributedText = htmlText.htmlToAttributedString
                let attributedString = htmlText.htmlToAttributedString
                let stringNew = attributedString?.string
                ValidationManager.sharedManager.hideLoader()
                //                    self.textView.font = UIFont(name: "System", size: 20)
                
                let stringWithAttribute = NSAttributedString(string: stringNew ?? "",
                                                             attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.0)])
                
                self.textView.attributedText = stringWithAttribute
                
                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
    func privacy_policy(){

        
        ValidationManager.sharedManager.showLoader()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&appType=Driver&iMemberId=\(userId)&vTime Zone=Asia %2FCalcutta&iPageId=33&type=staticPage&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "type", value: kStaticPage),
                          URLQueryItem(name: "iMemberId", value: self.userId),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "appType", value: "Passenger"),
                          URLQueryItem(name: "iPageId", value: "33"),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone)]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let htmlText = json["page_desc"].stringValue
                
                //                    self.textView.attributedText = htmlText.htmlToAttributedString
                let attributedString = htmlText.htmlToAttributedString
                let stringNew = attributedString?.string
                let stringWithAttribute = NSAttributedString(string: stringNew ?? "",
                                                             attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.0)])
                self.textView.attributedText = stringWithAttribute
                
                
                
                
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
    func Terms(){
        
        ValidationManager.sharedManager.showLoader()
//        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&appType=Driver&iMemberId=\(userId)&vTime Zone=Asia %2FCalcutta&iPageId=4&type=staticPage&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=Android"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!

        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "type", value: kStaticPage),
                          URLQueryItem(name: "iMemberId", value: self.userId),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "appType", value: "Passenger"),
                          URLQueryItem(name: "iPageId", value: "4"),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone)]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                let htmlText = json["page_desc"].stringValue
                
                //                    self.textView.attributedText = htmlText.htmlToAttributedString
                let attributedString = htmlText.htmlToAttributedString
                let stringNew = attributedString?.string
                let stringWithAttribute = NSAttributedString(string: stringNew ?? "",
                                                             attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.0)])
                self.textView.attributedText = stringWithAttribute
                
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
}
