//
//  TransactionsCell.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 15/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class TransactionsCell: UITableViewCell {

   
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDebited: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    
    @IBOutlet weak var viewCell: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.corner_radius()
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func corner_radius(){
        viewCell.layer.borderWidth = 1
        viewCell.layer.borderColor = UIColor.lightGray.cgColor
        viewCell.layer.cornerRadius = 5

    }
}
