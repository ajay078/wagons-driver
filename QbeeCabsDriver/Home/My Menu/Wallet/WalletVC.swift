//
//  WalletVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 14/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Stripe
import Alamofire
import SwiftyJSON

class WalletVC: UIViewController {

    
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var txtRechargeAmount: UITextField!
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var lblYourBalance: UILabel!
    @IBOutlet weak var btnViewTransactions: UIButton!
    @IBOutlet weak var lblAddMoney: UILabel!
    @IBOutlet weak var lblItsSafe: UILabel!
    @IBOutlet weak var lblByConducting: UILabel!
    @IBOutlet weak var btnTermsAndConditions: UIButton!
    @IBOutlet weak var btnAddMoney: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }

// MARK: - Button Actions

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnViewTransations(_ sender: Any) {

        let vc:ViewTranscationsVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewTranscationsVC") as! ViewTranscationsVC
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnTerms(_ sender: Any) {
        self.view.endEditing(true)
        let vc:AboutUsVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
        vc.isFromTerms = true
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnAddMoney(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CardDetailsVC") as! CardDetailsVC
        vc.amount = self.txtRechargeAmount.text!
        vc.isFromAddMoney = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_RIDER_WALLET"] as? String ?? ""
        lblYourBalance.text! = objLang?["LBL_USER_BALANCE"] as? String ?? ""
        lblAddMoney.text! = objLang?["LBL_ADD_MONEY_TXT"] as? String ?? ""
        lblItsSafe.text! = objLang?["LBL_RIDER_ADD_MONEY_1_MANAGE_WALLET"] as? String ?? ""
        txtRechargeAmount.placeholder = objLang?["LBL_RECHARGE_AMOUNT_TXT"] as? String ?? ""
        lblByConducting.text! = objLang?["LBL_PRIVACY_POLICY"] as? String ?? ""
        btnViewTransactions.setTitle(objLang?["LBL_VIEW_TRANS_HISTORY"] as? String ?? "", for: .normal)
        btnTermsAndConditions.setTitle(objLang?["LBL_PRIVACY_POLICY1"] as? String ?? "", for: .normal)
        btnAddMoney.setTitle(objLang?["LBL_ADD_MONEY_MANAGE_WALLET"] as? String ?? "", for: .normal)
        
        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft
            lblAddMoney.textAlignment = .right
            lblItsSafe.textAlignment = .right
            txtRechargeAmount.textAlignment = .right

            
        }else{

        }
        
        
        
    }
    
    
}


