//
//  CardDetailsCell.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 29/07/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

class CardDetailsCell: UITableViewCell {

    
    @IBOutlet weak var imgDefault: UIImageView!
    @IBOutlet weak var lblCardNumber: UILabel!
    
    @IBOutlet weak var btnMakeDefault: UIButton!
    @IBOutlet weak var btnDeleteCard: UIButton!
    
    @IBOutlet weak var viewCell: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewCell.layer.cornerRadius = 5
        self.viewCell.layer.masksToBounds = true
        self.viewCell.layer.borderWidth = 1
        self.viewCell.layer.borderColor = UIColor.darkGray.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
