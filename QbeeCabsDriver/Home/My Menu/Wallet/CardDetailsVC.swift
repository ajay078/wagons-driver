//
//  CardDetailsVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 23/07/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Stripe
import NVActivityIndicatorView



class CardDetailsVC: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, STPAddCardViewControllerDelegate, STPCustomerEphemeralKeyProvider {

    
    
    var window: UIWindow?
    var navigationcontroller:UINavigationController?
    var currentPaymentMethod: STPPaymentMethod?

    var arrListCard = [ModalCardDetails]()
    var iCardUid:String = ""
    var strTripId:String = ""
    var amount:String = ""
    var delegatePayment:SendDataBackDelegate!
    var isOnlinePayed:Bool = false
    var isFromAddMoney:Bool = false
    var strCardNumber = ""
    var strMonth = ""
    var strYear = ""
    
    var isMoneyAdded:Bool = false
    
    @objc let SupportedPaymentNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex]

    @IBOutlet weak var cardNumberTextField:UITextField!
    @IBOutlet weak var expirationMonthTextField:UITextField!
    @IBOutlet weak var expirationYearTextField:UITextField!
    @IBOutlet weak var cardVerificationCodeTextField:UITextField!
    @IBOutlet weak var getTokenButton:UIButton!
    
    @IBOutlet weak var txtCardNumber:UITextField!
    @IBOutlet weak var txtMonth:UITextField!
    @IBOutlet weak var txtYear:UITextField!
    @IBOutlet weak var txtCvv:UITextField!
    
//    @IBOutlet weak var activityIndicatorAcceptSDKDemo:UIActivityIndicatorView!
    @IBOutlet weak var textViewShowResults:UITextView!
    @IBOutlet weak var headerView:UIView!

    
    @IBOutlet weak var viewTxtCardNumber: UIView!
    @IBOutlet weak var viewTxtExpMonth: UIView!
    @IBOutlet weak var viewTxtExpYear: UIView!
    @IBOutlet weak var viewTxtSecurityCode: UIView!
    
    @IBOutlet weak var viewBtnPay: UIView!
    @IBOutlet weak var lblAmount: UILabel!

    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewMessageConfirm: UIView!
    @IBOutlet weak var viewMessageUpdated: UIView!
    @IBOutlet weak var lblMessageUpdated: UILabel!
    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
    @IBOutlet weak var tableViewCard: UITableView!
    
    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.corner_radius()
        self.post_listCard()
        
//       self.setUIControlsTagValues()
//       self.initializeUIControls()
//       self.initializeMembers()
//
//       self.updateTokenButton(false)
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }



// MARK: - Button Actions

    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)

        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnPay(_ sender: Any) {
        
        strCardNumber = "4242424242424242"
        strMonth = "11"
        strYear = "2022"
        
        if isFromAddMoney == true{
            self.post_addMoney()
        }else{
        }
        
    }

    
    @IBAction func btnMakeDefault(_ sender: UIButton) {
        let obj = arrListCard[sender.tag]
        self.iCardUid = obj.iCardUid
        self.post_CardMakeDefault()
        
    }
    
    @IBAction func btnDeleteCard(_ sender: UIButton) {
        let obj = arrListCard[sender.tag]
        self.iCardUid = obj.iCardUid
        self.viewMessage.isHidden = false
        self.viewMessageConfirm.isHidden = false
    }
    
    
    @IBAction func btnMessageOk(_ sender: UIButton) {
        self.post_deleteCard()
    }
    @IBAction func btnMessageCancel(_ sender: Any) {
        self.viewMessage.isHidden = true

    }
    @IBAction func btnMessageUpdatedOk(_ sender: Any) {
        
        if isMoneyAdded == true{
            self.navigationController?.popViewController(animated: false)
            
//            let sb: UIStoryboard = UIStoryboard(name: "HomeSB", bundle:Bundle.main)
//            navigationcontroller = sb.instantiateViewController(withIdentifier: "navHome") as? UINavigationController
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.window?.rootViewController = navigationcontroller
//            window?.makeKeyAndVisible()
        }
        
        self.viewMessage.isHidden = true
        
        

    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListCard.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CardDetailsCell = tableView.dequeueReusableCell(withIdentifier: "CardDetailsCell", for: indexPath) as! CardDetailsCell
        let obj = arrListCard[indexPath.row]
        cell.lblCardNumber.text! = obj.iCardNumber
        
        cell.btnMakeDefault.tag = indexPath.row
        cell.btnDeleteCard.tag = indexPath.row
        
        if obj.iDefault == "1"{
            cell.imgDefault.image = UIImage(named: "correct")
            cell.viewCell.layer.borderColor = #colorLiteral(red: 0.168627451, green: 0.3529411765, blue: 0.8588235294, alpha: 1)
            cell.viewCell.layer.borderWidth = 2

        }else{
            cell.imgDefault.image = UIImage(named: "circle")
            cell.viewCell.layer.borderColor = UIColor.darkGray.cgColor
            cell.viewCell.layer.borderWidth = 1

        }
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = arrListCard[indexPath.row]
        self.txtCardNumber.text! = obj.iCardNumber
        self.txtMonth.text! = obj.iMM
        self.txtYear.text! = obj.iYY
        
        
        
    }

    
    func corner_radius(){
        
        self.viewMessage.isHidden = true
        self.viewMessageUpdated.isHidden = true
        self.viewTxtCardNumber.layer.borderWidth = 0.5
        self.viewTxtCardNumber.layer.borderColor = UIColor.darkGray.cgColor
        
        self.viewTxtExpMonth.layer.borderWidth = 0.5
        self.viewTxtExpMonth.layer.borderColor = UIColor.darkGray.cgColor
        self.viewTxtExpYear.layer.borderWidth = 0.5
        self.viewTxtExpYear.layer.borderColor = UIColor.darkGray.cgColor
        self.viewTxtSecurityCode.layer.borderWidth = 0.5
        self.viewTxtSecurityCode.layer.borderColor = UIColor.darkGray.cgColor
        
        self.viewMessageConfirm.layer.cornerRadius = 5
        self.viewMessageUpdated.layer.cornerRadius = 5
        
        
        self.lblAmount.text! = amount
        
        self.viewMessage.isHidden = true
        
        
        
    }
    
    func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        
    }
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        dismiss(animated: true)
        
        
        
    }
    
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreatePaymentMethod paymentMethod: STPPaymentMethod, completion: @escaping STPErrorBlock) {
        
        currentPaymentMethod = paymentMethod
        
        print(paymentMethod.card!)
        print(paymentMethod.stripeId)
        print(paymentMethod.customerId!)
        print(paymentMethod.allResponseFields)
        
        
        
        
        //        dismiss(animated: true)
        
    }
    
    
}

extension CardDetailsVC{
    
    
    @IBAction func btnApplePay(_ sender: Any) {
        
        
//        let supportedNetworks = [ PKPaymentNetwork.amex, PKPaymentNetwork.masterCard, PKPaymentNetwork.visa ]
//
//        if PKPaymentAuthorizationViewController.canMakePayments() == false {
//            let alert = UIAlertController(title: "Apple Pay is not available", message: nil, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            return self.present(alert, animated: true, completion: nil)
//        }
//
//        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: supportedNetworks) == false {
//            let alert = UIAlertController(title: "No Apple Pay payment methods available", message: nil, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            return self.present(alert, animated: true, completion: nil)
//        }
//
//        let request = PKPaymentRequest()
//        request.currencyCode = "USD"
//        request.countryCode = "US"
//        request.merchantIdentifier = "merchant.nextday.Software.QbeeCabs-Passenger"
//        request.supportedNetworks = SupportedPaymentNetworks
//        // DO NOT INCLUDE PKMerchantCapability.capabilityEMV
//        request.merchantCapabilities = PKMerchantCapability.capability3DS
//
//        request.paymentSummaryItems = [
//            PKPaymentSummaryItem(label: "Total", amount: 254.00)
//        ]
//
//        let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
//        applePayController?.delegate = self as! PKPaymentAuthorizationViewControllerDelegate
//
//        self.present(applePayController!, animated: true, completion: nil)
        
    }
    
    
    

    
    

    
    
}

// MARK: - Webservice calling

extension CardDetailsVC{
    
    
    func post_saveCard(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        self.activityIndicatorView.startAnimating()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=SaveCard&UserType=Driver&GeneralUserType=Passenger&tSessionId=\(sessionId)&iUserId=\(userId)&GeneralDeviceType=Android&CardNo=\(txtCardNumber.text!)&mm=\(txtMonth.text!)&yy=\(txtYear.text!)"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                    self.post_listCard()
                    self.viewMessage.isHidden = false
                    self.viewMessageConfirm.isHidden = true
                    self.viewMessageUpdated.isHidden = false
                }
                

                self.activityIndicatorView.stopAnimating()

                
            case .failure(let error):
                print(error)
                self.activityIndicatorView.stopAnimating()
            }
        }
        
    }
    
    

    func post_listCard(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        self.activityIndicatorView.startAnimating()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=ListCards&UserType=Driver&GeneralUserType=Passenger&tSessionId=\(sessionId)&iUserId=\(userId)&GeneralDeviceType=ios"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                    self.arrListCard.removeAll()
                    if let cards = json["cards"].arrayObject{
                        for index in 0..<cards.count{
                            let obj = cards[index]
                            let modalObj = ModalCardDetails.init(dict: obj as? [String : Any] ?? [:])
                            self.arrListCard.append(modalObj)
                        }
                    }
                    
                    self.tableViewCard.reloadData()

                }
                
                
                
                self.activityIndicatorView.stopAnimating()

                
            case .failure(let error):
                print(error)
                self.activityIndicatorView.stopAnimating()
            }
        }
        
    }
    
    func post_CardMakeDefault(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        self.activityIndicatorView.startAnimating()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=ListCards&UserType=Driver&GeneralUserType=Passenger&tSessionId=\(sessionId)&iUserId=\(userId)&GeneralDeviceType=Android"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                    self.post_listCard()
                }
                
                
                
                self.activityIndicatorView.stopAnimating()

                
            case .failure(let error):
                print(error)
                self.activityIndicatorView.stopAnimating()
            }
        }
        
    }
    
    func post_deleteCard(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        self.activityIndicatorView.startAnimating()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia/Kolkata&type=DeleteCard&UserType=Driver&GeneralUserType=Passenger&tSessionId=\(sessionId)&iUserId=\(userId)&GeneralDeviceType=Android&iCardUid=\(self.iCardUid)"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                    self.post_listCard()
                    self.viewMessage.isHidden = false
                    self.viewMessageConfirm.isHidden = true
                    self.viewMessageUpdated.isHidden = false
                }
                self.viewMessage.isHidden = false
                self.viewMessageConfirm.isHidden = true
                self.viewMessageUpdated.isHidden = false
                
                
                self.activityIndicatorView.stopAnimating()
                
                self.tableViewCard.reloadData()

                
            case .failure(let error):
                print(error)
                self.activityIndicatorView.stopAnimating()
            }
        }
        
    }

    
    

    func post_addMoney(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        
        self.activityIndicatorView.startAnimating()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralAppVersion=1.0.4&GeneralUserType=Driver&GeneralDeviceType=Android&vUserDeviceCountry=in&vTimeZone=Asia%2FKolkata&vFirebaseDeviceToken=\(kDeviceToken)&eTransactionId=pi_1HSdv5HyZB79Dwoy4Qx5wY5m&fAmount=\(amount)&ePaymentMode=Card&iMemberId=\(userId)&type=addMoneyUserWallet&tSessionId=\(sessionId)&GeneralMemberId=\(userId)&vDeviceToken=\(kDeviceToken)&isPaymentApproved=TXN_SUCCESS"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                    let balance = json["Action"].stringValue
                    self.viewMessage.isHidden = false
                    self.viewMessageUpdated.isHidden = false
                    self.lblMessageUpdated.text! = "Money has been added successfully"
                    self.isMoneyAdded = true
                    
                    UserDefaults.standard.set(balance, forKey: "user_available_balance")
                    self.activityIndicatorView.stopAnimating()
                }
                
            case .failure(let error):
                print(error)
                self.activityIndicatorView.stopAnimating()
            }
        }
        
    }

    
    

    
}
