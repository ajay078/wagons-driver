//
//  ViewTranscationsVC.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 15/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class ViewTranscationsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var arrTransactions = [ModalViewTransactions]()
    var arrFilteredMoneyIn = [ModalViewTransactions]()
    var arrFilteredMoneyOut = [ModalViewTransactions]()

    var isMoneyIn:Bool = false
    var isMoneyOut:Bool = false
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnMoneyIn: UIButton!
    @IBOutlet weak var btnMoneyOut: UIButton!
    
    @IBOutlet weak var viewAll: UIView!
    @IBOutlet weak var viewMoneyIn: UIView!
    @IBOutlet weak var viewMoneyOut: UIView!
    
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    
    @IBOutlet weak var lblNoTransactions: UILabel!
    @IBOutlet weak var tableViewTransactions: UITableView!
    
    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
    
// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblNoTransactions.isHidden = true
        self.post_viewTransactions()
        self.color_button()
        self.setLanguageLabel()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
    }
    
// MARK: - Button Actions

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAll(_ sender: Any) {
        btnAll.setTitleColor(.black, for: .normal)
        btnMoneyIn.setTitleColor(.darkGray, for: .normal)
        btnMoneyOut.setTitleColor(.darkGray, for: .normal)

        viewAll.isHidden = false
        viewMoneyIn.isHidden = true
        viewMoneyOut.isHidden = true    }
    @IBAction func btnMoneyIn(_ sender: Any) {
        btnAll.setTitleColor(.darkGray, for: .normal)
        btnMoneyIn.setTitleColor(.black, for: .normal)
        btnMoneyOut.setTitleColor(.darkGray, for: .normal)

        viewAll.isHidden = true
        viewMoneyIn.isHidden = false
        viewMoneyOut.isHidden = true
    }
    @IBAction func btnMoneyOut(_ sender: Any) {
        btnAll.setTitleColor(.darkGray, for: .normal)
        btnMoneyIn.setTitleColor(.darkGray, for: .normal)
        btnMoneyOut.setTitleColor(.black, for: .normal)

        viewAll.isHidden = true
        viewMoneyIn.isHidden = true
        viewMoneyOut.isHidden = false
    }
    
// MARK: - Table View Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTransactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TransactionsCell = tableView.dequeueReusableCell(withIdentifier: "TransactionsCell", for: indexPath) as! TransactionsCell
        
        return cell
    }
    
    func color_button(){
        btnAll.setTitleColor(.black, for: .normal)
        btnMoneyIn.setTitleColor(.darkGray, for: .normal)
        btnMoneyOut.setTitleColor(.darkGray, for: .normal)

        viewAll.isHidden = false
        viewMoneyIn.isHidden = true
        viewMoneyOut.isHidden = true
        
        

    }
    
// MARK: - set Language Labels

    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_Transaction_HISTORY"] as? String ?? ""
        lblNoTransactions.text! = objLang?["LBL_NO_TRANSACTION_AVAIL"] as? String ?? ""
        btnAll.setTitle(objLang?["LBL_ALL_MANAGE_WALLET"] as? String ?? "", for: .normal)
        btnMoneyIn.setTitle(objLang?["LBL_MONEY_IN"] as? String ?? "", for: .normal)
        btnMoneyOut.setTitle(objLang?["LBL_MONEY_OUT"] as? String ?? "", for: .normal)

        
        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft


            
        }else{

        }
        
        
        
        
        
    }
    
}





// MARK: - Webservice calling

extension ViewTranscationsVC{
    
    func post_viewTransactions(){
        
        let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
        let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
        let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
        
        self.activityIndicatorView.startAnimating()
        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralMemberId=\(userId)&GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&iMemberId=\(userId)&vTimeZone=Asia/Kolkata&page=0&type=getTransactionHistory&UserType=Driver&GeneralUserType=Passenger&tSessionId=\(sessionId)&GeneralDeviceType=ios"
        
        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let searchURL = URL(string: newURL ?? "")!
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(searchURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                    if let message = json["message"].arrayObject{
                        for index in 0..<message.count{
                            let obj = message[index]
                            let modalObj = ModalViewTransactions.init(dict: obj as? [String : Any] ?? [:])
                            self.arrTransactions.append(modalObj)
                            
                            self.arrFilteredMoneyIn = self.arrTransactions.filter({$0.eType == "Credit"})
                            self.arrFilteredMoneyOut = self.arrTransactions.filter({$0.eType == "Debit"})

                            self.tableViewTransactions.reloadData()
                        }
                    }
                    
                }else{
                    self.lblNoTransactions.isHidden = false
                    
                }
                self.activityIndicatorView.stopAnimating()

                
            case .failure(let error):
                print(error)
                self.activityIndicatorView.stopAnimating()
            }
        }
        
    }
    
    
    
}
