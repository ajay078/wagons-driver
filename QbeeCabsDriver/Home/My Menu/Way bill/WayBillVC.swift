//
//  WayBillVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 09/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class WayBillVC: UIViewController {

    var objWayBill = MWayBill(dict: [:])
    
    // MARK: - Web service variable

    let userId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
    let country:String = UserDefaults.standard.string(forKey: "vCountry") ?? ""
    let sessionId:String = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
    let appVersion:String = UserDefaults.standard.string(forKey: "iAppVersion") ?? ""
    let timeZone:String = UserDefaults.standard.string(forKey: "vTimeZone") ?? ""
    
    //MARK: - Outlets
    
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblTripNo: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPassengerName: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    
    @IBOutlet weak var lblDriverNameDown: UILabel!
    @IBOutlet weak var lblLicenceNo: UILabel!
    @IBOutlet weak var lblCapacity: UILabel!
    
    
    @IBOutlet weak var viewMiddle: UIView!
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewTripNumberL: UIView!
    
    
    @IBOutlet weak var lblTripUpL: UILabel!
    @IBOutlet weak var lblTripNoL: UILabel!
    @IBOutlet weak var lblRateL: UILabel!
    @IBOutlet weak var lblTimeL: UILabel!
    @IBOutlet weak var lblPassengerNameL: UILabel!
    @IBOutlet weak var lblViaL: UILabel!
    @IBOutlet weak var lblFromL: UILabel!
    @IBOutlet weak var lblToL: UILabel!
    @IBOutlet weak var lblDriverL: UILabel!
    @IBOutlet weak var lblNameL: UILabel!
    @IBOutlet weak var lblLicencePlateL: UILabel!
    @IBOutlet weak var lblPassengerCapacityL: UILabel!
    
    @IBOutlet weak var viewDriverName: UIView!
    @IBOutlet weak var viewTrip: UIView!
    @IBOutlet weak var viewSTripNo: UIStackView!
    @IBOutlet weak var viewSRate: UIStackView!
    @IBOutlet weak var viewSTime: UIStackView!
    @IBOutlet weak var viewSPassengerName: UIStackView!
    @IBOutlet weak var viewVia: UIView!
    @IBOutlet weak var viewSFrom: UIStackView!
    @IBOutlet weak var viewSTo: UIStackView!
    @IBOutlet weak var viewDriver: UIView!
    @IBOutlet weak var viewSName: UIStackView!
    @IBOutlet weak var viewSLicencePlate: UIStackView!
    @IBOutlet weak var viewSPassengerCapacity: UIStackView!
    

// MARK: - View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        self.corner_radius()
        self.post_wayBill()
        
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
// MARK: - Button Actions

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    



    func details(){
        
        
        self.lblDriverName.text! = objWayBill.driverName
        self.lblTripNo.text! = objWayBill.vRideNo
        self.lblRate.text! = objWayBill.Rate
        self.lblTime.text! = objWayBill.tTripRequestDate
        self.lblPassengerName.text! = objWayBill.PassengerName
        self.lblFrom.text! = objWayBill.tSaddress
        self.lblTo.text! = objWayBill.tDaddress
        self.lblDriverNameDown.text! = objWayBill.driverName
        self.lblLicenceNo.text! = objWayBill.Licence_Plate
        self.lblCapacity.text! = objWayBill.PassengerCapacity
        
    }
    
    
// MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_MENU_WAY_BILL"] as? String ?? ""
        lblTripUpL.text! = objLang?["LBL_TRIP_TXT"] as? String ?? ""
        lblTripNoL.text! = "\(objLang?["LBL_TRIP_TXT"] as? String ?? "")#"
        lblRateL.text! = objLang?["LBL_RATE_WAY_BILL"] as? String ?? ""
        lblTimeL.text! = objLang?["LBL_TIME_WAY_BILL"] as? String ?? ""
        lblPassengerNameL.text! = objLang?["LBL_PASSENGER_NAME_WAY_BILL"] as? String ?? ""
        lblViaL.text! = objLang?["LBL_VIA_WAY_BILL"] as? String ?? ""
        lblFromL.text! = objLang?["LBL_FROM_WAY_BILL"] as? String ?? ""
        lblToL.text! = objLang?["LBL_TO_WAY_BILL"] as? String ?? ""
        lblDriverL.text! = objLang?["LBL_DIVER_WAY_BILL"] as? String ?? ""
        lblNameL.text! = objLang?["LBL_NAME_WAY_BILL"] as? String ?? ""
        lblLicencePlateL.text! = objLang?["LBL_LICENCE_PLATE_WAY_BILL"] as? String ?? ""
        let passengertext = objLang?["LBL_PASSENGER_WAY_BILL"] as? String ?? ""
        let capacityText = objLang?["LBL_CAPACITY_WAY_BILL"] as? String ?? ""
        lblPassengerCapacityL.text! = passengertext + capacityText


        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft
            self.viewMain.semanticContentAttribute = .forceRightToLeft
            self.lblDriverName.semanticContentAttribute = .forceRightToLeft
            self.lblDriverL.semanticContentAttribute = .forceRightToLeft
            self.lblTripUpL.semanticContentAttribute = .forceRightToLeft
            self.viewSTripNo.semanticContentAttribute = .forceRightToLeft
            self.viewSRate.semanticContentAttribute = .forceRightToLeft
            self.viewSTime.semanticContentAttribute = .forceRightToLeft
            self.viewSPassengerName.semanticContentAttribute = .forceRightToLeft
            self.viewVia.semanticContentAttribute = .forceRightToLeft
            self.viewSFrom.semanticContentAttribute = .forceRightToLeft
            self.viewSTo.semanticContentAttribute = .forceRightToLeft
            self.viewDriver.semanticContentAttribute = .forceRightToLeft
            self.viewSName.semanticContentAttribute = .forceRightToLeft
            self.viewSLicencePlate.semanticContentAttribute = .forceRightToLeft
            self.viewSPassengerCapacity.semanticContentAttribute = .forceRightToLeft
            
            
            self.lblTripNoL.textAlignment = .right
            self.lblRateL.textAlignment = .right
            self.lblTimeL.textAlignment = .right
            self.lblPassengerNameL.textAlignment = .right
            self.lblFromL.textAlignment = .right
            self.lblToL.textAlignment = .right
            self.lblDriverL.textAlignment = .right
            self.lblNameL.textAlignment = .right
            self.lblLicencePlateL.textAlignment = .right
            self.lblPassengerCapacityL.textAlignment = .right

            
            

            
        }else{

        }
        
        
        
    }
    func corner_radius(){
        
        self.viewMiddle.layer.borderWidth = 1
        self.viewMiddle.layer.borderColor = UIColor.lightGray.cgColor
        
        self.viewBottom.layer.borderWidth = 1
        self.viewBottom.layer.borderColor = UIColor.lightGray.cgColor
        
    }
    
}

// MARK: - Webservice calling

extension WayBillVC{
    
    func post_wayBill(){

        ValidationManager.sharedManager.showLoader()

//        let urlString = "https://apps.wagonstaxi.com/webservice.php?GeneralAppVersion=1.0.4&vUserDeviceCountry=\(country)&vTimeZone=Asia %2FCalcutta&type=displayWayBill&GeneralUserType=Driver&tSessionId=\(sessionId)&iDriverId=\(userId)&GeneralDeviceType=Android"
//
//        let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
//        let searchURL = URL(string: newURL ?? "")!
        
        var urlComps = URLComponents(string: BaseURL)!
        let queryItems = [URLQueryItem(name: "GeneralUserType", value: kUserType),
                          URLQueryItem(name: "type", value: kDisplayWayBill),
                          URLQueryItem(name: "GeneralMemberId", value: self.userId),
                          URLQueryItem(name: "iDriverId", value: self.userId),
                          URLQueryItem(name: "GeneralAppVersion", value: self.appVersion),
                          URLQueryItem(name: "vUserDeviceCountry", value: self.country),
                          URLQueryItem(name: "GeneralDeviceType", value: kDeviceType),
                          URLQueryItem(name: "tSessionId", value: self.sessionId),
                          URLQueryItem(name: "vTimeZone", value: self.timeZone)]
        
        urlComps.queryItems = queryItems
        let addedUrl = urlComps.url!
        print(addedUrl)
        
        
        
        let headers:HTTPHeaders = ["Authorization" : "Bearer",
                                   "Content-Type": "application/json"]
        
        AF.request(addedUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            
            switch responseObject.result {
            case .success(let value):
                let json = JSON(value)
                print(json)

                let action = json["Action"].stringValue
                if action == "1"{
                    
                }
                if let message = json["message"].dictionaryObject{
                    self.objWayBill = MWayBill.init(dict: message)
                    self.details()
                }
                
                ValidationManager.sharedManager.hideLoader()

                
            case .failure(let error):
                print(error)
                ValidationManager.sharedManager.hideLoader()
            }
        }
        
    }
    
    
    
}
