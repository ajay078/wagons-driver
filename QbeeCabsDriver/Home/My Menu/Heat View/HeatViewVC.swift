//
//  HeatViewVC.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 11/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class HeatViewVC: UIViewController {

    var strCurrentLat:Double = UserDefaults.standard.double(forKey: "currentLat")
    var strCurrentLong:Double = UserDefaults.standard.double(forKey: "currentLong")

    var zoomLevel:Float = 14
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var viewUpar: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLanguageLabel()
        print(strCurrentLat)
        print(strCurrentLong)
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        mapView.padding = UIEdgeInsets(top: 0, left: 20, bottom: 20, right: 0)

        mapView.camera = GMSCameraPosition.camera(withLatitude: strCurrentLat,longitude: strCurrentLat, zoom: 1)

        self.animate_googleMaps()

        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    func animate_googleMaps(){
        
        
        var count:Double = 0.1
        var zoomLevel:Double = 0.1
        for _ in 1...70{
            count = count + 0.1
            zoomLevel = zoomLevel + 0.1
            DispatchQueue.main.asyncAfter(deadline: .now()+count){
                CATransaction.begin()
                CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
                let city = GMSCameraPosition.camera(withLatitude: self.strCurrentLat,longitude: self.strCurrentLong, zoom: Float(zoomLevel))
                self.mapView.animate(to: city)
                CATransaction.commit()
            }
        }
        
    }
    
    
    // MARK: - set Language Labels
    
    func setLanguageLabel(){
        
        let objLang = UserDefaults.standard.dictionary(forKey: "AllLanguageLabels")
        lblHeader.text! = objLang?["LBL_TITLE_HEAT_VIEW"] as? String ?? ""

        let langCode = UserDefaults.standard.string(forKey: "default_language")
        if langCode == "AR"{
            
            self.viewUpar.semanticContentAttribute = .forceRightToLeft

            
        }else{

        }
        
        
        
    }
    
    

}
