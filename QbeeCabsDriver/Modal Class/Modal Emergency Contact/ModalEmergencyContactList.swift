//
//  ModalEmergencyContactList.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 25/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit

struct ModalEmergencyContactList {

    
    var strUserId:String = ""
    var strPhone:String = ""
    var strName:String = ""
    var strEmergencyId:String = ""
    var strUserType:String = ""
    var strCountry:String = ""

    
    init(dict: [String:Any]) {
        
        self.strUserId = dict["iUserId"] as? String ?? ""
        self.strPhone = dict["vPhone"] as? String ?? ""
        self.strName = dict["vName"] as? String ?? ""
        self.strEmergencyId = dict["iEmergencyId"] as? String ?? ""
        self.strUserType = dict["eUserType"] as? String ?? ""
        self.strCountry = dict["vCountry"] as? String ?? ""

    }
    
    
}


struct ModalSelectIssue {

    
    var vTitle:String = ""
    var iHelpDetailId:String = ""


    
    init(dict: [String:Any]) {
        
        self.vTitle = dict["vTitle"] as? String ?? ""
        self.iHelpDetailId = dict["iHelpDetailId"] as? String ?? ""


    }
    
    
}


//["iUserId": 564, "vPhone": +919346430455, "vName": Chaitu, "iEmergencyId": 281, "eUserType": Passenger]
