//
//  ModalLanguage.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 20/07/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation

struct ModalListLang {
    
    var vCode = ""
    var vGMapLangCode = ""
    var eType = ""
    var vTitle = ""
    var vCurrencyCode = ""
    var vCurrencySymbol = ""
    var eDefault = ""

    init(dict: [String:Any]) {
        
        self.vCode = dict["vCode"] as? String ?? ""
        self.vGMapLangCode = dict["vGMapLangCode"] as? String ?? ""
        self.eType = dict["eType"] as? String ?? ""
        self.vTitle = dict["vTitle"] as? String ?? ""
        self.vCurrencyCode = dict["vCurrencyCode"] as? String ?? ""
        self.vCurrencyCode = dict["vCurrencyCode"] as? String ?? ""
        self.eDefault = dict["eDefault"] as? String ?? ""

        
    }
    
    
}


struct ModalListCurrency {
    
    var iCurrencyId = ""
    var vName = ""
    var vSymbol = ""
    var iDispOrder = ""
    var eDefault = ""
    var Ratio = ""
    var fThresholdAmount = ""
    var eStatus = ""

    init(dict: [String:Any]) {
        
        self.iCurrencyId = dict["iCurrencyId"] as? String ?? ""
        self.vName = dict["vName"] as? String ?? ""
        self.vSymbol = dict["vSymbol"] as? String ?? ""
        self.iDispOrder = dict["iDispOrder"] as? String ?? ""
        self.eDefault = dict["eDefault"] as? String ?? ""
        self.Ratio = dict["Ratio"] as? String ?? ""
        self.fThresholdAmount = dict["fThresholdAmount"] as? String ?? ""
        self.eStatus = dict["eStatus"] as? String ?? ""

        
    }
    
    
}
