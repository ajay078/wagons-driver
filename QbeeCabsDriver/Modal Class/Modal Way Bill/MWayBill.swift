//
//  MWayBill.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 10/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation

struct MWayBill {
    
    
    var driverName = ""
    var vRideNo = ""
    var tTripRequestDate = ""
    var ProjectName = ""
    var tSaddress = ""
    var tDaddress = ""
    var PassengerName = ""
    var Licence_Plate = ""
    var PassengerCapacity = ""
    var PackageName = ""
    var tPackageDetails = ""
    var vReceiverName = ""
    var Rate = ""
    var eType = ""


    
    
    init(dict: [String:Any]) {
        
        
        self.driverName = dict["DriverName"] as? String ?? ""
        self.vRideNo = dict["vRideNo"] as? String ?? ""
        self.tTripRequestDate = dict["tTripRequestDate"] as? String ?? ""
        self.ProjectName = dict["ProjectName"] as? String ?? ""
        self.tSaddress = dict["tSaddress"] as? String ?? ""
        self.tDaddress = dict["tDaddress"] as? String ?? ""
        self.PassengerName = dict["PassengerName"] as? String ?? ""
        self.Licence_Plate = dict["Licence_Plate"] as? String ?? ""
        self.PassengerCapacity = dict["PassengerCapacity"] as? String ?? ""
        self.PackageName = dict["PackageName"] as? String ?? ""
        self.tPackageDetails = dict["tPackageDetails"] as? String ?? ""
        self.Rate = dict["Rate"] as? String ?? ""
        self.eType = dict["eType"] as? String ?? ""

        
        
        
    }
    
}
