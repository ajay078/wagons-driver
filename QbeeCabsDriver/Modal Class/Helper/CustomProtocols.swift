//
//  CustomerProtocols.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 16/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation

@objc protocol SendDataBackDelegate : class {
    
    @objc optional func sendData(addCountryCode:String)
    
    @objc optional func sendDataPromo(addPromoCode:String)
    
    @objc optional func sendDataContacts(addContacts:String, addNumber:String)

    @objc optional func sendDataPayment(addPayment:Bool)

    
}


/*
 
 let urlString = "https://apps.wagonstaxi.com/webservice.php?vFirebaseDeviceToken=\(kDeviceToken)&GeneralAppVersion=1.0.4&vLang=EN&vDeviceType=ios&type=getDetail&GeneralUserType=Driver&tSessionId=\(sessionId)&GeneralDeviceType=ios&AppVersion=1.0.4&GeneralMemberId=\(userId)&iUserId=\(userId)&vUserDeviceCountry=\(country)&vDeviceToken=\(kDeviceToken)&vTimeZone=Asia/Kolkata&UserType=Driver"

 let newURL = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
 let searchURL = URL(string: newURL ?? "")!
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 */
