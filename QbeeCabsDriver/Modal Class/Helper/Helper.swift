//
//  Helper.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 15/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation



func checkSession(){
    
    var window: UIWindow?
    var navigationcontroller:UINavigationController?
    
    UserDefaults.standard.removeObject(forKey: "tSessionId")
    let sb: UIStoryboard = UIStoryboard(name: "Main", bundle:Bundle.main)
    navigationcontroller = sb.instantiateViewController(withIdentifier: "navMain") as? UINavigationController
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    appDelegate.window?.rootViewController = navigationcontroller
    window?.makeKeyAndVisible()
}



extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
//    textField.setLeftPaddingPoints(10)
//    textField.setRightPaddingPoints(10)

}



extension EnRouteVC{
    
    func alert_show(message:String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension AcceptRequestVC{
    
    func alert_show(message:String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension YourTripsVC{
    
    func alert_show(message:String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
extension HomePageVC{
    
    @objc func respondToSwipeToEmergencyCall(gesture: UIGestureRecognizer) {
        
        self.bottomConstantEmergencyCall = self.viewEmergencyCall.frame.size.height
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
            case UISwipeGestureRecognizer.Direction.down:
                print("Swiped down")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3){
                    self.viewEmergencyCall.isHidden = false
                }

                self.bottomViewEmergencyCall.constant = -self.bottomConstantEmergencyCall
                UIView.animate(withDuration: 0.4) {
                    self.view.layoutIfNeeded()
                }
                
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
//                    self.viewSearchBar.isHidden = true
//                }
//
//                self.ViewCreateBottom.constant = 5
//                UIView.animate(withDuration: 0.7) {
//                    self.view.layoutIfNeeded()
//                }
                
            default:
                break
            }
        }
    }
}


extension HomePageVC{
    
    @objc func respondToSwipeToEmergencyContactList(gesture: UIGestureRecognizer) {
        
        self.bottomConstantContactList = self.viewEmergencyContactList.frame.size.height
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
            case UISwipeGestureRecognizer.Direction.down:
                print("Swiped down")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
                    self.viewEmergencyContactList.isHidden = false
                }

                self.bottomViewEmergencyContactList.constant = -self.bottomConstantContactList
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
//                    self.viewSearchBar.isHidden = true
//                }
//
//                self.ViewCreateBottom.constant = 5
//                UIView.animate(withDuration: 0.7) {
//                    self.view.layoutIfNeeded()
//                }
                
            default:
                break
            }
        }
    }
}


extension EnRouteVC{
    
    @objc func respondToSwipeToEmergencyCall(gesture: UIGestureRecognizer) {
        
        self.bottomConstantEmergencyCall = self.viewEmergencyCall.frame.size.height
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
            case UISwipeGestureRecognizer.Direction.down:
                print("Swiped down")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
                    self.viewEmergencyCall.isHidden = false
                }

                self.bottomViewEmergencyCall.constant = -self.bottomConstantEmergencyCall
                UIView.animate(withDuration: 0.7) {
                    self.view.layoutIfNeeded()
                }
                
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
//                    self.viewSearchBar.isHidden = true
//                }
//
//                self.ViewCreateBottom.constant = 5
//                UIView.animate(withDuration: 0.7) {
//                    self.view.layoutIfNeeded()
//                }
                
            default:
                break
            }
        }
    }
}


extension EnRouteVC{
    
    @objc func respondToSwipeToEmergencyContactList(gesture: UIGestureRecognizer) {
        
        self.bottomConstantContactList = self.viewEmergencyContactList.frame.size.height
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
            case UISwipeGestureRecognizer.Direction.down:
                print("Swiped down")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
                    self.viewEmergencyContactList.isHidden = false
                }

                self.bottomViewEmergencyContactList.constant = -self.bottomConstantContactList
                UIView.animate(withDuration: 0.7) {
                    self.view.layoutIfNeeded()
                }
                
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
//                    self.viewSearchBar.isHidden = true
//                }
//
//                self.ViewCreateBottom.constant = 5
//                UIView.animate(withDuration: 0.7) {
//                    self.view.layoutIfNeeded()
//                }
                
            default:
                break
            }
        }
    }
}


extension EnRouteVC{
    
    @objc func respondToBeginEndTrip(gesture: UIGestureRecognizer) {
                
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
                if isTripBegin == false{
                    print("trip begun")
                    self.post_startTrip()
                    isTripBegin = true
                }else{
                    print("trip end")
                    self.post_endTrip()

                }
            case UISwipeGestureRecognizer.Direction.down:
                print("Swiped down")
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
                
            default:
                break
            }
        }
    }
}





//extension FinalFareVC{
//
//    func alert_show(message:String){
//        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
//    }
//
//}

//extension GetMobileVC{
//
//    func alert_show(message:String){
//        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
//    }
//
//}




