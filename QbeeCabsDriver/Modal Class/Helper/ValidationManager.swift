//
//  ValidationManager.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 02/09/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import UIKit
import NVActivityIndicatorView

let themeColor = #colorLiteral(red: 1, green: 0.8588235294, blue: 0.2392156863, alpha: 1)


class ValidationManager: UIViewController, NVActivityIndicatorViewable {

    static let sharedManager = ValidationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    func showAlert(message: String, title: String = "", btnTitle: String, controller: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: NSLocalizedString(btnTitle, tableName: nil, comment: ""), style: .default, handler: nil)
        alertController.addAction(OKAction)
        controller.present(alertController, animated: true, completion: nil)
    }

    func showAlertWithAction(title:String,message:String, btnText:String,controller:UIViewController, completion: @escaping (_ status: Bool) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let subView = alertController.view.subviews.first!
        let alertContentView = subView.subviews.first!
        alertContentView.backgroundColor = UIColor.gray
        alertContentView.layer.cornerRadius = 20
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            completion(true)
        })
        alertController.addAction(OKAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- ADD BUTTON ON KEYBOARD
        
    func addDoneButtonOnKeyboard(textType: UITextField){
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
            doneToolbar.barStyle = .default
            
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    //        let done: UIBarButtonItem = UIBarButtonItem(title: "Next", style: .done, target: self, action: #selector(self.doneButtonAction))
            
            if textType == textType{
            let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonActionMobile))
                
                let items = [flexSpace, done]
                doneToolbar.items = items
                doneToolbar.sizeToFit()

            }
     
            textType.inputAccessoryView = doneToolbar

    //        txtTelephoneNumber.inputAccessoryView = doneToolbar
        }
    
    @objc func doneButtonActionMobile(){

        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)

    }
    
    //MARK:- Enimation of view

    
    func showMainViewNN(viewMain: UIView, viewInside:UIView) {
    //        viewLang.frame = self.view.frame

    //        self.view.window?.addSubview(viewLang)
    //        viewLang.border1(UIColor.lightGray, 8, 0)
            viewMain.isHidden = false
            self.showPopUpAnimation(viewTest: viewInside)
        }
        
        
        func showPopUpAnimation(viewTest: UIView) {
            let orignalT: CGAffineTransform = viewTest.transform
            viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
            UIView.animate(withDuration: 0.3, animations: {
                
                viewTest.transform = orignalT
            }, completion:nil)
        }
        
        func removeSubViewWithAnimation(viewMain: UIView, viewPopUP: UIView) {
            let orignalT: CGAffineTransform = viewPopUP.transform
            UIView.animate(withDuration: 0.3, animations: {
                viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            }, completion: {(sucess) in
    //            viewMainView.removeFromSuperview()
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2){
                viewMain.isHidden = true
                viewPopUP.transform = orignalT

            }
        }
    
    
    //MARK:- loader
    
    
    func showLoader(){
        
        let size = CGSize(width: 75.0, height: 75.0)
        let Color = UIColor(red: 163.0/255.0, green: 163.0/255.0, blue: 163.0/255.0, alpha: 0.2)
        
        startAnimating(size, type: .ballSpinFadeLoader, color: themeColor, backgroundColor: Color, fadeInAnimation: nil)
        
        
    }
    
    func hideLoader(){
        stopAnimating()
    }
    
}
