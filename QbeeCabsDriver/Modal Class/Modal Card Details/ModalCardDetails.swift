//
//  ModalCardDetails.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 29/07/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation




struct ModalCardDetails {
    
    var iDefault = ""
    var iCardNumber = ""
    var iCardUid = ""
    var iMM = ""
    var iYY = ""
    var created = ""
    
    
    
    
    init(dict: [String:Any]) {
        
        self.iDefault = dict["iDefault"] as? String ?? ""
        self.iCardNumber = dict["iCardNumber"] as? String ?? ""
        self.iCardUid = dict["iCardUid"] as? String ?? ""
        self.iMM = dict["iMM"] as? String ?? ""
        self.iYY = dict["iYY"] as? String ?? ""
        self.created = dict["created"] as? String ?? ""


        
        
    }
    
}
