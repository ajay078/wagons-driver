//
//  MHomeSelectVehicle.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 30/05/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation


struct ModalHomeSelectVehicle {

    var iMakeId:String = ""
    var iModelId:String = ""
    var vTitle:String = ""
    var vMake:String = ""
    var vLicencePlate:String = ""
    var eStatus:String = ""
    var iDriverVehicleId:String = ""
    var iYear:String = ""
    var vColour:String = ""
    var vCarType:String = ""
    
    init(dict: [String:Any]) {
        
        self.vTitle = dict["vTitle"] as? String ?? ""
        self.vMake = dict["vMake"] as? String ?? ""
        self.vLicencePlate = dict["vLicencePlate"] as? String ?? ""
        self.iDriverVehicleId = dict["iDriverVehicleId"] as? String ?? ""
        self.eStatus = dict["eStatus"] as? String ?? ""
        self.iYear = dict["iYear"] as? String ?? ""
        self.vColour = dict["vColour"] as? String ?? ""
        self.vCarType = dict["vCarType"] as? String ?? ""

    }
    
    
}
