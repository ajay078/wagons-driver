//
//  ModalViewTransactions.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 07/07/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation


struct ModalViewTransactions {
    
    var iUserWalletId = ""
    var iUserId = ""
    var eUserType = ""
    var iBalance = ""
    var eType = ""
    var iTripId = ""
    var tDescription = ""
    var dDate = ""

    
    init(dict: [String:Any]) {
        
        self.iUserWalletId = dict["iUserWalletId"] as? String ?? ""
        self.iUserId = dict["iUserId"] as? String ?? ""
        self.eUserType = dict["eUserType"] as? String ?? ""
        self.iBalance = dict["iBalance"] as? String ?? ""
        self.eType = dict["eType"] as? String ?? ""
        self.iTripId = dict["iTripId"] as? String ?? ""
        self.tDescription = dict["tDescription"] as? String ?? ""
        self.dDate = dict["dDate"] as? String ?? ""

        
        
    }
    
}
