//
//  UserDetails.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 29/05/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation

struct MUserDetaiils {
    
    var userId:String = ""
    var firstName:String = ""
    var lastName:String = ""
    var email:String = ""
    var type:String = ""
    var mobile:String = ""
    var countryCode:String = ""
    var language:String = ""
    var currency:String = ""
    var userType:String = ""
    var country:String = ""
    var sessionId:String = ""
    var balance:String = ""
    var iAppVersion = ""
    var vTimeZone = ""
    
    var iDriverVehicleId = ""
    var vMake = ""
    var vModel = ""
    var vLicencePlateNo = ""
    
    var imgUserId:String = ""
    var imageUrl:String = ""
    
    
    init(dict: [String:Any]) {
        
        self.userId = dict["iDriverId"] as? String ?? ""
        self.firstName = dict["vName"] as? String ?? ""
        self.lastName = dict["vLastName"] as? String ?? ""
        self.email = dict["vEmail"] as? String ?? ""
        self.type = dict["eRefType"] as? String ?? ""
        self.mobile = dict["vPhone"] as? String ?? ""
        self.countryCode = dict["vCode"] as? String ?? ""
        self.language = dict["vLang"] as? String ?? ""
        self.currency = dict["vCurrencyDriver"] as? String ?? ""
        self.userType = dict["eUserType"] as? String ?? ""
        self.country = dict["vCountry"] as? String ?? ""
        self.sessionId = dict["tSessionId"] as? String ?? ""
        self.iAppVersion = dict["iAppVersion"] as? String ?? ""
        self.vTimeZone = dict["vTimeZone"] as? String ?? ""
        self.iDriverVehicleId = dict["iDriverVehicleId"] as? String ?? ""
        self.vMake = dict["vMake"] as? String ?? ""
        self.vModel = dict["vModel"] as? String ?? ""
        self.vLicencePlateNo = dict["vLicencePlateNo"] as? String ?? ""

        self.balance = dict["user_available_balance"] as? String ?? ""
        
        self.imgUserId = dict["vImage"] as? String ?? ""
        self.imageUrl = "http://qbeecabs.com/webimages/upload/Driver/"
        
        //        http://qbeecabs.com/webimages/upload/Passenger/564/3_1589900181_39995.jpg
        
        self.saveUserInfoToUserdefaults()
        
        
    }
    
    
    func saveUserInfoToUserdefaults(){
        
        UserDefaults.standard.set( self.userId, forKey: "iDriverId")
        UserDefaults.standard.set( self.firstName, forKey: "vName")
        UserDefaults.standard.set( self.lastName, forKey: "vLastName")
        UserDefaults.standard.set( self.email, forKey: "vEmail")
        UserDefaults.standard.set( self.type, forKey: "eRefType")
        UserDefaults.standard.set( self.mobile, forKey: "vPhone")
        UserDefaults.standard.set( self.countryCode, forKey: "vCode")
        UserDefaults.standard.set( self.language, forKey: "vLang")
        UserDefaults.standard.set( self.currency, forKey: "vCurrencyDriver")
        UserDefaults.standard.set( self.userType, forKey: "eUserType")
        UserDefaults.standard.set( self.country, forKey: "vCountry")
        UserDefaults.standard.set( self.sessionId, forKey: "tSessionId")
        UserDefaults.standard.set( self.balance, forKey: "user_available_balance")
        UserDefaults.standard.set( self.iAppVersion, forKey: "iAppVersion")
        UserDefaults.standard.set( self.vTimeZone, forKey: "vTimeZone")
        UserDefaults.standard.set( self.iDriverVehicleId, forKey: "iDriverVehicleId")
        UserDefaults.standard.set( self.vMake, forKey: "vMake")
        UserDefaults.standard.set( self.vModel, forKey: "vModel")
        UserDefaults.standard.set( self.vLicencePlateNo, forKey: "vLicencePlateNo")

        UserDefaults.standard.set( self.imgUserId, forKey: "vImage")
        
        
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    
}

class SavedUserDetails: NSObject {
    
//    static let shared = SavedUserDetails()
//
//    let strUserId:String = UserDefaults.standard.string(forKey: "iDriverId") ?? ""
//    let strFirstName = UserDefaults.standard.string(forKey: "vName") ?? ""
//    let strLastName = UserDefaults.standard.string(forKey: "vLastName") ?? ""
//    let strCountry = UserDefaults.standard.string(forKey: "vCountry") ?? ""
//    let strSessionId = UserDefaults.standard.string(forKey: "tSessionId") ?? ""
//
//    let strBalance = UserDefaults.standard.string(forKey: "user_available_balance") ?? ""
//
//    let strImgUserId = UserDefaults.standard.string(forKey: "vImage") ?? ""
//    let strProfileImageUrl = "http://qbeecabs.com/webimages/upload/Driver/"
//
//    let baseImageUrl:String = "http://qbeecabs.com/webimages/icons/VehicleType/"
    
}
