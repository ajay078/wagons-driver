//
//  WebServiceConstants.swift
//  QbeeCabs Passenger
//
//  Created by mac on 23/10/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation

var BaseURL : String = "https://apps.wagonstaxi.com/webservice.php"


let kUserType : String = "Driver"
let kDeviceType : String = "ios"

let kSignIn : String = "signIn"
let kSignUp : String = "signup"

let kGetDetail : String = "getDetail"
let kGeneralConfigData : String = "generalConfigData"

let kLoadAvailableCab : String = "loadAvailableCab"
let kDisplaydrivervehicles : String = "displaydrivervehicles"
let kUpdateDriverStatus : String = "updateDriverStatus"




let kGetMemberWalletBalance : String = "GetMemberWalletBalance"
let kLoadEmergencyContacts : String = "loadEmergencyContacts"
let kAddEmergencyContacts : String = "addEmergencyContacts"
let kDeleteEmergencyContacts : String = "deleteEmergencyContacts"
let kGetRideHistory : String = "getRideHistory"
let kUpdateUserProfileDetail : String = "updateUserProfileDetail"
let kUpdatePassword : String = "updatePassword"
let kRequestResetPassword : String = "requestResetPassword"
let kGetTransactionHistory : String = "getTransactionHistory"
let kGetFAQ : String = "getFAQ"
let kGetsubHelpdetail : String = "getsubHelpdetail"
let kLoadDriverFeedBack : String = "loadDriverFeedBack"
let kDriverBankDetails : String = "DriverBankDetails"
let kDisplayWayBill : String = "displayWayBill"
let kGetDriverRideHistory : String = "getDriverRideHistory"




let kListPromoCode : String = "ListPromoCode"
let kStaticPage : String = "staticPage"
let kSendContactQuery : String = "sendContactQuery"

