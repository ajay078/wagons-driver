//
//  Modaltriphelp.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 25/05/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation


struct ModalTripHelp {
    
    var iHelpDetailCategoryId:String = ""
    var vTitle:String = ""
    var iUniqueId:String = ""

    

    init(dict: [String:Any]) {
        
        self.iHelpDetailCategoryId = dict["iHelpDetailCategoryId"] as? String ?? ""
        self.vTitle = dict["vTitle"] as? String ?? ""
        self.iUniqueId = dict["iUniqueId"] as? String ?? ""

        

        
    }

    
}


struct ModalTripHelpDetails {
    
    var iHelpDetailId:String = ""
    var vTitle:String = ""
    var tAnswer:String = ""

    

    init(dict: [String:Any]) {
        
        self.iHelpDetailId = dict["iHelpDetailId"] as? String ?? ""
        self.vTitle = dict["vTitle"] as? String ?? ""
        self.tAnswer = dict["tAnswer"] as? String ?? ""

        

        
    }

    
}
