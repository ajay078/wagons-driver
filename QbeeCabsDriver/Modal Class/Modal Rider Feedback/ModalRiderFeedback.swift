//
//  ModalRiderFeedback.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 29/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation

struct MRiderFeedback {

    
    var iRatingId:String = ""
    var iTripId:String = ""
    var vRating1:String = ""
    var tDate:String = ""
    var eUserType:String = ""
    var vImgName:String = ""
    var passengerid:String = ""
    var vName:String = ""
    
    var vImage:String = ""

    
    init(dict: [String:Any]) {
        
        self.iRatingId = dict["iRatingId"] as? String ?? ""
        self.iTripId = dict["iTripId"] as? String ?? ""
        self.vRating1 = dict["vRating1"] as? String ?? ""
        self.tDate = dict["tDate"] as? String ?? ""
        self.eUserType = dict["eUserType"] as? String ?? ""
        self.vImgName = dict["vImgName"] as? String ?? ""
        self.passengerid = dict["passengerid"] as? String ?? ""
        self.vName = dict["vName"] as? String ?? ""
        
        self.vImage = dict["vImage"] as? String ?? ""

    }
    
    
}
