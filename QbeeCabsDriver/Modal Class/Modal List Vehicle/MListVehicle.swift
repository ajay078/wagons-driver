//
//  MListVehicle.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 01/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation


struct MListVehicle {
    
    var iMakeId:String = ""
    var vMake:String = ""
    
    var arrListVehicleModal = [MListVehicleModal]()
    
    init(dict: [String:Any]) {
        
        self.iMakeId = dict["iMakeId"] as? String ?? ""
        self.vMake = dict["vMake"] as? String ?? ""
        
        let arrTempListVehicleModal = dict["vModellist"] as? [[String:Any]] ?? [[:]]
        for index in 0..<arrTempListVehicleModal.count{
            let obj = arrTempListVehicleModal[index]
            let modalObj = MListVehicleModal.init(dict: obj)
            self.arrListVehicleModal.append(modalObj)
        }
        
        
    }
    
}


struct MListVehicleModal {
    
    var iModelId:String = ""
    var vTitle:String = ""
    var eStatus:String = ""
    
    init(dict: [String:Any]) {
        
        self.iModelId = dict["iModelId"] as? String ?? ""
        self.vTitle = dict["vTitle"] as? String ?? ""
        self.eStatus = dict["eStatus"] as? String ?? ""
        
        
    }
    
}
