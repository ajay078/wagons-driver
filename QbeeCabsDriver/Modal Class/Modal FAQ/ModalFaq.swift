//
//  modalFaq.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 27/04/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation


struct ModalFaq {
    
    var strFaqcategoryId:String = ""
    var strStatus:String = ""
    var strDisplayOrder:String = ""
    var strTitle:String = ""
    var strCode:String = ""
    var strUniqueId:String = ""
    
    var arrQuestions = [modalFaqDetail]()

    init(dict: [String:Any]) {
        
        self.strFaqcategoryId = dict["iFaqcategoryId"] as? String ?? ""
        self.strStatus = dict["eStatus"] as? String ?? ""
        self.strDisplayOrder = dict["iDisplayOrder"] as? String ?? ""
        self.strTitle = dict["vTitle"] as? String ?? ""
        self.strCode = dict["vCode"] as? String ?? ""
        self.strUniqueId = dict["iUniqueId"] as? String ?? ""
        
        let arrTempQuestions = dict["Questions"] as? [[String:Any]] ?? [[:]]
        for index in 0..<arrTempQuestions.count{
            let obj = arrTempQuestions[index]
            let modalObj = modalFaqDetail.init(dictDetail: obj)
            self.arrQuestions.append(modalObj)
            
        }
        
    }

    
}

struct modalFaqDetail {
    
    var strTitle:String = ""
    var strAnswer:String = ""
    
    init(dictDetail: [String:Any]) {
        self.strTitle = dictDetail["vTitle"] as? String ?? ""
        self.strAnswer = dictDetail["tAnswer"] as? String ?? ""

        
        
    }
    
    
}
