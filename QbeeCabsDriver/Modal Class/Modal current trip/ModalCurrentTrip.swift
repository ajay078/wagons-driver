//
//  ModalDriverDetails.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 13/05/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation
import SwiftyJSON


struct ModalCurrentTrip {
    
    var strUserId = ""

    
    var tripDetails = ModalBookedTripDetails(dict: [:])
    
    init(dict: [String:Any]) {
        
        self.strUserId = dict["iUserId"] as? String ?? ""

        
//        http://qbeecabs.com/webimages/upload/Driver/261/3_1572529907_36971.jpg
        let trip = dict["TripDetails"] as? [String:Any] ?? [:]
        self.tripDetails = ModalBookedTripDetails.init(dict: trip)
        
        
        
    }
    
    
}


struct ModalBookedTripDetails {
    
    
    var iTripId = ""
    var tSaddress = ""
    var tDaddress = ""
    var tStartLat = ""
    var tStartLong = ""
    var tEndLat = ""
    var tEndLong = ""
    
    
    var driverDetails = ModalBookedDriverDetails(dict: [:])
    var carDetails = ModalBookedCarDetails(dict: [:])
    var passengerDetails = ModalBookedPassengerDetails(dict: [:])
    
    
    init(dict: [String:Any]) {
        
        self.iTripId = dict["iTripId"] as? String ?? ""
        print(iTripId)
        self.tSaddress = dict["tSaddress"] as? String ?? ""
        self.tDaddress = dict["tDaddress"] as? String ?? ""
        self.tStartLat = dict["tStartLat"] as? String ?? ""
        self.tStartLong = dict["tStartLong"] as? String ?? ""
        self.tEndLat = dict["tEndLat"] as? String ?? ""
        self.tEndLong = dict["tEndLong"] as? String ?? ""

        
        let driver = dict["DriverDetails"] as? [String:Any] ?? [:]
        self.driverDetails = ModalBookedDriverDetails.init(dict: driver)
        
        let car = dict["DriverCarDetails"] as? [String:Any] ?? [:]
        self.carDetails = ModalBookedCarDetails.init(dict: car)
        
        let passenger = dict["PassengerDetails"] as? [String:Any] ?? [:]
        self.passengerDetails = ModalBookedPassengerDetails.init(dict: passenger)
        
        
    }
    
    
}


struct ModalBookedDriverDetails {
    
    var iDriverId = ""
    var driverFirstName = ""
    var driverLastName = ""
    var driverLat = ""
    var driverLong = ""
    
    var imgIdDriver = ""
    var imageUrlDriver = ""
    
    
    
    init(dict: [String:Any]) {
        
        self.iDriverId = dict["iDriverId"] as? String ?? ""
        self.driverFirstName = dict["vName"] as? String ?? ""
        self.driverLastName = dict["vLastName"] as? String ?? ""
        self.driverLat = dict["vLatitude"] as? String ?? ""
        self.driverLong = dict["vLongitude"] as? String ?? ""

        self.imgIdDriver = dict["vImage"] as? String ?? ""
        self.imageUrlDriver = "http://qbeecabs.com/webimages/upload/Driver/"
        
        
        
    }
}



struct ModalBookedCarDetails {
    
    
    var vMake = ""
    var vTitle = ""
    var vColour = ""
    var imgIdVehicle = ""
    var iDriverVehicleId = ""
    var imageUrlVehicle = ""

    
    init(dict: [String:Any]) {
        
        self.vMake = dict["vMake"] as? String ?? ""
        self.vTitle = dict["vTitle"] as? String ?? ""
        self.vColour = dict["vColour"] as? String ?? ""
        self.imgIdVehicle = dict["vImage"] as? String ?? ""
        self.iDriverVehicleId = dict["iDriverVehicleId"] as? String ?? ""
 
        self.imageUrlVehicle = "http://qbeecabs.com/webimages/upload/documents/vehicles//"

        
        

    }
    
}


struct ModalBookedPassengerDetails {
    
    
    var firstName = ""
    var lastName = ""
    var rating = ""
    var passengerId = ""
    var passengerIntialLat = ""
    var passengerIntailLong = ""
    
    
    
    var imgIdPassenger = ""
    var imageUrlPassenger = ""

    
    init(dict: [String:Any]) {
        
        self.firstName = dict["vName"] as? String ?? ""
        self.lastName = dict["vLastName"] as? String ?? ""
        self.rating = dict["vAvgRating"] as? String ?? ""
        self.passengerId = dict["iUserId"] as? String ?? ""
        self.passengerIntialLat = dict["vLatitude"] as? String ?? ""
        self.passengerIntailLong = dict["vLongitude"] as? String ?? ""

        self.imgIdPassenger = dict["vImgName"] as? String ?? ""

 
        self.imageUrlPassenger = "http://qbeecabs.com/webimages/upload/Passenger/"

        
        
//        http://qbeecabs.com/webimages/upload/documents/vehicles//680/TempFile_20200507014212.jpg
    }
    
}
