//
//  CountryCodePick.swift
//  QbeeCabs Passenger
//
//  Created by Vijay on 04/09/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation



struct countrySection {
    
    var countryInitial = ""
    var arrCountryCode = [countryCode]()
    
    init(initial:String, array: [[String:String]]) {
        
        self.countryInitial = initial
        
        for obj in array{
            let objNew = countryCode.init(dict: obj)
            self.arrCountryCode.append(objNew)
        }
        
    }
    
    
}

struct countryCode {
    
    var country_name = ""
    var phone_code = ""
    var code = ""
    
    init(dict: [String:String]) {
        
        self.country_name = dict["country_name"] ?? ""
        self.phone_code = dict["phone_code"] ?? ""
        self.code = dict["code"] ?? ""

        
    }
    
}
