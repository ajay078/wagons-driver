//
//  ModalFinalFare.swift
//  QbeeCabs Passenger
//
//  Created by Ajay on 15/05/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation
import SwiftyJSON


struct ModalFinalFare {
    
    
    var strFixedFare:String = ""
    var PricePerKM:Double = 0
    var PricePerMin:Double = 0
    var strPricePerHour:String = ""
    var BaseFare:Double = 0
    var strCommision = ""
    var strMinFare = ""
    var strPickUpPrice = ""
    var strNightPrice = ""
    var strTripId = ""
    
    var locationStart:String = ""
    var locationEnd:String = ""
    
    var tripDate:String = ""
    var vehicleType:String = ""
    var finalDistance:String = ""
    var finalTime:String = ""

    
    
    init(dict: JSON) {
        
        
        self.strFixedFare = dict["fFixedFare"].stringValue
        self.PricePerKM = dict["fPricePerKM"].doubleValue
        self.PricePerMin = dict["fPricePerMin"].doubleValue
        self.strPricePerHour = dict["fPricePerHour"].stringValue
        self.BaseFare = dict["iBaseFare"].doubleValue
        self.strCommision = dict["fCommision"].stringValue
        self.strMinFare = dict["iMinFare"].stringValue
        self.strPickUpPrice = dict["fPickUpPrice"].stringValue
        self.vehicleType = dict["vVehicleType"].stringValue
        self.strTripId = dict["iTripId"].stringValue

        self.tripDate = dict["tTripRequestDate"].stringValue
        self.finalDistance = dict["fDistance"].stringValue
        self.finalTime = dict["TripTimeInMinutes"].stringValue

        self.locationStart = dict["tSaddress"].stringValue
        self.locationEnd = dict["tDaddress"].stringValue

        
    }
    
}
