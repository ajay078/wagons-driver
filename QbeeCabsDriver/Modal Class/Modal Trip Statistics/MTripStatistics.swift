//
//  MTripStatistics.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 11/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation


struct MTripStatistics {
    
    var CurrentMonth = ""
    var TotalEarnings = ""
    var TripCount = ""
    
    init(dict: [String:Any]) {
        
        self.CurrentMonth = dict["CurrentMonth"] as? String ?? ""
        self.TotalEarnings = dict["TotalEarnings"] as? String ?? ""
        self.TripCount = dict["TripCount"] as? String ?? ""
        
    }
    
}
