//
//  MBankDetails.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 04/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation


struct MBankDetails {
    
    var vPaymentEmail = ""
    var vBankAccountHolderName = ""
    var vAccountNumber = ""
    var vBankLocation = ""
    var vBankName = ""
    var vBIC_SWIFT_Code = ""
    
    
    init(dict: [String:Any]) {
        
        self.vPaymentEmail = dict["vPaymentEmail"] as? String ?? ""
        self.vBankAccountHolderName = dict["vBankAccountHolderName"] as? String ?? ""
        self.vAccountNumber = dict["vAccountNumber"] as? String ?? ""
        self.vBankLocation = dict["vBankLocation"] as? String ?? ""
        self.vBankName = dict["vBankName"] as? String ?? ""
        self.vBIC_SWIFT_Code = dict["vBIC_SWIFT_Code"] as? String ?? ""
        
        
        
    }
    
}
