//
//  MTripList.swift
//  QbeeCabsDriver
//
//  Created by Ajay on 05/06/20.
//  Copyright © 2020 mac mini . All rights reserved.
//

import Foundation


struct MTripList {
    
    var vName = ""
    var vLastName = ""
    var vRating1 = ""
    var PassengerUserId = ""
    var vRideNo = ""
    var TripTime = ""
    var eType = ""
    var iFare = ""
    var tTripRequestDate = ""
    var tSaddress = ""
    var tDaddress = ""
    var iActive = ""
    var vTripPaymentMode = ""
    
    var vImageId = ""
    var baseImageUrl:String = ""

    var dictFareDetails:[String:Any] = [:]

    
    // http://qbeecabs.com/webimages/upload/Passenger/614/1589896013_40398.jpg
    
    init(dict: [String:Any]) {
        
        
        self.vName = dict["vName"] as? String ?? ""
        self.vLastName = dict["vLastName"] as? String ?? ""
        self.vRating1 = dict["vRating1"] as? String ?? ""
        self.PassengerUserId = dict["iUserId"] as? String ?? ""
        self.vRideNo = dict["vRideNo"] as? String ?? ""
        self.TripTime = dict["TripTime"] as? String ?? ""
        self.eType = dict["eType"] as? String ?? ""
        self.iFare = dict["iFare"] as? String ?? ""
        self.tTripRequestDate = dict["tTripRequestDate"] as? String ?? ""
        self.tSaddress = dict["tSaddress"] as? String ?? ""
        self.tDaddress = dict["tDaddress"] as? String ?? ""
        self.iActive = dict["iActive"] as? String ?? ""
        self.vTripPaymentMode = dict["vTripPaymentMode"] as? String ?? ""

        self.vImageId = dict["vImage"] as? String ?? ""
        self.baseImageUrl = "https://apps.wagonstaxi.com/webimages/upload/Passenger/"

        
        self.dictFareDetails = dict["HistoryFareDetailsArr"] as? [String:Any] ?? [:]

        
        
    }
    
    
}
